<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M03HowToTransaction;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HowToTransactionController extends Controller
{
    protected $view = 'frontend.vendor.how-to-transaction';
    protected $route = 'vendor.how-to-transaction';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $howToTransaction = M03HowToTransaction::where('m02_outlet_id',Auth::user()->m02_outlet->id)->first();
        return view($this->view.'.index')
        ->with('howToTransactionList', 'selected')
        ->with('howToTransactionClass', 'start active open')
        ->with('howToTransaction',$howToTransaction);
    }

    public function store(Request $request) {
        $this->validate($request, M03HowToTransaction::rules());
        try {

            DB::beginTransaction();
            $howToTransaction = M03HowToTransaction::where('m02_outlet_id',Auth::user()->m02_outlet->id)->first();
            if(!$howToTransaction) $howToTransaction = new M03HowToTransaction();
            $howToTransaction->m02_outlet_id = Auth::user()->m02_outlet->id;
            $howToTransaction->content = $request->content;
            $howToTransaction->created_at = Carbon::now()->format('Y-m-d H:i:s');
            if (!$howToTransaction->save()) {
                return back()->withInput()->withErrors($howToTransaction->errors());
            }

            DB::commit();
            SessionHelper::setMessage("How To Transaction berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

}