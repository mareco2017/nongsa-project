@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
{!! Form::open(['url' => route('user.password.save'),'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}

<div class="section-container bg-white">
    <!-- BEGIN container -->

    <div class="container">
        @include('includes.session_message')
        <!-- BEGIN section-title -->
        <h1 class="section-title clearfix">
            {{Auth::user()->name}}'s Profile<br>
        </h1>
        <br>
        <!-- END section-title -->
        <!-- BEGIN row -->
        <div class="row row-space-10">
            {{ csrf_field() }}
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <div class="form-group m-b-15 {{ $errors->has('old_password') ? ' has-error' : '' }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                    {{ Form::label('old_password', 'Old Password')}}
                    {{ Form::password('old_password', array('class' => 'form-control', 'placeholder' => 'Enter your old password', 'required' => 'required', 'maxlength'=> '250'))}}
                </div>
                <div class="form-group m-b-15 {{ $errors->has('password') ? ' has-error' : '' }}">
                    {{ Form::label('password', 'New Password')}}
                    {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Enter your new password', 'maxlength'=> '250'))}}
                </div>
                <div class="form-group m-b-15 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    {{ Form::label('password_confirmation', 'Re-enter Password')}}
                    {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Re-enter your password', 'maxlength'=> '250'))}}
                </div>
                <div class="login-buttons form-group">
                    <button type="submit" class="btn btn-success btn-block btn-lg">Save</button>
                </div>
                {{ Form::close() }}
            </div>
            <div class="col-md-3">
                <div class="form-group m-b-15">
                    {{Form::checkbox('googleotp',null, Auth::user()->googleotp)}}
                    {{ Form::label('googleotp', 'Google Auth')}}
                </div>
                <div class="form-group m-b-15">
                    {{Form::checkbox('emailotp',null, Auth::user()->emailotp)}}
                    {{ Form::label('emailotp', 'Email')}}
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
</div>
<!-- END #promotions -->
@endsection