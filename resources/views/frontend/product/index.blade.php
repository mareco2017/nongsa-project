@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #section -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container">
		{{ csrf_field() }}
		@include('includes.session_message')
		<!-- BEGIN carousel slide -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<img src="{{ asset('/uploads/95b85df2417f3047a8ba6a00273049d3.jpg') }}" alt="First Item">
				</div>

				<div class="item">
					<img src="{{ asset('/uploads/5ad13b1f39438bb7dce27f93fdb5f6ec.jpg') }}" alt="Second Item">
				</div>

				<div class="item">
					<img src="{{ asset('/uploads/f614cf92801f8e68f1fe925dd4bb4b3d.jpg') }}" alt="Third Item">
				</div>
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<!-- END carousel slide -->
		<!-- BEGIN products -->
		<br>
		<div id="products" class="category-container borderless-container">
			<!-- BEGIN container -->
			<div class="container section-title">
				<!-- BEGIN section-title -->
				<h4 class="section-title item product clearfix">
					<span class="fa fa-shopping-basket section-icon-product"></span>
					{{ $title }}
				</h4>
			</div>
			<div class="col-md-12 grid-items">
				<div class="col-md-12 grid-items">
					@if ($products)
					@foreach ($products as $product)
					<div class="col-md-4 col-sm-4 hoverable-item" style="padding-left:0px!important;padding-right:0px!important">
						<div class="item item-thumbnail section-item">
							<div id="hover-area">
								<div class="item-image">
									<img src="{{ $product->picture }}" >
								</div>
								<div class="item-info">
									<h4 class="item-title">
										{{ $product->name }}
									</h4>
									<p class="preview-description">
										{{ $product->description }}
									</p>
									
								</div>
							</div>
							<div class="hover-wrapper">
								<div class="wrapper-section">
									
								</div>
								<div class="centered-hover-button">
									<a href="{{ route('user.product.detail', $product->slug_url) }}" class="btn hoverable-white-button">
										View Details
									</a>
								</div>
								<div class="bottom-hover-button item-info">
									<button onclick="addToCart({{ $product->id }})" class="btn btn-yellow" >
										Add to Cart
									</button>
									<a href="#" class="btn btn-buy-blue">
										Buy
									</a>
								</div>
								
							</div>

						</div>
						
					</div>
					@endforeach
				</div>

				@endif
			</div>
			@if ($products)
			{{ $products->links() }}
			@endif
		</div>
		<!-- END products -->
	</div>
	<!-- END container -->
</div>
<!-- END #section -->
@endsection

@push('pageRelatedJs')
<script type="text/javascript">
	function addToCart($productId) {
		$.ajax({
			headers: {
				'X-CSRF-Token': $('input[name="_token"]').val()
			},
			url: "{{ route('user.product.cart') }}",
			method: "POST",
			data: { product_id : $productId,
				qty: 1 },
				success: function (data) {
					if (data.message == null) {
						$.gritter.add( 
						{  
							text: '<i class="fa fa-info"></i> Added to Cart Successfully',  
							sticky: false,  
							time: ""
						});
						$('#cart-badge').text(data.qty);
					} else {
						$.gritter.add( 
						{    
							text: '<i class="fa fa-exclamation-circle"></i> ' + data.message,  
							sticky: false,  
							time: "",
							class_name:'gritter-light'
						})
					}
					setTimeout(function() {
						$('.alert').fadeOut('fast');
					}, 2000);
				}
			});
	}
</script>
@endpush