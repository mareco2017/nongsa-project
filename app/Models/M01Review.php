<?php

namespace App\Models;

use App\Models\M01Review;
use App\Models\M01User;
use App\Models\M02Product;
use Illuminate\Database\Eloquent\Model;

class M01Review extends Model
{
	protected $table = "m01_reviews";

	protected $fillable = [
		'content'
	];

	protected $guarded = ['m01_user_id','m02_product_id','created_at'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			
		];
		return $rules;
	}

	public function m01_user() 
	{
		return $this->belongsTo(M01User::class);
	}

	public function m02_product() 
	{
		return $this->belongsTo(M02Product::class);
	}
}