<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Enums\UserRole;
use App\Helpers\SessionHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePassword extends Controller
{
    public function edit()
    {
        if(Auth::user()->role == UserRole::CUSTOMER) {
            return view('frontend.auth.profile');
        }elseif(Auth::user()->role == UserRole::VENDOR) {
            return view('frontend.auth.vendor_profile');
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]
    );

        $user = $request->user();
        if (!Hash::check($request->old_password, $user->password))
            return back()->withErrors(['old_password' => 'Incorect old password. Please enter the correct password']);
            $user->password = bcrypt($request->password);
            if ($request->googleotp) {
                $user->auth_option = 1;
                $user->googleotp = 1;
                $user->emailotp = 0;
            } elseif ($request->emailotp) {
                $user->auth_option = 2;
                $user->emailotp = 1;
                $user->googleotp = 0;
            } else {
                $user->auth_option = 0;
                $user->emailotp = 0;
                $user->googleotp = 0;
            }
            $user->save();
            SessionHelper::setMessage('Change password succeed');
            if($user->role == UserRole::CUSTOMER) {
                return redirect()->route('user.change.profile');
            }elseif($user->role == UserRole::VENDOR) {
                return redirect()->route('vendor.change.password');
                
            }
        }
    }
