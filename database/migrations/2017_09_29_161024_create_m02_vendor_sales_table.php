<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM02VendorSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m02_vendor_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_order_id');
            $table->unsignedInteger('m02_outlet_id');
            $table->longText('resi_no')->nullable();
            $table->decimal('grand_total', 24, 2)->default(0);
            $table->longText('delivery_status')->nullable();

            $table->foreign('m02_outlet_id')->references('id')->on('m02_outlets')->onDelete('cascade');
            $table->foreign('m01_order_id')->references('id')->on('m01_orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m02_vendor_sales');
    }
}
