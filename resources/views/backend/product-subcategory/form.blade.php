@extends('layouts.backend.master')
@section('content')

<?php 

use Carbon\Carbon;

?>

@if (isset($productSubcategory))
{!! Form::model($productSubcategory, ['url' => route('admin.product-subcategories.update', ['productSubcategory' => $productSubcategory]),'method' => 'PUT', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@else
{!! Form::open(['url' => route('admin.product-subcategories.save'),'method' => 'POST', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@endif

<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Subategori Produk</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Subkategori Produk</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <form class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('name', 'Nama Subkategori')}}
                                        {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Nama Kategori','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('name', 'Pilih Kategori')}}
                                        {{ Form::select('m03_category_id', $productCategories, null,array('class' => 'form-control', 'placeholder' => 'Pilih kategori...','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        <button type="submit" class="btn red btn-lg"><span class="fa fa-save"></span> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
@endsection