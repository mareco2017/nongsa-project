@extends('layouts.backend.master')
@section('content')

<?php 

use App\Helpers\Enums\OutletStatus;

?>

@if (isset($outlet))
{!! Form::model($outlet, ['url' => route('admin.outlets.update', ['outlet' => $outlet]),'method' => 'PUT', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@else
{!! Form::open(['url' => route('admin.outlets.save'),'method' => 'POST', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@endif

<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Outlet Profile</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Profile</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('outlet_no','No. Izin Outlet :')}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::text('outlet_no', null, array('class' => 'form-control', 'placeholder' => 'Outlet No','data-parsley-required' => 'true'))}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::label('m03_outlet_category_id','Jenis Usaha :')}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::select('m03_outlet_category_id', $outletCategory, null,array('class' => 'form-control', 'placeholder' => 'Pilih Usaha...','data-parsley-required' => 'true'))}}
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('blok','Blok :')}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::text('blok', null, array('class' => 'form-control', 'placeholder' => 'Blok','data-parsley-required' => 'true'))}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::label('status','Status :')}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::select('status', OutletStatus::getArray(), null, array('class' => 'form-control', 'placeholder' => 'Pilih Status Outlet...','data-parsley-required' => 'true'))}}
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('global_commission','Komisi Global :')}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::number('global_commission', null, array('class' => 'form-control', 'placeholder' => 'Komisi Global dalam %','data-parsley-required' => 'true'))}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('username', 'Username')}}
                                        {{ Form::text('username', null, array('class' => 'form-control', 'placeholder' => 'Username','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('password', 'Password')}}
                                        {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('email', 'Email')}}
                                        {{ Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Email','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('name', 'Nama Outlet')}}
                                        {{ Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Nama Outlet','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('address', 'Alamat')}}
                                        {{ Form::text('address', null, array('class' => 'form-control', 'placeholder' => 'Alamat','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('phone', 'Telepon')}}
                                        {{ Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Telepon','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('profile_pic', 'Gambar Toko')}}<br>
                                        @if(isset($outlet))
                                        <img src="{{ asset($outlet->profile_pic) }}" style="width: 210px; height: 210px" id="previewImage">
                                        @else
                                        <img src="{{ asset('assets/img/noimage.png') }}" style="width: 210px; height: 210px" id="previewImage">
                                        @endif
                                        <input type="file" name="profile_pic" id="imageSelect">
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        <button type="submit" class="btn red btn-lg"><span class="fa fa-save"></span> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function(){

        function readURL(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageSelect").change(function(){
            console.log('Change!');
            readURL(this);
        });
    });
</script>
@endpush