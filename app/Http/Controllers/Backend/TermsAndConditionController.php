<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M03TermsAndCondition;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class TermsAndConditionController extends Controller
{
    protected $view = 'backend.termsandcondition';
    protected $route = 'admin.terms-and-condition';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $tnc = M03TermsAndCondition::whereNull('m02_outlet_id')->first();
        return view($this->view.'.index')
        ->with('adminTermList', 'selected')
        ->with('settingClass', 'start active open')
        ->with('adminTermClass', 'start active open')
        ->with('tnc',$tnc);
    }

    public function store(Request $request) {
        $this->validate($request, M03TermsAndCondition::rules());
        try {

            DB::beginTransaction();
            $tnc = M03TermsAndCondition::whereNull('m02_outlet_id')->first();
            if(!$tnc) $tnc = new M03TermsAndCondition();
            $tnc->content = $request->content;
            $tnc->created_at = Carbon::now()->format('Y-m-d H:i:s');
            if (!$tnc->save()) {
                return back()->withInput()->withErrors($tnc->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Syarat dan Ketentuan berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

}