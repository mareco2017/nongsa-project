<?php

namespace App\Models;

use App\Helpers\FileHelper;
use App\Models\M01Feedback;
use App\Models\M01User;
use App\Models\M02Outlet;
use App\Models\M03OutletCategory;
use Illuminate\Database\Eloquent\Model;

class M02Product extends Model
{
	protected $table = "m02_products";

	protected $fillable = [
		'code','name','slug_url','description','retail_price','distributor_price','commission','picture','stock','total_qty_in','total_qty_out', 'm03_category_id', 'm03_subcategory_id'
	];

	protected $guarded = ['m02_outlet_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
		];
		return $rules;
	}
	
	public function m02_outlet() 
	{
		return $this->belongsTo(M02Outlet::class);
	}

	public function getPictureAttribute($value){
        if(!$value) return '';
        if(strpos($value, 'http') !== false) return $value;
        return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
    }
}