@extends('layouts.backend.master')
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="form-group">
			<h1>Daftar Komplain</h1>
			@include('includes.session_message')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-inverse">
						<div class="panel-body">
							<table class="table table-striped table-hover fullTable" id="feedback-list" width="100%">
								<thead>
									<tr>
										<th>No.</th>
										<th>Nama</th>
										<th>Email</th>
										<th>Subject</th>
										<th>Description</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($feedbacks as $key=>$feedback)
									<tr>
										<td>
											{{ ++$key }}
										</td>
										<td>
											{{ $feedback->name }}
										</td>
										<td>
											{{ $feedback->email }}
										</td>
										<td>
											{{ $feedback->subject }}
										</td>
										<td>
											{{ $feedback->description }}
										</td>
										<td>
											@if ($feedback->status == 0)
											Waiting...
											@else
											Replied
											@endif
										</td>
										<td>
											<button type="submit" class="btn green btn-xm" value="{{ $feedback }}">Select</button>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							<script type="text/javascript">

								$("button").click(function() {
									var currentValue = $(this).val();
									var convertedObj = JSON.parse(currentValue);
									var emailValue = convertedObj.email;
									var userID = convertedObj.id;
									console.log(convertedObj.id);
									$("#email").html(emailValue);
									$('#emailVal').val(emailValue);
									$('#userID').val(userID);
								});

							</script>
							<div class="col-md-12">
								{!! Form::open(['url' => route('admin.send.feedback.save'),'method' => 'POST', 'enctype'=>'multipart/form-data']) !!}
								<div class="content-header-gray">
									<label>Reply to :&nbsp</label><label id="email" name="email"></label>
									<input type="hidden" name="email" id="emailVal">
									<input type="hidden" name="id" id="userID">
								</div>
								<textarea class="content-text-area" id="feedbackmsg" name="content" placeholder="Enter any text here.." ></textarea>
								<div class="form-group col-md-6">
									<button type="submit" id="replyemail" class="btn green btn-xm">Reply</button>
								</div>
								{{ Form::close() }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection