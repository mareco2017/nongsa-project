<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM01OrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m01_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_user_id');
            $table->date('created_at')->nullable();
            $table->date('expired_at');
            $table->longText('invoice_no')->nullable();
            $table->decimal('grand_total', 24, 2)->default(0);
            $table->unsignedInteger('m03_shipping_method_id');
            $table->longText('address')->nullable();
            $table->unsignedInteger('status');

            $table->foreign('m01_user_id')->references('id')->on('m01_users')->onDelete('cascade');
            $table->foreign('m03_shipping_method_id')->references('id')->on('m03_shipping_methods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m01_orders');
    }
}
