<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM01FeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m01_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m02_outlet_id')->index()->nullable();
            $table->longText('name');
            $table->longText('email');
            $table->longText('subject');
            $table->longText('description');
            $table->integer('status')->default(0);
            $table->timestamp('created_at')->nullable();

            $table->foreign('m02_outlet_id')->references('id')->on('m02_outlets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m01_feedbacks');
    }
}
