<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\SessionHelper;
use App\Models\M01Feedback;
use App\Models\M01User;
use App\Models\M02Outlet;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Facades\Datatables;

class FeedbackController extends Controller
{
    protected $view = 'frontend.vendor.feedback';
    protected $route = 'vendor.feedbacks';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $feedbacks = M01Feedback::where('m02_outlet_id',Auth::user()->m02_outlet->id)->get();
        return view($this->view.'.index')
        ->with('feedbackList', 'selected')
        ->with('feedbackClass', 'start active open')
        ->with('feedbacks',$feedbacks);
    }

    public function sendFeedback(Request $request) {
        $data = array('name' => Auth::user()->name, 'content' => $request->content,'email' => $request->email);
        Mail::send('backend.mail.feedback', $data, function ($message) use ($data)
        {
            $emailCustomer = $data['email'];
            $message->from('nongsa.dev@gmail.com')->subject('Feedback Reply');
            $message->to($emailCustomer);
        });
        $feedback = M01Feedback::where('id','=', $request->id)->first();
        $feedback->status = 1;
        $feedback->save();
        
        SessionHelper::setMessage("Feedback berhasil dikirim.");
        return redirect()->route($this->route.'.index');

    }
}