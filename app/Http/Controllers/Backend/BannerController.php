<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01Picture;
use App\Models\M02Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class BannerController extends Controller
{
    protected $view = 'backend.banner';
    protected $route = 'admin.banners';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $banner = M02Banner::orderBy('is_first','desc')->where('title','=',null)->get();
        $storeBanner = M02Banner::where('title','=','store')->first();
        $productBanner = M02Banner::where('title','=','product')->first();
        return view($this->view.'.index')
        ->with('bannerList', 'selected')
        ->with('banners', $banner)
        ->with('storeBanner', $storeBanner)
        ->with('productBanner', $productBanner)
        ->with('bannerClass', 'start active open');
    }

    public function store(Request $request) {
        $this->validate($request, M01Picture::rules());

        try {

            DB::beginTransaction();

            $picture = new M01Picture();
            if ($request->file('url')) {
                $upload = FileHelper::upload(request(), 'url', 'jpeg,bmp,png');
                $picture->url = $upload;
            }
            $picture->save();

            $banner = new M02Banner();
            $banner->m01_picture()->associate($picture);
            $banner->m01_picture_id = $picture->id;
            if($request->title ) $banner->title = $request->input('title');
            if (!$banner->save()) {
                return back()->withInput()->withErrors($banner->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Form succesfully created");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function destroy($banner){
        try {

            DB::beginTransaction();
            $banner->m01_picture->delete();
            $banner->delete();

            DB::commit();
            SessionHelper::setMessage("Form succesfully deleted");
            return redirect()->route($this->route.'.index');
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function setFirstBanner($banner){
        try {
            DB::beginTransaction();
            $oldBanner = M02Banner::where('is_first','=', 1)->first();
            $oldBanner->is_first = 0;
            $oldBanner->save();

            $banner->is_first = 1;
            $banner->save();

            DB::commit();
            SessionHelper::setMessage("Banner succesfully set");
            return redirect()->route($this->route.'.index');
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function storeBanner(Request $request) {
        try {
            DB::beginTransaction();
            $oldStoreBanner = M02Banner::where('title','=', 'store')->first();
            if($oldStoreBanner) $oldStoreBanner->delete();
           
            $picture = new M01Picture();
            if ($request->file('url')) {
                $upload = FileHelper::upload(request(), 'url', 'jpeg,bmp,png');
                $picture->url = $upload;
            }
            $picture->save();

            $banner = new M02Banner();
            $banner->m01_picture()->associate($picture);
            $banner->m01_picture_id = $picture->id;
            $banner->title = 'store';
            if (!$banner->save()) {
                return back()->withInput()->withErrors($banner->errors());
            }

            DB::commit();
            SessionHelper::setMessage("store banner succesfully updated");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function productBanner(Request $request) {
        try {
            DB::beginTransaction();
            $oldStoreBanner = M02Banner::where('title','=', 'product')->first();
            if($oldStoreBanner) $oldStoreBanner->delete();
           
            $picture = new M01Picture();
            if ($request->file('url')) {
                $upload = FileHelper::upload(request(), 'url', 'jpeg,bmp,png');
                $picture->url = $upload;
            }
            $picture->save();

            $banner = new M02Banner();
            $banner->m01_picture()->associate($picture);
            $banner->m01_picture_id = $picture->id;
            $banner->title = 'product';
            if (!$banner->save()) {
                return back()->withInput()->withErrors($banner->errors());
            }

            DB::commit();
            SessionHelper::setMessage("product banner succesfully updated");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

}