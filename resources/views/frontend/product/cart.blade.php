@extends('layouts.frontend.master')
@section('content')
@include('includes.modal_input')
@include('includes.modal_delete')
<!-- BEGIN #section -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container">
		{{ csrf_field() }}
		<div class="checkout">
			@include('includes.session_message')
			<div class="checkout-body">
				@if (($carts) && (count($carts) > 0))
				<div class="table-responsive">
					<table class="table table-cart">
						<thead>
							<tr>
								<th >Product Name</th>
								<th class="text-center">Price</th>
								<th class="text-center">Quantity</th>
								<th class="text-center">Total</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach ($carts as $cart)
							<tr>
								<td class="cart-product">
									<div class="product-img">
										<img src="{{ $cart->m02_product->picture }}" alt>
									</div>
									<div class="product-info">
										<div class="title">{{ $cart->m02_product->name }}</div>
										<div class="desc">{{ $cart->m02_product->description }}</div>
									</div>
								</td>
								<td class="cart-price text-center">
									{{ round($cart->m02_product->retail_price,0) }}
								</td>
								<td class="cart-qty text-center">
									<div class="cart-qty-input">
										<a href="#" class="qty-control left disabled" data-click="decrease-qty" data-target="#qty{{ $cart->id}}">
											<i class="fa fa-minus"></i>
										</a>
										<input type="text" name="qty" value="{{ $cart->qty }}" class="form-control" id="qty{{ $cart->id}}" onchange="updateTotalPrice({{$cart}}, this.value)" onkeyup="this.onchange()" onpaste="this.onchange()" oninput="this.onchange()">
										<a href="#" class="qty-control right disabled" data-click="increase-qty" data-target="#qty{{ $cart->id}}">
											<i class="fa fa-plus"></i>
										</a>
									</div>
								</td>
								<td class="cart-total text-center" id="total{{ $cart->id}}" >
									{{ round($cart->subtotal,0) }}
								</td>
								<td class="cart-item-close">
									<a data-url="{{ route('user.cart.delete', $cart) }}" data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-message="Would you like to remove this product?" data-original-title title> x </a>
								</td>
							</tr>
							@endforeach
							<tr>
								<td class="cart-summary" colspan="5">
									<div class="summary-container">
										<div class="summary-row total">
											<div class="field">
												Total
											</div>
											<div class="value" id="total-price">
												{{ $totalPrice }}
											</div>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				@else
				<div class="section-title clearfix">
					Your Cart is Empty
				</div>
				@endif
				@if (($carts) && (count($carts) > 0))
				<div>
					<br>
					<a href={{ route('user.cart.checkout') }} type="submit" class="btn btn-buy-blue float-right">Checkout</a>
				</div>
				@endif
			</div>
		</div>
	</div>
	<!-- END container -->
</div>
<!-- END #section -->
@endsection

@push('pageRelatedJs')
<script type="text/javascript">
	function updateTotalPrice($cart, $qty) {
		$currentTotal = parseInt($('#total-price').text());
		$currentSubtotal = parseInt($('#total' + $cart.id).text());
		$totalFieldId = "total" + $cart.id;
		$newSubtotal = $cart.m02_product.retail_price * $qty;
		$('#'+$totalFieldId).html($newSubtotal);
		$newTotal = $currentTotal - $currentSubtotal + $newSubtotal;
		$('#total-price').text($newTotal);
		updateCart($cart, $qty);
	}

	function updateCart($cart, $qty) {
		console.log($cart,$qty);
		$.ajax(
		{
			headers: {
				'X-CSRF-Token': $('input[name="_token"]').val()
			},
			url:  "{{ route('user.cart.update') }}",
			data: { 'cart' : $cart, 'qty' : $qty},
			type: 'POST',
			success: function(result) {
			}
		});
	}
</script>
@endpush