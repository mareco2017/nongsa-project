<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\UserRole;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M02Outlet;
use App\Models\M03Credit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class CreditController extends Controller
{
    protected $view = 'backend.credit';
    protected $route = 'admin.credits';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $credit = M02Outlet::select('m01_user_id', DB::raw('CONCAT (outlet_no, " - " , name) AS creditList'))
        ->pluck('creditList','m01_user_id');
        return view($this->view.'.index')
        ->with('creditList', 'selected')
        ->with('listOfCredit', $credit)
        ->with('creditClass', 'start active open');
    }

    public function listAllCredit() {
        $creditList = M03Credit::with(['m01_user','m02_invoice'])->get();
        return Datatables::of($creditList)->make(true);
    }

}