@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
@include('includes.modal_delete')
<?php 

use App\Helpers\Enums\OutletStatus;



?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="form-group">
			<h1>Product Details: Inventory</h1>
			@include('includes.session_message')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<div class="panel-heading-btn">
								@if(Auth::user()->m02_outlet->status == OutletStatus::TERSEWA)
								<a href="{{ route('vendor.products.new') }}" class="btn btn-m btn-primary pull-right"><span class="fa fa-plus"></span> Tambah Produk</a>
								@endif
							</div>
							<h4>Daftar Produk</h4>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-hover fullTable" id="product-list" width="100%">
								<thead>
									<tr>
										<th>No.</th>
										<th>Kode</th>
										<th>Nama Produk</th>
										<th>Masuk</th>
										<th>Keluar</th>
										<th>Sisa Stok</th>
										<th>Action</th>
									</tr>
									<tr id="filter-row">
										<th></th>
										<th class="tr-align-center">Kode</th>
										<th class="tr-align-center">Nama Produk</th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
							<div class="panel panel-inverse">
								<div class="panel-heading">
									<h4>Form Inventory</h4>
								</div>
							</div>
							<div class="col-md-12">
								<div class="portlet light bordered">
									{!! Form::open(['url' => route('vendor.inventories.save'),'method' => 'POST', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
									<div class="portlet-body">
										<!-- <form class="form-horizontal"> -->
											<div class="row">
												<div class="row">
													<div class="form-group col-md-3"></div>
													<div class="form-group col-md-6">
														{{ Form::label('m02_product_id', 'Nama Produk')}}
														{{ Form::select('m02_product_id', $productsName, null, array('class' => 'form-control', 'placeholder' => 'Pilih Produk...','data-parsley-required' => 'true'))}}
													</div>
													<div class="form-group col-md-3"></div>
												</div>
											</div>
											<!-- </form> -->
										</div>

										<div class="portlet-">
											<!-- <form class="form-horizontal"> -->
												<div class="row">
													<div class="row">
														<div class="form-group col-md-3"></div>
														<div class="form-group col-md-6">
															{{ Form::label('created_at', 'Tanggal Masuk')}}
															{{ Form::text('created_at', \Carbon\Carbon::now(), array('class' => 'form-control dateonlypicker', 'placeholder' => 'Username','data-parsley-required' => 'true'))}}
														</div>
														<div class="form-group col-md-3"></div>
													</div>
													<div class="row">
														<div class="form-group col-md-3"></div>
														<div class="form-group col-md-6">
															{{ Form::label('reference_no', 'No. Referensi')}}
															{{ Form::text('reference_no', null, array('class' => 'form-control', 'placeholder' => 'Masukkan nomor referensi','data-parsley-required' => 'true'))}}
														</div>
														<div class="form-group col-md-3"></div>
													</div>
													<div class="row">
														<div class="form-group col-md-3"></div>
														<div class="form-group col-md-6">
															{{ Form::label('remark', 'Keterangan')}}
															{{ Form::textarea('remark', null, array('class' => 'form-control', 'placeholder' => 'Masukkan keterangan','data-parsley-required' => 'true'))}}
														</div>
														<div class="form-group col-md-3"></div>
													</div>
													<div class="row">
														<div class="form-group col-md-3"></div>
														<div class="form-group col-md-6">
															{{ Form::label('qty_in', 'Kuantitas Masuk')}}
															{{ Form::number('qty_in', null, array('class' => 'form-control', 'placeholder' => 'Masukkan nilai kuantitas masuk','data-parsley-required' => 'true'))}}
														</div>
														<div class="form-group col-md-3"></div>
													</div>

												</div>
												<div class="form-group">
													<div class="row">
														<div class="form-group col-md-3"></div>
														<div class="form-group col-md-6">
															<button type="submit" class="btn red btn-lg"><span class="fa fa-save"></span> Simpan</button>
														</div>
													</div>
												</div>
												<!-- </form> -->
											</div>
											{{ Form::close() }}
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		@endsection
		@push('pageRelatedJs')
		<script type="text/javascript">
			$(document).ready(function () {
				var productDataTable = $('#product-list').DataTable(
				{
					dom: "lrtip",
					orderCellsTop: true,
					responsive: true,
					serverSide: true,
					ajax: {
						url:  "{{ route('vendor.products.list') }}",
						data: { '_token' : '{{ csrf_token() }}'},
						type: 'POST',
					},
					columns: [
					{
						"data": "id",
						render: function (data, type, row, meta) {
							return meta.row + meta.settings._iDisplayStart + 1;
						}
					},
					{ data: 'code', name: 'code', 'className': 'text-center' },
					{ data: 'name', name: 'name', 'className': 'text-center' },
					{ data: 'total_qty_in', name: 'total_qty_in', 'className': 'text-center' },
					{ data: 'total_qty_out', name: 'total_qty_out', 'className': 'text-center' },
					{ data: 'stock', name: 'stock', 'className': 'text-center' },
					{ data: 'action', name: 'action', 'className': 'text-center', orderable: false, searchable: false  }
					]
				});

				$("#product-list thead tr#filter-row th").each(function (index) {
					var column = $(this).text();

					switch(column){
						case "Kode":
						case "Nama Produk":
						case "Masuk":
						case "Keluar":
						case "Stock":
						var input = '<input type="text" class="text-input form-control" width="100%" placeholder="Search By ' + column + '" />';

						$(this).html(input);
						break;
					}
				});

				$(".text-input").on("keyup change", function () { 
					productDataTable
					.column($(this).parent().index()) 
					.search(this.value) 
					.draw(); 
				});

			});
		</script>
		@endpush