@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN section-title -->
        @include('includes.session_message')
        <h1 class="section-title clearfix">Payment<br>
        </h1>
        <br>
        <!-- END section-title -->
        <!-- BEGIN row -->
        <div class="row row-space-10">
            <form class="form-horizontal" method="POST" action="{{ route('user.order.payment.save', $order->id) }}">
                {{ csrf_field() }}
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="form-group m-b-15">
                        <input type="text" id="payment_date" class="form-control dateonlypicker" name="payment_date" placeholder="Select Date" required>
                    </div>
                    <div class="form-group m-b-15 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="number" id="amount" class="form-control" name="amount" placeholder="Type amount of payment" required>
                    </div>
                    <div class="form-group m-b-15">
                        Upload Proof of Payment
                    </div>
                    <div class="form-group m-b-15">
                        <img src="{{ asset('assets/img/noimage.png') }}" style="width: 210px; height: 210px" id="previewImage">
                        <input type="file" name="picture" id="imageSelect" class="form-control no-border">
                    </div>
                    <div class="login-buttons form-group">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Submit</button>
                    </div>
               </div>
               <div class="col-md-4"></div>
           </form>
       </div>
        <!-- END row -->
    </div>
    <!-- END container -->
</div>
<!-- END #promotions -->
@endsection

@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function(){

        function readURL(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageSelect").change(function(){
            console.log('Change!');
            readURL(this);
        });
    });
</script>
@endpush