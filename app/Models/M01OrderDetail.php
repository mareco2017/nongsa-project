<?php

namespace App\Models;

use App\Models\M01Order;
use App\Models\M01User;
use App\Models\M02Product;
use Illuminate\Database\Eloquent\Model;

class M01OrderDetail extends Model
{
	protected $table = "m01_order_details";

	protected $fillable = [
		'qty','price','subtotal'
	];

	protected $guarded = ['m01_order_id','m02_product_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
		];
		return $rules;
	}

	public function m01_order() 
	{
		return $this->belongsTo(M01Order::class);
	}

	public function m02_product() 
	{
		return $this->belongsTo(M02Product::class);
	}

}