@extends('layouts.app')

@section('content')
<div class="login-content">
    <form class="form-horizontal" method="POST" action="{{ route('admin.verify.register.google2f', $user) }}">
        {{ csrf_field() }}

        <div class="form-register-success">
            <h3>You have been successfully registered.</h3>
            <p>Please scan the barcode below with Google Authenticator Apps.</p>
        </div>
        <div class="form-steps">
            <div class="form-group">
                @include('flash::message')
            </div>
            <div style="text-align: center">
                <img src="{{ $google2fa_url }}" alt="" style="margin-bottom: 25px;">
            </div>
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon"> <i class="fa fa-lock"></i> </div>
                    <input type="text" class="form-control" name="google_auth_code" id="google_auth_code" placeholder="Enter Google Authenticator Code" autocomplete="off" autofocus /> 
                </div>
            </div>
            <div class="login-buttons form-group">
                <button type="submit" class="btn btn-success btn-block btn-login"> <i class="entypo-right-open-mini"></i> Complete Registration
                </button>
            </div>
        </div>
        <div class="text-center text-inverse">
            <a href="{{ route('admin.login.form') }}" class="link"> <i class="entypo-lock"></i> Return to Login Page
            </a>
        </div>
        <hr />
        <p class="text-center text-inverse">
            PT Mareco Prima Mandiri All Right Reserved 2017 &trade;
        </p>
    </form>
</div>
@endsection
