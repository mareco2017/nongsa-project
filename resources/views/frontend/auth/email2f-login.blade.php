@extends('layouts.frontend.master')
@section('content')
<div class="section-container bg-white">
    <div class="container">
        <h1 class="section-title clearfix">
            Login<br>
            <small>Verify Email Authenticator.</small>
        </h1>
        <br>
        <div class="row row-space-10">
            <form class="form-horizontal" method="POST" action="{{ route('user.verify.register.email.code') }}">
                {{ csrf_field() }}
                <div class="col-md-4"></div>
                <div class="col-md-4" style="text-align: center">
                    <div class="form-register-success">
                        <i class="entypo-check"></i>
                        <p>You have been successfully login.</p>
                        <p>We have emailed you the confirmation code.</p>
                    </div>
                    <div class="form-group m-b-15">
                        @include('flash::message')
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"> <i class="fa fa-lock"></i> </div>
                            <input type="text" class="form-control" name="emailotp" id="emailotp" placeholder="Enter Confirmation Code" data-mask="[a-zA-Z0-1\.]+" data-is-regex="true" autocomplete="off" required/> 
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-block btn-login"> <i class="entypo-right-open-mini"></i> Submit
                        </button>
                    </div>
                </div>
                <div class="col-md-4"></div>

            </form>
        </div>
    </div>
</div>
@endsection
