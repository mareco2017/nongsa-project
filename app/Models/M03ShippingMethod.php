<?php

namespace App\Models;

use App\Helpers\FileHelper;
use App\Models\M02Outlet;
use Illuminate\Database\Eloquent\Model;

class M03ShippingMethod extends Model
{
	protected $table = "m03_shipping_methods";

	protected $fillable = [
		'name','price','created_at'
	];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'name' => 'required',
		];
		return $rules;
	}

}