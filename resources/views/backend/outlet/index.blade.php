@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
@include('includes.modal_delete')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Outlet Profile</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Outlet</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="{{ route('admin.outlets.new') }}" class="btn btn-m btn-primary pull-right"><span class="fa fa-plus"></span> Tambah Outlet</a>
                        </div>
                        <h4>Daftar outlet</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover fullTable" id="outlet-list" width="100%">
                            <thead>
                                <tr>
                                    <th>No. Outlet</th>
                                    <th>Nama Outlet</th>
                                    <th>Email</th>
                                    <th>Telepon</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                <tr id="filter-row">
                                    <th class="tr-align-center">No. Outlet</th>
                                    <th class="tr-align-center">Nama Outlet</th>
                                    <th class="tr-align-center">Email</th>
                                    <th class="tr-align-center">Telepon</th>
                                    <th class="tr-align-center">Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var outletDataTable = $('#outlet-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.outlets.list') }}",
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'outlet_no', name: 'outlet_no', 'className': 'text-center' },
            { data: 'name', name: 'name', 'className': 'text-center' },
            { data: 'email', name: 'email', 'className': 'text-center' },
            { data: 'phone', name: 'phone', 'className': 'text-center' },
            { data: 'status', name: 'status', 'className': 'text-center' },
            { data: 'action', name: 'action', 'className': 'text-center', orderable: false, searchable: false  }
            ]
        });

        $("#outlet-list thead tr#filter-row th").each(function (index) {
            var column = $(this).text();

            switch(column){
                case "No. Outlet":
                case "Nama Outlet":
                case "Email":
                case "Telepon":
                var input = '<input type="text" class="text-input form-control" width="100%" placeholder="Search By ' + column + '" />';

                $(this).html(input);
                break;

                case "Status": 
                var select = '{{ Form::select("status", $outletStatus, null, array("id" => "status", "class" => "select-input form-control")) }}' 

                $(this).html(select); 
                break; 
            }
        });
        $(".select-input").prepend('<option value="" selected="selected">All</option>');

        $(".text-input").on("keyup change", function () { 
            outletDataTable
            .column($(this).parent().index()) 
            .search(this.value) 
            .draw(); 
        }); 


        $(".select-input").on("change", function () { 
            outletDataTable
            .column($(this).parent().index()) 
            .search(this.value) 
            .draw();
        }); 


    });
</script>
@endpush