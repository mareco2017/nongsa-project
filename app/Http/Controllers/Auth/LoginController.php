<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Enums\AuthOption;
use App\Helpers\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerification;
use App\Models\M01User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';
    protected $vendorTo = '/vendor';
    protected $loginView = 'admin.login';
    protected $customerTo = '/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if ($this->guard()->user()->role == UserRole::CUSTOMER) {
                $this->guard()->logout();
                return redirect()->back()
                ->withErrors(['email' => 'Unauthorized access!']);
            }

            if ($this->guard()->user()->role == UserRole::VENDOR) {
                if ($this->guard()->user()->auth_option == AuthOption::GOOGLE_AUTH) {
                    return redirect()->route('admin.verify.login.form', [$this->guard()->user()]);
                } elseif ($this->guard()->user()->auth_option == AuthOption::EMAIL) {
                    $this->guard()->user()->emailotp = strtoupper(str_random(6));
                    $email = new EmailVerification(new M01User(['emailotp' => $this->guard()->user()->emailotp, 'name' => $this->guard()->user()->name]));
                    $this->guard()->user()->save();
                    Mail::to($this->guard()->user()->email)->send($email);
                    return redirect()->route('admin.verify.login.email', [$this->guard()->user()]);
                }
            }

            return $this->sendLoginResponse($request);
        }
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('admin.login.form');
    }

}
