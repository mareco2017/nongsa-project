@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN section-title -->
		@include('includes.session_message')
		<h1 class="section-title clearfix">CONTACT US<br>
		</h1>
		<br>
		<!-- END section-title -->
		<!-- BEGIN row -->
        <div class="row row-space-10">
            <form class="form-horizontal" method="POST" action="{{ route('user.contact-us.save') }}">
                {{ csrf_field() }}
                <div class="col-md-1"></div>
                <div class="col-md-4">
                	<div class="form-group m-b-15">
                        <input id="name" class="form-control" name="name" placeholder="Name" required autofocus>
                    </div>
                    <div class="form-group m-b-15 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group m-b-15">
                        <textarea id="subject" class="form-control" rows="5" name="subject" placeholder="Messages" required autofocus></textarea>
                    </div>
                    <div class="login-buttons form-group">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Send</button>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </form>
            {{ csrf_field() }}
            <div class="col-md-5">
                <div class="google-maps">
                    @if($contactUs)
                    <iframe width="600" height="400" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDcoLrlKTUKlAM4-grY3ihbdBNL1NCfmnE&q={{ $contactUs->latlong }}" allowfullscreen>
                    </iframe>
                    @else
                    <iframe width="600" height="400" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDcoLrlKTUKlAM4-grY3ihbdBNL1NCfmnE&q=Nongsa+Point+Marina+%26+Resort" allowfullscreen>
                    </iframe>

                    @endif
                    <address>
                        <strong>PT. Nongsa Green Park</strong><br />
                        Jl. Hang Lekiu, Nongsa<br />
                        Batam, Kepulauan Riau, Indonesia<br /><br />
                        <abbr title="Phone">Phone:</abbr> (081) 1777-270<br />
                        <abbr title="Fax">Fax:</abbr> (081) 1777-270<br />
                        <abbr title="Email">Email:</abbr> <a href="mailto:nongsaintialam@gmail.com">nongsaintialam@gmail.com</a><br />
                    </address>
                </div>
            </div>
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
</div>

@endsection