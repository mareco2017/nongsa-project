@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
@include('includes.modal_delete')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Invoice Tagihan</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Invoice</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="{{ route('admin.invoices.new') }}" class="btn btn-m btn-primary pull-right"><span class="fa fa-plus"></span> Tambah Invoice</a>
                        </div>
                        <h4>Daftar Invoice</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover fullTable" id="invoice-list" width="100%">
                            <thead>
                                <tr>
                                    <th>Kepada</th>
                                    <th>No. Invoice</th>
                                    <th>Tgl Invoice</th>
                                    <th>Perihal</th>
                                    <th>Total Tagihan</th>
                                    <th>Kategori</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                <tr id="filter-row">
                                    <th class="tr-align-center">Kepada</th>
                                    <th class="tr-align-center">No. Invoice</th>
                                    <th></th>
                                    <th class="tr-align-center">Perihal</th>
                                    <th></th>
                                    <th class="tr-align-center">Kategori</th>
                                    <th class="tr-align-center">Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var invoiceDataTable = $('#invoice-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.invoices.list') }}",
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'm01_user_id', name: 'm01_user.name', 'className': 'text-center' },
            { data: 'invoice_no', name: 'invoice_no', 'className': 'text-center' },
            { data: 'created_at', name: 'created_at', 'className': 'text-center' },
            { data: 'in_terms_of', name: 'in_terms_of', 'className': 'text-center' },
            { data: 'grand_total', name: 'grand_total', 'className': 'text-center' },
            { data: 'bill_category', name: 'bill_category', 'className': 'text-center' },
            { data: 'status', name: 'status', 'className': 'text-center' },
            { data: 'action', name: 'action', 'className': 'text-center', orderable: false, searchable: false  }
            ]
        });

        $("#invoice-list thead tr#filter-row th").each(function (index) {
            var column = $(this).text();

            switch(column){
                case "Kepada":
                case "No. Invoice":
                case "Perihal":
                var input = '<input type="text" class="text-input form-control" width="100%" placeholder="Search By ' + column + '" />';

                $(this).html(input);
                break;

                case "Kategori": 
                var select = '{{ Form::select("bill_category", $listOfCategory, null, array("id" => "bill_category", "class" => "select-input form-control")) }}' 
                $(this).html(select); 
                break; 

                case "Status": 
                var select = '{{ Form::select("status", $listOfStatus, null, array("id" => "status", "class" => "select-input form-control")) }}' 

                $(this).html(select); 
                break; 
            }
        });
        $(".select-input").prepend('<option value="" selected="selected">All</option>');

        $(".text-input").on("keyup change", function () { 
            invoiceDataTable
            .column($(this).parent().index()) 
            .search(this.value) 
            .draw(); 
        }); 

        $(".select-input").on("change", function () { 
            invoiceDataTable
            .column($(this).parent().index()) 
            .search(this.value) 
            .draw();
        }); 
        
    });
</script>
@endpush