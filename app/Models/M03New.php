<?php

namespace App\Models;

use App\Helpers\FileHelper;
use App\Models\M01User;
use Illuminate\Database\Eloquent\Model;

class M03New extends Model
{
	protected $table = "m03_news";

	protected $fillable = [
		'description','created_at','tags','picture','event_time','title'
	];

	protected $guarded = ['m01_user_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'description' => 'required',
		];
		return $rules;
	}

	public function m01_user() 
	{
		return $this->belongsTo(M01User::class);
	}
	
	public function getPictureAttribute($value){
        if(!$value) return '';
        if(strpos($value, 'http') !== false) return $value;
        return 'http://'.$_SERVER['SERVER_NAME'].'/'.$value;
    }
}