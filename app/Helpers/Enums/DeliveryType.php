<?php

namespace App\Helpers\Enums;

final class DeliveryType {

	const GOJEK = 0;
	const JNE = 1;
	const POS = 2;

	public static function getList() {
		return [
			DeliveryType::GOJEK,
			DeliveryType::JNE,
			DeliveryType::POS,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "GO-Jek";
			case 1:
				return "JNE";
			case 2:
				return "POS INDONESIA";
		}
	}

}

?>
