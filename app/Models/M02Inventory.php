<?php

namespace App\Models;

use App\Models\M02Product;
use Illuminate\Database\Eloquent\Model;

class M02Inventory extends Model
{
	protected $table = "m02_inventories";

	protected $fillable = [
		'reference_no','remark','qty_in','qty_out','created_at'
	];

	protected $guarded = ['m02_product_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
		];
		return $rules;
	}
}