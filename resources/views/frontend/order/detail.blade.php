@extends('layouts.frontend.master')
@section('content')
<?php

use App\Helpers\Enums\OrderStatus;

?>
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN section-title -->
		@include('includes.session_message')
		<h1 class="section-title clearfix">ORDER DETAIL<br>
		</h1>
		<br>
		<!-- END section-title -->
		<!-- BEGIN row -->
        <div class="row row-space-10">
            <table class="table table-striped table-hover fullTable">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <tr>
                        <td class="col-md-6">
                            No. Invoice :
                        </td>
                        <td class="col-md-6">
                            {{ $order->invoice_no }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Delivery Type :
                        </td>
                        <td>
                            {{ $order->m03_shipping_method->name }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Delivery Cost :
                        </td>
                        <td>
                            Rp {{ $order->m03_shipping_method->price }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Address :
                        </td>
                        <td>
                            {{ $order->address }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Grand Total :
                        </td>
                        <td>
                            Rp {{ round($order->grand_total,0) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Status :
                        </td>
                        <td>
                         {{ OrderStatus::getString($order->status) }}
                     </td>
                 </tr>
                 <tr>
                    <td colspan="2">
                        Product List :
                    </td>
                </tr>
            </div>
            <div class="col-md-3"></div>
        </table>
        @foreach ($outletIdList as $key => $outletId)
        <label>Resi No : {{ $resiNoList[$key] }}</label>
        <table class="table table-cart">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th class="text-center">Price</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Total</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orderList[$key] as $orderItem)
                <tr>
                    <td class="cart-product">
                        <div class="product-img">
                            <img src="{{ $orderItem->m02_product->picture }}" alt>
                        </div>
                        <div class="product-info">
                            <div class="title">{{ $orderItem->m02_product->name }}</div>
                            <div class="desc">{{ $orderItem->m02_product->description }}</div>
                        </div>
                    </td>
                    <td class="cart-price text-center">
                        {{ round($orderItem->price,0) }}
                    </td>
                    <td class="cart-price text-center">
                        {{ $orderItem->qty }}
                    </td>
                    <td class="cart-total text-center" >
                        {{ round($orderItem->subtotal,0) }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @endforeach
        @if ($order->status == 0) 
        <a href="{{ route('user.order.payment.index', $order->id) }}" type="submit" class="btn btn-info btn-outline float-right">Konfirmasi Pembayaran</a>
        @endif
    </div>
    <!-- END row -->
</div>
<!-- END container -->
</div>

@endsection