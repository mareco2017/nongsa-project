<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01Cart;
use App\Models\M01Review;
use App\Models\M01User;
use App\Models\M02Inventory;
use App\Models\M02Outlet;
use App\Models\M02Product;
use App\Models\M03ProductCategory;
use App\Models\M03ProductSubcategory;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class ProductController extends Controller
{
    protected $view = 'frontend.vendor.product';
    protected $route = 'vendor.products';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $productsName = M02Product::where('m02_outlet_id', Auth::user()->m02_outlet->id)->select('id', DB::raw('CONCAT(code, " - " , name) AS productList'))->pluck('productList','id');
        return view($this->view.'.index')
        ->with('productList', 'selected')
        ->with('productClass', 'start active open')
        ->with('productsName', $productsName);
    }

    public function listAllProduct() {
        $products = M02Product::where('m02_outlet_id',Auth::user()->m02_outlet->id)->get();
        return Datatables::of($products)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('vendor.products.edit', ['product' => $model->id]).'"
            class="btn btn-info btn-outline btn-sm">
            <span class="fa fa-eye"></span> View
            </a>
            <a data-url="'.route('vendor.products.destroy', ['product' => $model->id]).'"
            data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#product-list" data-message="Would you like to delete this product?" 
            class="btn btn-danger btn-outline btn-sm" >
            <span class="fa fa-trash-o"></span> Delete
            </a>';
        })
        ->make(true);
    }

    public function newForm() {
        $productCategories = M03ProductCategory::pluck('name', 'id');
        return view($this->view.'.form')
        ->with('productCategories', $productCategories)
        ->with('productSubcategories', [])
        ->with('method','POST');
    }

    public function edit($product) {
        $productCategories = M03ProductCategory::pluck('name', 'id');
        $productSubcategories = null;
        if ($product->m03_category_id) {
            $productSubcategories = M03ProductSubcategory::where('m03_category_id', $product->m03_category_id)
            ->pluck('name', 'id');
        }
        return view($this->view.'.form')
        ->with('product', $product)
        ->with('productCategories', $productCategories)
        ->with('productSubcategories', $productSubcategories)
        ->with('method', 'PUT');
    }

    public function store(Request $request) {
        $this->validate($request, M02Product::rules());
        try {

            DB::beginTransaction();
            $product = new M02Product();
            $product->m02_outlet_id = Auth::user()->m02_outlet->id;
            $product->code = $request->code;
            $product->name = $request->name;
            $product->m03_category_id = $request->m03_category_id;
            if ($request->m03_subcategory_id) {
                if ($request->m03_subcategory_id != 0) {
                    $product->m03_subcategory_id = $request->m03_subcategory_id;
                }
            }
            $product->slug_url =  str_slug($request->name, '-') . '-' . str_random(6);
            $product->description = $request->description;
            $product->retail_price = $request->retail_price;
            $product->distributor_price = $request->distributor_price;
            $product->commission = $request->commission;
            if ($request->file('picture')) {
                $upload = FileHelper::upload(request(), 'picture', 'jpg,jpeg,bmp,png');
                $product->picture = $upload;
            }
            if (!$product->save()) {
                return back()->withInput()->withErrors($product->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Produk berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function storeInventory(Request $request) {
        $this->validate($request, M02Inventory::rules());
        try {
            DB::beginTransaction();
            $inventory = new M02Inventory();
            $inventory->m02_product_id = $request->m02_product_id;
            $inventory->reference_no = $request->reference_no;
            $inventory->remark = $request->remark;
            $inventory->qty_in = $request->qty_in;
            $inventory->created_at = $request->created_at;
            if (!$inventory->save()) {
                return back()->withInput()->withErrors($inventory->errors());
            }
            else {
                $product = M02Product::where('id','=',$request->m02_product_id)->first();
                $product->total_qty_in += $request->qty_in;
                $product->stock += $request->qty_in;
                if (!$product->save()) {
                    return back()->withInput()->withErrors($product->errors());
                }
            }
            DB::commit();
            SessionHelper::setMessage("Data Inventory berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function update(Request $request, $product) {
        $this->validate($request, M02Product::rules($product->id));

        try {
            DB::beginTransaction();
            $product->m02_outlet_id = Auth::user()->m02_outlet->id;
            $product->code = $request->code;
            $product->name = $request->name;
            $product->m03_category_id = $request->m03_category_id;
            if ($request->m03_subcategory_id) {
                $product->m03_subcategory_id = $request->m03_subcategory_id;
            }
            $product->slug_url =  str_slug($request->name, '-') . '-' . str_random(6);
            $product->description = $request->description;
            $product->retail_price = $request->retail_price;
            $product->distributor_price = $request->distributor_price;
            $product->commission = $request->commission;
            if ($request->file('picture')) {
                $upload = FileHelper::upload(request(), 'picture', 'jpeg,bmp,png');
                $product->picture = $upload;
            }
            if (!$product->save()) {
                return back()->withInput()->withErrors($product->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Form succesfully updated");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }
    public function destroy($product, Request $request) {
        $product->delete();
        if ($request->ajax() || $request->wantsJson()) {
            return response('ok.', 200);
        }
        SessionHelper::setMessage("Product Has Been Deleted!");
        return redirect()->route($this->route.'.index');
    }

    public function filterProductByCategories($category_slug_url) {
        $category = M03ProductCategory::where('slug_url', $category_slug_url)->first();
        $products = M02Product::where('m03_category_id', $category->id)->paginate(12);
        return $this->showIndex($category->name, $products);
    }

    public function filterProductBySubcategories($category_slug_url, $subcategory_slug_url) {
        $subcategory = M03ProductSubcategory::where('slug_url', $subcategory_slug_url)->first();
        $products = M02Product::where('m03_subcategory_id', $subcategory->id)->paginate(12);
        return $this->showIndex($subcategory->name, $products);
    }

    public function showIndex($title, $products) {
        return view('frontend.product.index')
        ->with('title', $title)
        ->with('products', $products);
    }

    public function addReview($productId, Request $request) {
        $review = new M01Review();
        try {
            DB::beginTransaction();
            $review->m01_user_id = Auth::user()->id;
            $review->m02_product_id = $productId;
            $review->content = $request->id_input;
            $review->created_at = Carbon::now()->format('Y-m-d H:i:s');
            if (!$review->save()) {
                return back()->withInput()->withErrors($review->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Ulasan berhasil dikirim");
            $product = M02Product::where('id',$productId)->first();
            return redirect()->route('user.product.detail',$product->slug_url);

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
        return redirect()->route($this->route.'.index');
    }

    public function updateReview($reviewId, Request $request) {
        $review = M01Review::where('id', $reviewId)->first();
        try {
            DB::beginTransaction();
            $review->content = $request->id_input;
            $review->created_at = Carbon::now()->format('Y-m-d H:i:s');
            if (!$review->save()) {
                return back()->withInput()->withErrors($review->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Ulasan berhasil diubah");
            $product = M02Product::where('id',$review->m02_product_id)->first();
            return redirect()->route('user.product.detail',$product->slug_url);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
        return redirect()->route($this->route.'.index');
    }

    public function deleteReview($reviewId, Request $request) {
        $review = M01Review::where('id',$reviewId)->first();
        $review->delete();
        SessionHelper::setMessage("Review Has Been Deleted!");
        $product = M02Product::where('id',$review->m02_product_id)->first();
        return;
    }

    public function addToCart(Request $request) {
        $product = M02Product::where('id', $request->product_id)->first();
        $qty = 0;
        $cart = null;
        $message = null;
        if ($request->qty > $product->stock) {
            $message = "Product is out of stock";
            return response()->json(['data' => $cart, 'qty' => $qty, 'message' => $message]);
        }
        try {
            DB::beginTransaction();
            if (Auth::user()) {
                $cart = M01Cart::where('m01_user_id', Auth::user()->id)->where('m02_product_id',$request->product_id)->first();
            }
            else {
                $cart = M01Cart::where('session_id', request()->session()->getId())->where('m02_product_id',$request->product_id)->first();
            }
            if ($cart) {
                $cart->qty += $request->qty;
                $cart->subtotal = $cart->qty * $cart->m02_product->retail_price;
            } 
            else {
                $cart = new M01Cart();
                if (Auth::user()) {
                    $cart->m01_user_id = Auth::user()->id;
                }
                else {
                    $cart->session_id = request()->session()->getId();
                }
                $cart->m02_product_id = $request->product_id;
                $cart->qty = $request->qty;
                $cart->subtotal = $cart->qty * $cart->m02_product->retail_price;
            }
            if (!$cart->save()) {
                return back()->withInput()->withErrors($cart->errors());
            }
            DB::commit();

            if (Auth::user()) {
                $carts = M01Cart::where('m01_user_id', Auth::user()->id)->get();
            }
            else {
                $carts = M01Cart::where('session_id', request()->session()->getId())->get();
            }
            $qty = count($carts);
            return response()->json(['data' => $cart, 'qty' => $qty, 'message' => $message]);
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function deleteCart($cart, Request $request) {
        $cart->delete();
        $product = M02Product::where('id', $cart->m02_product_id)->first();
        $product->stock += $cart->qty;
        $product->save();
        return;
    }

    public function getCartCount() {
        $qty = 0;
        if (Auth::user()) {
            $carts = M01Cart::where('m01_user_id', Auth::user()->id)->get();
            if ($carts) $qty = count($carts);        
        }
        else {
            $carts = M01Cart::where('session_id', request()->session()->getId())->get();
            if ($carts) $qty = count($carts);  
        }
        return response()->json(['qty' => $qty]);
    }

    public function updateCart(Request $request) {
        $cart = M01Cart::where('id',$request->cart['id'])->first();
        $cart->qty = $request->qty;
        $cart->subtotal = $cart->qty * $cart->m02_product->retail_price;
        $cart->save();
    }

    public function getSubcategoriesByCategory(Request $request) {
        $categoryId = $request->category_id;
        $productSubcategories = M03ProductSubcategory::where('m03_category_id', $categoryId)->get();

        if (count($productSubcategories) == 0){
            return null;
        }

        $formListOption = "<label>Subkategori</label>";
        $formListOption .= "<select name='m03_subcategory_id' class='form-control' placeholder='Pilih subkategori...'>";
        $formListOption .= "<option value=0>Pilih subkategori...</option>";
        foreach ($productSubcategories as $key => $subcategory) {
            $formListOption .= "<option value=" . $subcategory->id . ">" . $subcategory->name . "</option>";
        }
        $formListOption .= "</select>";
        
        return $formListOption;
    }
}