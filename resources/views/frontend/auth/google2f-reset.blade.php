@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN section-title -->
        <h1 class="section-title clearfix">
            Reset Password<br>
            <small>Verify Google Authenticator.</small>
        </h1>
        <br>
        <div class="row row-space-10">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('user.verify.password.google2f', $user) }}">
                {{ csrf_field() }}
                <div class="col-md-4"></div>
                <div class="col-md-4" style="text-align: center">
                    <div class="form-register-success">
                        <i class="entypo-check"></i>
                        <h3>You are about to reset the password.</h3>
                        <p>Please input code from Google Authenticator Apps.</p>
                    </div>
                    <div class="form-group m-b-15">
                        @include('flash::message')
                    </div>
                    <div class="form-group m-b-15">
                        <div class="input-group">
                            <div class="input-group-addon"> <i class="fa fa-lock"></i> </div>
                            <input type="text" class="form-control" name="google_auth_code" id="google_auth_code" placeholder="Enter Google Authenticator Code" autocomplete="off" autofocus /> 
                        </div>
                    </div>
                    <div class="form-group m-b-15">
                        <button type="submit" class="btn btn-success btn-block btn-login"> <i class="entypo-right-open-mini"></i> Next
                        </button>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </form>
        </div>
    </div>
</div>
@endsection
