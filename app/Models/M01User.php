<?php

namespace App\Models;

use App\Helpers\Enums\UserRole;
use App\Models\M01Feedback;
use App\Models\M02Outlet;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class M01User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    'profile_pic','name','email','emailotp','email_verification','password',
    'phone','phoneotp','phone_verification','google_auth_code','googleotp',
    'iplog','role','auth_option'

    ];

    public $timestamps = false;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];

    public function m02_outlet() 
    {
        return $this->hasOne(M02Outlet::class);
    }

    public function m01_feedback() 
    {
        return $this->hasMany(M01Feedback::class);
    }

    public function isAdmin()
    {
        return $this->role == UserRole::ADMIN;
    }

    public function isVendor()
    {
        return $this->role == UserRole::VENDOR;
    }

    public static function updateProfileRules($id) {
        return [
            'old_password' => 'required',
        ];
    }
}
