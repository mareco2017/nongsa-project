@extends('layouts.app')

@section('content')
<div class="login-content">
    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4" style="padding: 6px 10px !important;">
                <button type="submit" class="btn btn-primary">
                    Send Password Reset Link
                </button>
            </div>
        </div>
        <div class="text-center text-inverse">
            <a href="{{ route('admin.login.form') }}" class="link"> <i class="entypo-lock"></i> Return to Login Page
            </a>
        </div>
        <hr />
        <p class="text-center text-inverse">
            PT Nongsa Green Park All Right Reserved 2017 &trade;
        </p>
    </form>
</div>
@endsection
