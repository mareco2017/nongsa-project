<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\OutletStatus;
use App\Helpers\Enums\UserRole;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M03ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class ProductCategoryController extends Controller
{
    protected $view = 'backend.product-category';
    protected $route = 'admin.product-categories';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view($this->view.'.index')
        ->with('productCategoryList', 'selected')
        ->with('productCategoryClass', 'start active open');
    }

    public function listAllProductCategory() {
        $productCategoryList = M03ProductCategory::all();
        return Datatables::of($productCategoryList)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('admin.product-categories.edit', ['productCategory' => $model->id]).'"
                class="btn btn-info btn-outline btn-sm">
                <span class="fa fa-eye"></span> Lihat
            </a>
            <a data-url="'.route('admin.product-categories.destroy', ['productCategory' => $model->id]).'"
                data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#product-category-list" data-message="Apakah anda yakin ingin menghapus kategori ini ?" 
                class="btn btn-danger btn-outline btn-sm" >
                <span class="fa fa-trash-o"></span> Hapus
            </a>';
        })
        ->make(true);
    }

    public function newForm() {
        return view($this->view.'.form')
        ->with('method','POST')
        ->with('productCategoryList', 'selected')
        ->with('productCategoryClass', 'start active open');
    }

    public function store(Request $request) {
        $this->validate($request, M03ProductCategory::rules());
        try {

            DB::beginTransaction();

            $productCategory = new M03ProductCategory();
            $productCategory->name = $request->name;
            $productCategory->slug_url = str_slug($request->name, '-');
            $productCategory->description = $request->description;
            if (!$productCategory->save()) {
                return back()->withInput()->withErrors($productCategory->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Kategori berhasil disimpan");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function edit($id) {
        $productCategory = M03ProductCategory::where('id', $id)->first();

        return view($this->view.'.form')
        ->with('productCategory', $productCategory)
        ->with('method', 'PUT')
        ->with('productCategoryList', 'selected')
        ->with('productCategoryClass', 'start active open');
    }

    public function update(Request $request, $id) {
        $productCategory = M03ProductCategory::where('id', $id)->first();
        $this->validate($request, M03ProductCategory::rules($productCategory->id));

        try {

            DB::beginTransaction();
            $productCategory->name = $request->name;
            $productCategory->slug_url = str_slug($request->name, '-');
            $productCategory->description = $request->description;
            if (!$productCategory->save()) {
                return back()->withInput()->withErrors($productCategory->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Kategori Produk berhasil diupdate");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function destroy($id, Request $request) {
        $productCategory = M03ProductCategory::where('id', $id)->first();
        $productCategory->delete();
        if ($request->ajax() || $request->wantsJson()) {
            return response('ok.', 200);
        }
        SessionHelper::setMessage("Kategori Produk berhasil dihapus.");
        return redirect()->route($this->route.'.index');
    }

}