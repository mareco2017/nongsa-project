@extends('layouts.backend.master')
@section('content')

<?php 

use App\Helpers\Enums\OutletStatus;

?>

@if (isset($product))
{!! Form::model($product, ['url' => route('vendor.products.update', ['product' => $product]),'method' => 'PUT', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@else
{!! Form::open(['url' => route('vendor.products.save'),'method' => 'POST', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@endif

<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('vendor.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Daftar Produk</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Profil Produk</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-body">
                        <form class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('code', 'Kode Produk')}}
                                        {{ Form::text('code', null, array('class' => 'form-control', 'placeholder' => 'Kode Produk','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('name', 'Nama')}}
                                        {{ Form::text('name',null, array('class' => 'form-control', 'placeholder' => 'Nama','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('description', 'Keterangan')}}
                                        {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Keterangan','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('category', 'Kategori')}}
                                        {{ Form::select('m03_category_id', $productCategories, null,array('class' => 'form-control', 'placeholder' => 'Pilih kategori...','data-parsley-required' => 'true', 'id' => 'select-category'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row {{ (count($productSubcategories) > 0) ? '' : 'hide' }}" id="subcategory-wrapper">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6" id="select-subcategory">
                                        @if (count($productSubcategories) > 0)
                                        {{ Form::label('subcategory', 'Subkategori')}}
                                        {{ Form::select('m03_subcategory_id', $productSubcategories, null,array('class' => 'form-control', 'placeholder' => 'Pilih kategori...','data-parsley-required' => 'true', 'id' => 'select-category'))}}
                                        @endif
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('retail_price', 'Harga Retail')}}
                                        {{ Form::number('retail_price', null, array('class' => 'form-control', 'placeholder' => 'Harga Retail','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('distributor_price', 'Harga Distributor')}}
                                        {{ Form::number('distributor_price', null, array('class' => 'form-control', 'placeholder' => 'Harga Distributor','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('commission', 'Komisi')}}
                                        {{ Form::number('commission', null, array('class' => 'form-control', 'placeholder' => 'Komisi','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('picture', 'Gambar Produk')}}<br>
                                        @if(isset($product))
                                        <img src="{{ asset($product->picture) }}" style="width: 210px; height: 210px" id="previewImage">
                                        @else
                                        <img src="{{ asset('assets/img/noimage.png') }}" style="width: 210px; height: 210px" id="previewImage">
                                        @endif
                                        <input type="file" name="picture" id="imageSelect">
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        <button type="submit" class="btn red btn-lg"><span class="fa fa-save"></span> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function(){

        function readURL(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageSelect").change(function(){
            readURL(this);
        });

        $('#select-category').on('change', function() {
            getSubcategoriesList($(this).val());
        });

        function getSubcategoriesList(categoryId) {
            $.ajax(
            {
                    headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url:  "{{ route('vendor.subcategories.filter') }}",
                data: { 
                    'category_id' : categoryId
                },
                type: 'GET',
                success: function(result) {
                    if (result) {
                        $('#select-subcategory').html(result);
                        $('#subcategory-wrapper').removeClass('hide');
                    }
                    else {
                        $('#subcategory-wrapper').addClass('hide');
                    }
                }
            });
        }
    });
</script>
@endpush