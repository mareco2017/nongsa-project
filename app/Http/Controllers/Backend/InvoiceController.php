<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\BillCategory;
use App\Helpers\Enums\InvoiceStatus;
use App\Helpers\Enums\UserRole;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M02Invoice;
use App\Models\M02Outlet;
use App\Models\M03Credit;
use App\Models\M03Overdue;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class InvoiceController extends Controller
{
    protected $view = 'backend.invoice';
    protected $route = 'admin.invoices';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view($this->view.'.index')
        ->with('invoiceList', 'selected')
        ->with('listOfStatus', InvoiceStatus::getArray() )
        ->with('listOfCategory', BillCategory::getArray() )
        ->with('invoiceClass', 'start active open');
    }

    public function listAllInvoice() {
    $invoiceList = M02Invoice::with('m01_user')->get();
        return Datatables::of($invoiceList)
        ->addColumn('action', function ($model) {
            return '
            <a data-url="'.route('admin.invoices.destroy', ['invoice' => $model->id]).'"
            data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#invoice-list" data-message="Would you like to delete this invoice?" 
            class="btn btn-danger btn-outline btn-sm" >
            <span class="fa fa-trash-o"></span> Delete
            </a>';
        })
        ->editColumn('status', function($model){
            return InvoiceStatus::getString($model->status);
        })
        ->editColumn('created_at', function($model){
            return Carbon::parse($model->created_at)->format('Y-m-d');
        })
        ->editColumn('bill_category', function($model){
            return BillCategory::getString($model->bill_category);
        })
        ->editColumn('m01_user_id', function($model){
            return $model->m01_user->name;
        })
        ->make(true);
    }

    public function newForm() {
        $user = M02Outlet::select('m01_user_id', DB::raw('CONCAT(blok, " - " , name) AS userList'))
        ->pluck('userList','m01_user_id');
        return view($this->view.'.form')
        ->with('method','POST')
        ->with('invoiceList', 'selected')
        ->with('listOfUser', $user)
        ->with('invoiceClass', 'start active open');
    }

    public function store(Request $request) {
        $this->validate($request, M02Invoice::rules());
        try {

            DB::beginTransaction();

            $invoice = new M02Invoice();
            $invoice->m01_user_id = $request->m01_user_id;
            $invoice->created_at = $request->created_at;
            $invoice->expired_at = $request->expired_at;
            $invoice->invoice_no = $request->invoice_no;
            $invoice->in_terms_of = $request->in_terms_of;
            $invoice->remark = $request->remark;
            $invoice->bill_category = $request->bill_category;
            $invoice->bill_amount = $request->bill_amount;
            $invoice->discount = $request->discount;
            $invoice->service_charge = $request->service_charge;
            $invoice->tax = $request->tax;
            $invoice->sub_total = $request->sub_total;
            $invoice->grand_total = $request->grand_total;
            $invoice->status = $request->status;
            if (!$invoice->save()) {
                return back()->withInput()->withErrors($invoice->errors());
            }

            $credit = new M03Credit();
            $credit->m02_invoice()->associate($invoice);
            $credit->m01_user_id = $invoice->m01_user_id;
            $credit->debit = $invoice->grand_total;
            $credit->credit = 0;
            $credit->bill_category = $invoice->bill_category;
            $credit->save();
            
            $overdue = new M03Overdue();
            $overdue->m02_invoice()->associate($invoice);
            $overdue->m01_user_id = $invoice->m01_user_id;
            $overdue->invoice_date = $invoice->created_at;
            $overdue->expired_date = $invoice->expired_at;
            $overdue->grand_total = $invoice->grand_total;
            $invoiceDate = Carbon::parse($invoice->created_at);
            $expiredDate = Carbon::parse($invoice->expired_at);
            $rawData = $expiredDate->diffForHumans($invoiceDate);
            if(strpos($rawData, 'setelah') !== false) {
                $overdue->overdue_day = str_replace("setelah", "", $rawData);
            } else {
                $overdue->overdue_day = str_replace("sebelum", "", $rawData);
            }
            $overdue->save();

            DB::commit();
            SessionHelper::setMessage("Invoice berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function edit($invoice) {
        $user = M02Outlet::select('id', DB::raw('CONCAT(blok, " - " , name) AS userList'))
        ->pluck('userList','m01_user_id');
        return view($this->view.'.form')
        ->with('invoice', $invoice)
        ->with('listOfUser', $user)
        ->with('method', 'PUT')
        ->with('invoiceList', 'selected')
        ->with('invoiceClass', 'start active open');
    }

    public function update(Request $request, $invoice) {
        $this->validate($request, M02Invoice::rules($invoice->id));

        try {

            DB::beginTransaction();
            $invoice->m01_user_id = $request->m01_user_id;
            $invoice->created_at = $request->created_at;
            $invoice->expired_at = $request->expired_at;
            $invoice->invoice_no = $request->invoice_no;
            $invoice->in_terms_of = $request->in_terms_of;
            $invoice->remark = $request->remark;
            $invoice->bill_category = $request->bill_category;
            $invoice->bill_amount = $request->bill_amount;
            $invoice->discount = $request->discount;
            $invoice->service_charge = $request->service_charge;
            $invoice->sub_total = $request->sub_total;
            $invoice->tax = $request->tax;
            $invoice->grand_total = $request->grand_total;
            $invoice->status = $request->status;
            if (!$invoice->save()) {
                return back()->withInput()->withErrors($invoice->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Invoice berhasil diupdate.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function destroy($invoice, Request $request) {
        $invoice->delete();
        if ($request->ajax() || $request->wantsJson()) {
            return response('ok.', 200);
        }
        SessionHelper::setMessage("Record Has Been Deleted!");
        return redirect()->route($this->route.'.index');
    }

    public function ajaxToogleGrandTotal(Request $request) {
        if ($request->ajax() || $request->wantsJson()) {
            return $request->amount;
        }
        return abort(403);
    }

}