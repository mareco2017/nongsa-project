<?php

namespace App\Models;

use App\Models\M01Order;
use App\Models\M02Outlet;
use Illuminate\Database\Eloquent\Model;

class M02VendorSale extends Model
{
    protected $table = "m02_vendor_sales";

    protected $fillable = [
        'resi_no','grand_total','delivery_status'
    ];

    protected $guarded = ['m01_order_id','m02_outlet_id'];

    public $timestamps = false;

    public static function rules($id = '') 
    {
        $rules = [
        ];
        return $rules;
    }

    public function m01_order() 
    {
        return $this->belongsTo(M01Order::class,'m01_order_id');
    }

    public function m02_outlet() 
    {
        return $this->belongsTo(M02Outlet::class);
    }

}