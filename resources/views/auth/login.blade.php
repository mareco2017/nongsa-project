@extends('layouts.app')

@section('content')
<div class="login-content">
    <form class="form-horizontal" method="POST" action="{{ route('admin.login.form') }}">
        {{ csrf_field() }}
        <div class="form-group m-b-15 {{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
            @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
            @endif
        </div>
        <div class="form-group m-b-15 {{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
            @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
        </div>
        <div class="checkbox m-b-30">
            <label>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
            </label>
        </div>
        <div class="login-buttons">
            <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
        </div>
        <a class="btn btn-link" href="{{ route('admin.password.request.index') }}">
            Forgot Your Password?
        </a>
        <div class="m-t-20 m-b-40 p-b-40">
            Not a member yet? Click <a href="{{ route('admin.register.form') }}" class="text-success">here</a> to register.
        </div>
        <hr />
        <p class="text-center text-inverse">
             PT Mareco Prima Mandiri All Right Reserved 2017 &trade;
        </p>
    </form>
</div>
@endsection
