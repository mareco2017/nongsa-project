<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Enums\OrderStatus;
use App\Helpers\Enums\OutletStatus;
use App\Helpers\Enums\UserRole;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01Cart;
use App\Models\M01Feedback;
use App\Models\M01Order;
use App\Models\M01OrderDetail;
use App\Models\M01Payment;
use App\Models\M01Review;
use App\Models\M01User;
use App\Models\M02Banner;
use App\Models\M02Outlet;
use App\Models\M02Product;
use App\Models\M02VendorSale;
use App\Models\M03AboutUs;
use App\Models\M03ContactUs;
use App\Models\M03HowToTransaction;
use App\Models\M03New;
use App\Models\M03ProductCategory;
use App\Models\M03ProductSubcategory;
use App\Models\M03ShippingMethod;
use App\Models\M03TermsAndCondition;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class HomepageController extends Controller
{
    protected $view = 'frontend.homepage';
    protected $route = 'user.homepages';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {
        return redirect()->route('user.homepages.showIndex');
    }

    public function showIndex() {
        $outlets = M02Outlet::where('status','=', OutletStatus::TERSEWA)->paginate(6, ['*'], 'outlets', null);
        $products = M02Product::paginate(6, ['*'], 'products', null);
        $banners = M02Banner::orderBy('is_first','desc')->where('title','=',NULL)->get();
        $storeBanner = M02Banner::where('title','=', 'store')->first();
        $productBanner = M02Banner::where('title','=', 'product')->first();
        $countBanner = count($banners);
        $isFirst = true;
        return view($this->view.'.index')
        ->with('creditList', 'selected')
        ->with('creditClass', 'start active open')
        ->with('outlets', $outlets)
        ->with('isFirst', $isFirst)
        ->with('storeBanner', $storeBanner)
        ->with('productBanner', $productBanner)
        ->with('banners', $banners)
        ->with('countBanner', $countBanner)
        ->with('products',$products);
    }

    public function terms() {
        $term = M03TermsAndCondition::whereNull('m02_outlet_id')->first();
        return view('frontend.terms.index')->with('term', $term);
    }

    public function abouts() {
        $about = M03AboutUs::whereNull('m02_outlet_id')->first();
        return view('frontend.abouts.index')->with('about', $about)
        ->with('aboutUsClass', 'start active open');
    }

    public function howTo() {
        $howToTransaction = M03HowToTransaction::whereNull('m02_outlet_id')->first();
        return view('frontend.how-to.index')->with('howTo', $howToTransaction);
    }

    public function delivery() {
        $delivery = null;
        $user = null;
        $shippingMethod = null;
        return view('frontend.delivery.index')
        ->with('delivery', $delivery)
        ->with('user', $user)
        ->with('shippingMethod', $shippingMethod);
    }

    public function checkDelivery(Request $request) {
        try {
            $delivery = M02VendorSale::where(strtolower('resi_no'),strtolower($request->resi_no))->with('m01_order')->select(
                'm02_vendor_sales.resi_no','m02_vendor_sales.m01_order_id','m02_vendor_sales.delivery_status')->first();
            $user = null;
            $shippingMethod = null;
            if (!$delivery ) {
                SessionHelper::setMessage('Resi Number not found.','warning');
                return view('frontend.delivery.index')
                ->with('delivery', $delivery)
                ->with('user', $user)
                ->with('shippingMethod', $shippingMethod);
            }
            $shippingMethod = M01Order::where('id',$delivery->m01_order_id)->first()->m03_shipping_method->name;
            $user = M01User::where('id',$delivery->m01_order->m01_user_id)->first();

            return view('frontend.delivery.index')
            ->with('delivery',$delivery)
            ->with('user',$user)
            ->with('shippingMethod', $shippingMethod);
        }
        catch (\Exception $e){
            return back()->withInput()->withErrors($e);
        }
    }

    public function feedback() {
        return view('frontend.feedback.index')
        ->with('feedbackClass', 'start active open');
    }

    public function saveFeedback(Request $request) {
        $this->validate($request, M01Feedback::rules());
        try {
            DB::beginTransaction();
            $feedback = new M01Feedback();
            $feedback->name = $request->name;
            $feedback->email = $request->email;
            $feedback->subject = $request->subject;
            $feedback->description = $request->description;
            if (!$feedback->save()) {
                return back()->withInput()->withErrors($feedback->errors());
            }
            DB::commit();
            SessionHelper::setMessage("Thank you, your feedback has been sent.");
            return redirect()->route('user.feedback.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function contactUs() {
        $contactUs = M03AboutUs::whereNull('m02_outlet_id')->first();
        return view('frontend.contact-us.index')
        ->with('contactUs', $contactUs)
        ->with('contactUsClass', 'start active open');
    }

    public static function getFavoredSubcategories() {
        $subcategories = [];
        $category = M03ProductCategory::where('slug_url', 'tanaman-hias')->first();
        if ($category) {
            $subcategories = M03ProductSubcategory::where('m03_category_id', $category->id)
            ->with('category')
            ->limit(5)
            ->get();

        }
        return $subcategories;
    }

    public static function getOtherCategories() {
        $category = M03ProductCategory::where('slug_url', 'tanaman-hias')->first();
        $categories = M03ProductCategory::limit(5)->get();
        if ($category) {
            $categories = M03ProductCategory::where('id','<>',$category->id)
            ->limit(5)
            ->get();
        }

        return $categories;
    }

    public function saveMessage(Request $request) {
        $this->validate($request, M03ContactUs::rules());
        try {
            DB::beginTransaction();
            $message = new M03ContactUs();
            $message->name = $request->name;
            $message->email = $request->email;
            $message->subject = $request->subject;
            if (!$message->save()) {
                return back()->withInput()->withErrors($message->errors());
            }
            DB::commit();
            SessionHelper::setMessage("Thank you, your message has been sent.");
            return redirect()->route('user.contact-us.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function showProductDetail($slug_url) {
        $product = M02Product::where('slug_url', $slug_url)->first();
        $reviews = M01Review::where('m02_product_id',$product->id)->get();
        $hasReviewBefore = null;
        if(Auth::user()) {
            $hasReviewBefore = M01Review::where('m02_product_id', $product->id)->where('m01_user_id', Auth::user()->id)->first();
        }
        return view('frontend.product.detail')
        ->with('product',$product)
        ->with('reviews',$reviews)
        ->with('hasReviewBefore', $hasReviewBefore);
    }

    public function searchProduct(Request $request) {
        $products = M02Product::where('name', 'LIKE', '%'.$request->search.'%')->get();
        $content = '<div class="">
        <div class="cart-body">
        <ul class="cart-item">';
        if(count($products) < 1) {
            $content .= '<li style="list-style : none">No Product Found</li>';
        }
        foreach ($products as $product) {
            $content .= '<li style="list-style : none">
            <a href="'.route('user.product.detail',["slug" => $product->slug_url]).'">
            <div class="cart-item-image">
            <img src="'.$product->picture.'">
            </div>
            <div class="cart-item-info" style="margin-left: 80px;">
            <h4 style="font-size: 20px"> '.$product->name.' </h4>
            <p class="price" style="font-size: 15px; margin-top: 8px;">
            Rp. '.number_format($product->retail_price,2,',','.').'
            </p>
            </div>
            </a>
            </li>';
        }
        $content .= '</ul></div></div>';

        return $content;
    }

    public function newsAndEvents() {
        $news = M03New::orderBy('created_at','desc')->paginate(12);
        return view('frontend.news.index')->with('news', $news);
    }

    public function newsAndEventsDetail($m03_news) {
        return view('frontend.news.detail')->with('new', $m03_news);
    }

    public function outletDetail($m02_outlet){
        $about = M03AboutUs::where('m02_outlet_id','=', $m02_outlet->id)->first();
        $tnc = M03TermsAndCondition::where('m02_outlet_id','=', $m02_outlet->id)->first();
        $htt = M03HowToTransaction::where('m02_outlet_id','=', $m02_outlet->id)->first();
        return view('frontend.vendor.detail')
        ->with('about', $about)
        ->with('tnc', $tnc)
        ->with('htt', $htt)
        ->with('m02_outlet', $m02_outlet);
    }

    public function saveMessageToOutlet(Request $request, $m02_outlet) {
        $this->validate($request, M03ContactUs::rules());
        try {
            DB::beginTransaction();
            $message = new M03ContactUs();
            $message->name = $request->name;
            $message->m02_outlet_id = $m02_outlet->id;
            $message->email = $request->email;
            $message->subject = $request->subject;
            if (!$message->save()) {
                return back()->withInput()->withErrors($message->errors());
            }
            DB::commit();
            SessionHelper::setMessage("Thank you, your message has been sent.");
            return redirect()->route('frontend.vendor.detail', $m02_outlet->id);

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function saveFeedbackToOutlet(Request $request, $m02_outlet) {
        $this->validate($request, M01Feedback::rules());
        try {
            DB::beginTransaction();
            $feedback = new M01Feedback();
            $feedback->name = $request->name;
            $feedback->m02_outlet_id = $m02_outlet->id;
            $feedback->email = $request->email;
            $feedback->subject = $request->subject;
            $feedback->description = $request->description;
            $feedback->created_at = Carbon::now();
            if (!$feedback->save()) {
                return back()->withInput()->withErrors($feedback->errors());
            }
            DB::commit();
            SessionHelper::setMessage("Thank you, your feedback has been sent.");
            return redirect()->route('frontend.vendor.detail', $m02_outlet->id);

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function cart() {
        $totalPrice = null;
        $carts = null;
        if (Auth::user()) {
            $carts = M01Cart::where('m01_user_id', Auth::user()->id)->get();
            if ($carts) $totalPrice = $carts->sum('subtotal');
        }
        else {
            $carts = M01Cart::where('session_id', request()->session()->getId())->get();
            if ($carts) $totalPrice = $carts->sum('subtotal');
        }
        return view('frontend.product.cart')
        ->with('carts', $carts)
        ->with('totalPrice', $totalPrice);
    }

    public function buyProduct($m02_product_slug_url) {
        $product = M02Product::where('slug_url', $m02_product_slug_url)->first();
        $qty = 1;
        $cart = null;
        $message = null;
        try {
            DB::beginTransaction();
            if (Auth::user()) {
                $cart = M01Cart::where('m01_user_id', Auth::user()->id)->where('m02_product_id',$product->id)->first();
            }
            else {
                $cart = M01Cart::where('session_id', request()->session()->getId())->where('m02_product_id',$product->id)->first();
            }
            if ($cart) {
                $cart->qty += $qty;
                $cart->subtotal = $cart->qty * $cart->m02_product->retail_price;
            } 
            else {
                $cart = new M01Cart();
                if (Auth::user()) {
                    $cart->m01_user_id = Auth::user()->id;
                }
                else {
                    $cart->session_id = request()->session()->getId();
                }
                $cart->m02_product_id = $product->id;
                $cart->qty = $qty;
                $cart->subtotal = $cart->qty * $cart->m02_product->retail_price;
            }
            if (!$cart->save()) {
                return back()->withInput()->withErrors($cart->errors());
            }
            DB::commit();
            return redirect()->route('user.cart.index');
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function checkout()
    {
        if (!Auth::user()) {
            SessionHelper::setMessage("Please login first to continue.","warning");
            return redirect()->route('user.login.form');
        }
        $cartsBySession = M01Cart::where('session_id', request()->session()->getId())->get();
        if ($cartsBySession) {
            foreach ($cartsBySession as $cart) {
                $cart->m01_user_id = Auth::user()->id;
                $cart->save();
            }
        }
        $carts = M01Cart::where('m01_user_id', Auth::user()->id)->get();
        foreach ($carts as $key => $cart) {
            if ($cart->qty > $cart->m02_product->stock) {
                SessionHelper::setMessage($cart->m02_product->name . " stock is not enough.","warning");
                return back();
            }
        }
        $shippingMethods = M03ShippingMethod::pluck('name','id');
        $totalPrice = 0;
        if ($carts) $totalPrice = $carts->sum('subtotal');
        return view('frontend.product.checkout')
        ->with('carts', $carts)
        ->with('shippingMethods', $shippingMethods)
        ->with('totalPrice',$totalPrice);
    }

    public function searchIndex(Request $request) {
        $products = M02Product::where('name', 'LIKE', '%'.$request->search.'%')->paginate(12);
        return view('frontend.product.search')
        ->with('products', $products)
        ->with('searchName', $request->search);
    }

    public function order() {
        $orders = null;
        if (Auth::user()) {                
            $orders = M01Order::where('m01_user_id', Auth::user()->id)->get();
        }
        return view('frontend.order.index')
        ->with('orders',$orders);
    }

    public function orderDetail($m01_order_id) {
        $order = M01Order::where('id',$m01_order_id)->first();
        $outletIdList = [];
        $resiNoList = [];
        $orderList = [];
        $orderDetails = M01OrderDetail::where('m01_order_id', $m01_order_id)->get();
        foreach ($orderDetails as $key => $orderDetail) {
            $index = array_search($orderDetail->m02_product->m02_outlet_id, $outletIdList);
            if (false !== $index = array_search($orderDetail->m02_product->m02_outlet_id, $outletIdList)) {
                array_push($orderList[$index], $orderDetail);
            }
            else {
                array_push($outletIdList,$orderDetail->m02_product->m02_outlet_id);
                array_push($orderList, [$orderDetail]);
                $vendorSale = M02VendorSale::where('m02_outlet_id',$orderDetail->m02_product->m02_outlet_id)->where('m01_order_id', $m01_order_id)->first();
                if ($vendorSale) {
                    $resiNo = $vendorSale->resi_no;
                    if ($resiNo) {
                        array_push($resiNoList, $resiNo);
                    }
                    else {
                        array_push($resiNoList, 'Belum ada No. Resi');
                    }
                }
                else {
                    array_push($resiNoList, 'Belum ada No. Resi');
                }
            }
        }
        return view('frontend.order.detail')
        ->with('order',$order)
        ->with('outletIdList', $outletIdList)
        ->with('orderList', $orderList)
        ->with('resiNoList', $resiNoList);
    }

    public function payment($m01_order_id) {
        $order = M01Order::where('id',$m01_order_id)->first();
        return view('frontend.order.payment')
        ->with('order',$order);
    }

    public function submitPayment($m01_order_id, Request $request) {
        $this->validate($request, M01Payment::rules());
        try {
            DB::beginTransaction();
            $payment = new M01Payment();
            $payment->m01_order_id = $m01_order_id;
            $payment->payment_date = $request->payment_date;
            $payment->amount = $request->amount;
            if ($request->file('picture')) {
                $upload = FileHelper::upload(request(), 'picture', 'jpeg,bmp,png');
                $payment->payment_proof = $upload;
            }
            if (!$payment->save()) {
                return back()->withInput()->withErrors($payment->errors());
            }
            $order = M01Order::where('id', $m01_order_id)->first();
            if ($payment->amount < $order->grand_total) {
                SessionHelper::setMessage("Your payment is not enough.","warning");
                return back();
            }
            $order->status = OrderStatus::ON_VERIFICATION;
            $order->save();
            DB::commit();
            SessionHelper::setMessage("Thank you, your payment confirmation has been submitted.");
            return redirect()->route('user.order.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function getShippingCost(Request $request) {
        $shippingMethod = M03ShippingMethod::where('id', $request->id)->first();
        $carts = M01Cart::where('m01_user_id', Auth::user()->id)->get();
        $outletIdList = [];
        foreach ($carts as $key => $cart) {
            $index = array_search($cart->m02_product->m02_outlet_id, $outletIdList);
            if (false === $index = array_search($cart->m02_product->m02_outlet_id, $outletIdList)) {
                array_push($outletIdList,$cart->m02_product->m02_outlet_id);
            }
        }
        $totalPrice = 0;
        $shippingPrice = $shippingMethod->price * count($outletIdList);
        if ($carts) $totalPrice = $carts->sum('subtotal');
        return response()->json(['price' => $shippingPrice, 'totalPrice' => $totalPrice]);
    }

    public function submitCheckout(Request $request) {
        if ($request->m03_shipping_method_id == null) {
            SessionHelper::setMessage("Please select a delivery option.","warning");
            return back();
        }
        try {
            DB::beginTransaction();
            $carts = M01Cart::where('m01_user_id', Auth::user()->id)->get();
            foreach ($carts as $key => $cart) {
                if ($cart->qty > $cart->m02_product->stock) {
                    SessionHelper::setMessage($cart->m02_product->name . " stock is not enough.","warning");
                    return back();
                }
            }
            $totalPrice = 0;
            if ($carts) $totalPrice = $carts->sum('subtotal');
            $shippingMethod = M03ShippingMethod::where('id', $request->m03_shipping_method_id)->first();
            $grandTotal = $totalPrice + $shippingMethod->price;

            $order = new M01Order();
            $order->grand_total = $grandTotal;
            $order->m03_shipping_method_id = $request->m03_shipping_method_id;
            $order->m01_user_id = Auth::user()->id;
            $order->address = $request->address;
            $order->created_at = Carbon::now()->format('Y-m-d');
            $order->expired_at = Carbon::now()->format('Y-m-d');
            $order->status = 0;
            if (!$order->save()) {
                return back()->withInput()->withErrors($order->errors());
            }
            $order->save();

            $currentOrder = M01Order::where('id', $order->id)->first();
            $currentOrder->invoice_no = "INV-" . str_pad($order->id, 6, "0", STR_PAD_LEFT); 
            $currentOrder->save();

            $outletIdList = [];
            $outletGrandTotalList = [];
            foreach ($carts as $key => $cart) {
                $orderDetail = new M01OrderDetail();
                $orderDetail->m01_order_id = $order->id;
                $orderDetail->m02_product_id = $cart->m02_product_id;
                $orderDetail->qty = $cart->qty;
                $orderDetail->price = $cart->m02_product->retail_price;
                $orderDetail->subtotal = $orderDetail->qty * $orderDetail->price;
                $orderDetail->save();

                $product = M02Product::where('id', $orderDetail->m02_product_id)->first();
                $product->total_qty_out += $orderDetail->qty;
                $product->stock -= $orderDetail->qty;
                $product->save();

                $index = array_search($cart->m02_product->m02_outlet_id, $outletIdList);
                if (false !== $index = array_search($cart->m02_product->m02_outlet_id, $outletIdList)) {
                    $outletGrandTotalList[$index] += $orderDetail->subtotal;
                }
                else {
                    array_push($outletIdList,$cart->m02_product->m02_outlet_id);
                    array_push($outletGrandTotalList, $orderDetail->subtotal);
                }

                $cart->delete();
            }

            foreach ($outletIdList as $key => $outletId) {
                $vendorSale = new M02VendorSale();
                $vendorSale->m01_order_id = $order->id;
                $vendorSale->m02_outlet_id = $outletId;
                $vendorSale->grand_total = $outletGrandTotalList[$key] + $shippingMethod->price;
                $vendorSale->delivery_status = "Masih di gudang";
                $vendorSale->save();
            }

            DB::commit();
            SessionHelper::setMessage("Checkout Success !!");
            return redirect()->route('user.order.index');
            
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }
}
