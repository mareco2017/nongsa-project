<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Enums\BillCategory;
use App\Helpers\Enums\InvoiceStatus;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M02Inventory;
use App\Models\M02Invoice;
use App\Models\M02Outlet;
use App\Models\M02Product;
use App\Models\M03Credit;
use App\Models\M03Overdue;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class BillingController extends Controller
{
    protected $landView = 'frontend.vendor.billing.land';
    protected $landRoute = 'vendor.land';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    //Sewa Lahan
    public function indexLand() {
        $totalDebit = M03Credit::where('bill_category', BillCategory::SEWA_LAHAN)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('debit');

        $totalCredit = M03Credit::where('bill_category', BillCategory::SEWA_LAHAN)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('credit');

        return view($this->landView.'.index')
        ->with('landList', 'selected')
        ->with('totalDebit', $totalDebit)
        ->with('totalCredit', $totalCredit)
        ->with('landClass', 'start active open')
        ->with('billingClass', 'start active open');
    }

    public function listAllLand() {
        $billings = M03Credit::where('bill_category', BillCategory::SEWA_LAHAN)
        ->where('m01_user_id','=', Auth::user()->id)
        ->with('m02_invoice')
        ->select('m03_credits.id','m03_credits.m02_invoice_id','m03_credits.debit','m03_credits.credit','m03_credits.bill_category')
        ->get();
        return Datatables::of($billings)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('vendor.pick.land', ['credit' => $model->id]).'"
            class="btn btn-info btn-outline btn-sm">
            <span class="fa fa-magic"></span> Pilih Invoice
            </a>';
        })
        ->make(true);
    }

    public function specifyLand($m03_credit) {
        $totalDebit = M03Credit::where('bill_category', BillCategory::SEWA_LAHAN)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('debit');

        $totalCredit = M03Credit::where('bill_category', BillCategory::SEWA_LAHAN)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('credit');

        return view($this->landView.'.form')
        ->with('landList', 'selected')
        ->with('totalDebit', $totalDebit)
        ->with('totalCredit', $totalCredit)
        ->with('landClass', 'start active open')
        ->with('billingClass', 'start active open')
        ->with('m03_credit', $m03_credit);
    }

    public function storeLand(Request $request) {
        $this->validate($request, M03Credit::rules());
        try {

            DB::beginTransaction();

            $invoice = M02Invoice::where('id','=', $request->m02_invoice_id)->first();
            $invoice->status = InvoiceStatus::PAID;
            $invoice->save();

            $credit = new M03Credit();
            $credit->m02_invoice_id = $request->m02_invoice_id;
            $credit->m01_user_id = Auth::user()->id;
            $credit->debit = 0;
            $credit->credit = $request->bill_amount;
            $credit->remark = $request->remark;
            $credit->reference_no = $request->reference_no;
            $credit->bill_category = BillCategory::SEWA_LAHAN;
            $credit->save();

            $overdue = M03Overdue::where('m02_invoice_id','=', $request->m02_invoice_id)->first();
            if($overdue) $overdue->delete();

            DB::commit();
            SessionHelper::setMessage("Billing berhasil dikirim.");
            return redirect()->route($this->landRoute.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    //Biaya Listrik
    protected $electricityView = 'frontend.vendor.billing.electricity';
    protected $electricityRoute = 'vendor.electricity';

    public function indexElectricity() {
        $totalDebit = M03Credit::where('bill_category', BillCategory::LISTRIK)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('debit');

        $totalCredit = M03Credit::where('bill_category', BillCategory::LISTRIK)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('credit');

        return view($this->electricityView.'.index')
        ->with('electricityList', 'selected')
        ->with('totalDebit', $totalDebit)
        ->with('totalCredit', $totalCredit)
        ->with('electricityClass', 'start active open')
        ->with('billingClass', 'start active open');
    }

    public function listAllElectricity() {
        $billings = M03Credit::where('bill_category', BillCategory::LISTRIK)
        ->where('m01_user_id','=', Auth::user()->id)
        ->with('m02_invoice')
        ->select('m03_credits.id','m03_credits.m02_invoice_id','m03_credits.debit','m03_credits.credit','m03_credits.bill_category')
        ->get();
        return Datatables::of($billings)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('vendor.pick.electricity', ['credit' => $model->id]).'"
            class="btn btn-info btn-outline btn-sm">
            <span class="fa fa-magic"></span> Pilih Invoice
            </a>';
        })
        ->make(true);
    }

    public function specifyElectricity($m03_credit) {
        $totalDebit = M03Credit::where('bill_category', BillCategory::LISTRIK)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('debit');

        $totalCredit = M03Credit::where('bill_category', BillCategory::LISTRIK)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('credit');

        return view($this->electricityView.'.form')
        ->with('electricityList', 'selected')
        ->with('totalDebit', $totalDebit)
        ->with('totalCredit', $totalCredit)
        ->with('electricityClass', 'start active open')
        ->with('billingClass', 'start active open')
        ->with('m03_credit', $m03_credit);
    }

    public function storeElectricity(Request $request) {
        $this->validate($request, M03Credit::rules());
        try {

            DB::beginTransaction();

            $invoice = M02Invoice::where('id','=', $request->m02_invoice_id)->first();
            $invoice->status = InvoiceStatus::PAID;
            $invoice->save();

            $credit = new M03Credit();
            $credit->m02_invoice_id = $request->m02_invoice_id;
            $credit->m01_user_id = Auth::user()->id;
            $credit->debit = 0;
            $credit->credit = $request->bill_amount;
            $credit->remark = $request->remark;
            $credit->reference_no = $request->reference_no;
            $credit->bill_category = BillCategory::LISTRIK;
            $credit->save();

            $overdue = M03Overdue::where('m02_invoice_id','=', $request->m02_invoice_id)->first();
            if($overdue) $overdue->delete();

            DB::commit();
            SessionHelper::setMessage("Billing berhasil dikirim.");
            return redirect()->route($this->electricityRoute.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

     //Biaya Air
    protected $waterExpenseView = 'frontend.vendor.billing.water-expense';
    protected $waterExpenseRoute = 'vendor.water-expense';

    public function indexWaterExpense() {
        $totalDebit = M03Credit::where('bill_category', BillCategory::AIR)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('debit');

        $totalCredit = M03Credit::where('bill_category', BillCategory::AIR)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('credit');

        return view($this->waterExpenseView.'.index')
        ->with('waterExpenseList', 'selected')
        ->with('totalDebit', $totalDebit)
        ->with('totalCredit', $totalCredit)
        ->with('waterExpenseClass', 'start active open')
        ->with('billingClass', 'start active open');
    }

    public function listAllWaterExpense() {
        $billings = M03Credit::where('bill_category', BillCategory::AIR)
        ->where('m01_user_id','=', Auth::user()->id)
        ->with('m02_invoice')
        ->select('m03_credits.id','m03_credits.m02_invoice_id','m03_credits.debit','m03_credits.credit','m03_credits.bill_category')
        ->get();
        return Datatables::of($billings)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('vendor.pick.water-expense', ['credit' => $model->id]).'"
            class="btn btn-info btn-outline btn-sm">
            <span class="fa fa-magic"></span> Pilih Invoice
            </a>';
        })
        ->make(true);
    }

    public function specifyWaterExpense($m03_credit) {
        $totalDebit = M03Credit::where('bill_category', BillCategory::AIR)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('debit');

        $totalCredit = M03Credit::where('bill_category', BillCategory::AIR)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('credit');

        return view($this->waterExpenseView.'.form')
        ->with('waterExpenseList', 'selected')
        ->with('totalDebit', $totalDebit)
        ->with('totalCredit', $totalCredit)
        ->with('waterExpenseClass', 'start active open')
        ->with('billingClass', 'start active open')
        ->with('m03_credit', $m03_credit);
    }

    public function storeWaterExpense(Request $request) {
        $this->validate($request, M03Credit::rules());
        try {

            DB::beginTransaction();

            $invoice = M02Invoice::where('id','=', $request->m02_invoice_id)->first();
            $invoice->status = InvoiceStatus::PAID;
            $invoice->save();

            $credit = new M03Credit();
            $credit->m02_invoice_id = $request->m02_invoice_id;
            $credit->m01_user_id = Auth::user()->id;
            $credit->debit = 0;
            $credit->credit = $request->bill_amount;
            $credit->remark = $request->remark;
            $credit->reference_no = $request->reference_no;
            $credit->bill_category = BillCategory::AIR;
            $credit->save();

            $overdue = M03Overdue::where('m02_invoice_id','=', $request->m02_invoice_id)->first();
            if($overdue) $overdue->delete();

            DB::commit();
            SessionHelper::setMessage("Billing berhasil dikirim.");
            return redirect()->route($this->waterExpenseRoute.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

     //Biaya Satpam
    protected $guardView = 'frontend.vendor.billing.guard';
    protected $guardRoute = 'vendor.guards';

    public function indexGuard() {
        $totalDebit = M03Credit::where('bill_category', BillCategory::SATPAM)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('debit');

        $totalCredit = M03Credit::where('bill_category', BillCategory::SATPAM)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('credit');

        return view($this->guardView.'.index')
        ->with('guardList', 'selected')
        ->with('totalDebit', $totalDebit)
        ->with('totalCredit', $totalCredit)
        ->with('guardClass', 'start active open')
        ->with('billingClass', 'start active open');
    }

    public function listAllGuard() {
        $billings = M03Credit::where('bill_category', BillCategory::SATPAM)
        ->where('m01_user_id','=', Auth::user()->id)
        ->with('m02_invoice')
        ->select('m03_credits.id','m03_credits.m02_invoice_id','m03_credits.debit','m03_credits.credit','m03_credits.bill_category')
        ->get();
        return Datatables::of($billings)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('vendor.pick.guards', ['credit' => $model->id]).'"
            class="btn btn-info btn-outline btn-sm">
            <span class="fa fa-magic"></span> Pilih Invoice
            </a>';
        })
        ->make(true);
    }

    public function specifyGuard($m03_credit) {
        $totalDebit = M03Credit::where('bill_category', BillCategory::SATPAM)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('debit');

        $totalCredit = M03Credit::where('bill_category', BillCategory::SATPAM)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('credit');

        return view($this->guardView.'.form')
        ->with('guardList', 'selected')
        ->with('totalDebit', $totalDebit)
        ->with('totalCredit', $totalCredit)
        ->with('guardClass', 'start active open')
        ->with('billingClass', 'start active open')
        ->with('m03_credit', $m03_credit);
    }

    public function storeGuard(Request $request) {
        $this->validate($request, M03Credit::rules());
        try {

            DB::beginTransaction();

            $invoice = M02Invoice::where('id','=', $request->m02_invoice_id)->first();
            $invoice->status = InvoiceStatus::PAID;
            $invoice->save();

            $credit = new M03Credit();
            $credit->m02_invoice_id = $request->m02_invoice_id;
            $credit->m01_user_id = Auth::user()->id;
            $credit->debit = 0;
            $credit->credit = $request->bill_amount;
            $credit->remark = $request->remark;
            $credit->reference_no = $request->reference_no;
            $credit->bill_category = BillCategory::SATPAM;
            $credit->save();

            $overdue = M03Overdue::where('m02_invoice_id','=', $request->m02_invoice_id)->first();
            if($overdue) $overdue->delete();

            DB::commit();
            SessionHelper::setMessage("Billing berhasil dikirim.");
            return redirect()->route($this->guardRoute.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

     //Biaya Lain-lain
    protected $otherView = 'frontend.vendor.billing.other';
    protected $otherRoute = 'vendor.others';

    public function indexOther() {
        $totalDebit = M03Credit::where('bill_category', BillCategory::LAIN)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('debit');

        $totalCredit = M03Credit::where('bill_category', BillCategory::LAIN)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('credit');

        return view($this->otherView.'.index')
        ->with('otherList', 'selected')
        ->with('totalDebit', $totalDebit)
        ->with('totalCredit', $totalCredit)
        ->with('otherClass', 'start active open')
        ->with('billingClass', 'start active open');
    }

    public function listAllOther() {
        $billings = M03Credit::where('bill_category', BillCategory::LAIN)
        ->where('m01_user_id','=', Auth::user()->id)
        ->with('m02_invoice')
        ->select('m03_credits.id','m03_credits.m02_invoice_id','m03_credits.debit','m03_credits.credit','m03_credits.bill_category')
        ->get();
        return Datatables::of($billings)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('vendor.pick.others', ['credit' => $model->id]).'"
            class="btn btn-info btn-outline btn-sm">
            <span class="fa fa-magic"></span> Pilih Invoice
            </a>';
        })
        ->make(true);
    }

    public function specifyOther($m03_credit) {
        $totalDebit = M03Credit::where('bill_category', BillCategory::LAIN)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('debit');

        $totalCredit = M03Credit::where('bill_category', BillCategory::LAIN)
        ->where('m01_user_id','=', Auth::user()->id)
        ->groupBy('bill_category')
        ->sum('credit');

        return view($this->otherView.'.form')
        ->with('otherList', 'selected')
        ->with('totalDebit', $totalDebit)
        ->with('totalCredit', $totalCredit)
        ->with('otherClass', 'start active open')
        ->with('billingClass', 'start active open')
        ->with('m03_credit', $m03_credit);
    }

    public function storeOther(Request $request) {
        $this->validate($request, M03Credit::rules());
        try {

            DB::beginTransaction();

            $invoice = M02Invoice::where('id','=', $request->m02_invoice_id)->first();
            $invoice->status = InvoiceStatus::PAID;
            $invoice->save();

            $credit = new M03Credit();
            $credit->m02_invoice_id = $request->m02_invoice_id;
            $credit->m01_user_id = Auth::user()->id;
            $credit->debit = 0;
            $credit->credit = $request->bill_amount;
            $credit->remark = $request->remark;
            $credit->reference_no = $request->reference_no;
            $credit->bill_category = BillCategory::LAIN;
            $credit->save();

            $overdue = M03Overdue::where('m02_invoice_id','=', $request->m02_invoice_id)->first();
            if($overdue) $overdue->delete();

            DB::commit();
            SessionHelper::setMessage("Billing berhasil dikirim.");
            return redirect()->route($this->otherRoute.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }


}