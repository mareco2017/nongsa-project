<div id="footer" class="footer">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN row -->
        <div class="row">
            <!-- BEGIN col-4 -->
            <div class="col-md-3">
                <h4 class="footer-header">COMPANY</h4>
                <a href="{{ route('user.news.index') }}">
                    <p>
                        News & Events
                    </p>
                </a>
                <a href="{{ route('user.terms.index') }}">
                    <p>
                        Terms & Conditions
                    </p>
                </a>
            </div>
            <!-- END col-4 -->
            <!-- BEGIN col-4 -->
            <div class="col-md-3">
                <h4 class="footer-header">CUSTOMER</h4>
                <ul class="fa-ul">
                    <li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('user.how-to-transaction.index') }}">How to transaction ?</a></li>
                    <li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('user.order.index') }}">Confirmation Payment</a></li>
                    <li><i class="fa fa-li fa-angle-right"></i> <a href="{{ route('user.delivery.index') }}">Check Order Status</a></li>
                </ul>
            </div>
            <!-- END col-4 -->
            <!-- BEGIN col-4 -->
            <div class="col-md-3">
                <h4 class="footer-header">CONTACT US</h4>
                <address>
                    <strong>PT. Nongsa Green Park</strong><br />
                    Jl. Hang Lekiu, Nongsa<br />
                    Batam, Kepulauan Riau, Indonesia<br /><br />
                    <abbr title="Phone">Phone:</abbr> (081) 1777-270<br />
                    <abbr title="Fax">Fax:</abbr> (081) 1777-270<br />
                    <abbr title="Email">Email:</abbr> <a href="mailto:nongsaintialam@gmail.com">nongsaintialam@gmail.com</a><br />
                </address>
            </div>
            <div class="col-md-3">
                <p class="text-black mb-0 f-bold">Download Aplikasi <b>Nongsa Green Park</b> di HP anda sekarang !</p>
                <div class="row">
                    <div class="col-6">
                        <a href="https://play.google.com/store" target="_blank">
                            <img class="img-fluid" src="{{asset('assets/img/get-in-google-play.png') }}" style="width: 220px;" />
                        </a>
                    </div>
                    <br>
                    <div class="col-6">
                        <a href="https://itunes.apple.com/us/genre/ios/id36?mt=8" target="_blank">
                            <img class="img-fluid" src="{{ asset('assets/img/download-at-app-store.png') }}" style="width: 220px;" />
                        </a>
                    </div>
                </div>
            </div>
            <!-- END col-4 -->
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
</div>
<div id="footer-copyright" class="copyright">
    <!-- BEGIN container -->
    <div class="container">
        <div class="copyright">
            Copyright &copy; 2017 PT. Nongsa Green Park. All rights reserved.
        </div>
    </div>
    <!-- END container -->
</div>
