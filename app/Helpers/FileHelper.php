<?php

namespace App\Helpers;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\File\isValid;
use Validator;

final class FileHelper {

    public static function upload(Request $request, $fieldName = 'file', $fileType = '', $maxSize = 50000) {
        $validator = Validator::make($request->all(), [
            $fieldName => 'mimes:'.$fileType.'|required|max:'.$maxSize,
            ]);
        if ($validator->fails()) {
            throw new ValidationException($validator->errors());
        } 
        else {
            if ($request->file($fieldName)->isValid()) {
                $name = Carbon::now().rand(1111111,9999999).'-'.
                $request->file($fieldName)->getClientOriginalName();
                $ext = $request->file($fieldName)->getClientOriginalExtension();
                $fileName = md5($name).'.'.$ext; // renaming
                $folder = 'uploads';
                $directory = public_path().'/'. $folder;
                $filePath = $request->file($fieldName);
                $filePath->move($directory, $fileName);
                return $folder.'/'.$fileName;
            }

            return false;
        }
    }

    public static function makeDir($path) {
        if (Storage::exists($path)) {
            return $path.'/';            
        }
        Storage::makeDirectory($path);
        return $path.'/';
    }

    public static function getUrl($value)
    {
        if(!$value) return '';
        if(strpos($value, 'http') !== false) return $value;
        return URL::to($value);
    }

}