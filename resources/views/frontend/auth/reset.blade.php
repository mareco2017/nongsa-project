@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN section-title -->
        <h1 class="section-title clearfix">
            Reset Password<br>
            <small>Fill the reset form</small>
        </h1>
        <br>
        <div class="row row-space-10">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">

                <div class="col-md-4"></div>
                <div class="col-md-4" style="text-align: center">
                    <div class="form-group m-b-15">
                        @include('flash::message')
                    </div>
                    <div class="form-group m-b-15 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail Address" required autofocus>

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group m-b-15 {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" required placeholder="New Password">

                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group m-b-15 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Re-type New Password">

                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group m-b-15">
                        <button type="submit" class="btn btn-success btn-block btn-login"> <i class="entypo-right-open-mini"></i> Next
                        </button>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </form>
        </div>
    </div>
</div>
@endsection
