<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Enums\AuthOption;
use App\Helpers\Enums\UserRole;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerification;
use App\Models\M01User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use PragmaRX\Google2FA\Google2FA;
use Redirect;
use RedirectsUsers;
use Session;
use Validator;

class RegisterController extends Controller
{
	use ThrottlesLogins;

	public function showRegistrationForm() {
		return view('frontend.auth.register');
	}

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:m01_users',
            'password' => 'required|min:6',
            'phone' => 'required|max:15',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function register(Request $request)
    {
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        try {

            $user = new M01User;
            $google2fa = new Google2FA();
            $user->name = $request->name;
            $user->username = $request->username;
            $user->phone = $request->phone;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->auth_option = $request->auth_option;
            $user->google_auth_code = $google2fa->generateSecretKey();
            $user->role = UserRole::CUSTOMER;
            $user->password = Hash::make($request->password);
            $user->save();

            DB::commit();

            if($request->auth_option == AuthOption::GOOGLE_AUTH) {
                return redirect()->route('user.verify.register.form', [$user]);
            } elseif ($request->auth_option == AuthOption::EMAIL) {
                $user->emailotp = strtoupper(str_random(6));
                $email = new EmailVerification(new M01User(['emailotp' => $user->emailotp, 'name' => $user->name]));
                $user->save();
                Mail::to($user->email)->send($email);
                return redirect()->route('user.verify.register.with.email');
            }
        } catch(Exception $e) {
            DB::rollback(); 
            return back();
        }
    }

    public function showVerifyForm($user) {
        $google2fa = new Google2FA();
        $google2fa_url = $google2fa->getQRCodeGoogleUrl('Nongsa Green Park', $user->email, $user->google_auth_code);
        return view('frontend.auth.google2f-register')->with('user', $user)->with('google2fa_url', $google2fa_url);
    }

    public function verifyGoogle2F(Request $request, $user) {
        if($request->google_auth_code) {
            $google2fa = new Google2FA();
            $secret = $request->input('google_auth_code');
            $valid = $google2fa->verifyKey($user->google_auth_code, $secret);
            if($valid == true) {
                $user->googleotp = 1;
                $user->save();
                Auth()->login($user, true);
                SessionHelper::setMessage("Verify Success, Welcome to Nongsa Green Park");
                return redirect()->route('user.homepages.index');
            } else {
                SessionHelper::setMessage("Google Auth Code you enter is wrong", "warning");
                return back();
            }
        } else {
            SessionHelper::setMessage("You must input google auth code", "warning");
            return back();
        }
    }
    
    public function showVerifyEmail() {
        return view('frontend.auth.email2f-register');
    }

    public function verifyEmail(Request $request) {
        $user = M01User::where('emailotp', $request->emailotp)->first();
        if($user) {
            $user->email_verification = 1;
            $user->save();
            Auth()->login($user, true);
            SessionHelper::setMessage("Verify Success, Welcome to Nongsa");
            return redirect()->route('user.homepages.index');
        } else {
            SessionHelper::setMessage("The code you enter is invalid", "warning");
            return back();
        }
    }
}