<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M03New;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class NewController extends Controller
{
    protected $view = 'backend.new';
    protected $route = 'admin.news';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view($this->view.'.index')
        ->with('newsList', 'selected')
        ->with('settingClass', 'start active open')
        ->with('newsClass', 'start active open');
    }

    public function listAllNew() {
        $newList = M03New::with('m01_user')->get();
        return Datatables::of($newList)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('admin.news.edit', ['new' => $model->id]).'"
            class="btn btn-info btn-outline btn-sm">
            <span class="fa fa-eye"></span> View
            </a>
            <a data-url="'.route('admin.news.destroy', ['new' => $model->id]).'"
            data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#new-list" data-message="Would you like to delete this news?" 
            class="btn btn-danger btn-outline btn-sm" >
            <span class="fa fa-trash-o"></span> Delete
            </a>';
        })
        ->make(true);
    }

    public function newForm() {
        return view($this->view.'.form')
        ->with('method','POST')
        ->with('newsList', 'selected')
        ->with('newsClass', 'start active open');
    }

    public function store(Request $request) {
        $this->validate($request, M03New::rules());
        try {

            DB::beginTransaction();

            $new = new M03New();
            $new->m01_user_id = Auth::user()->id;
            $new->description = $request->description;
            $new->tags = $request->tags;
            $new->event_time = $request->event_time;
            $new->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $new->title = $request->title;
            if ($request->file('picture')) {
                $upload = FileHelper::upload(request(), 'picture', 'jpeg,bmp,png');
                $new->picture = $upload;
            }
            if (!$new->save()) {
                return back()->withInput()->withErrors($new->errors());
            }

            DB::commit();
            SessionHelper::setMessage("News / Event berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function edit($new) {
        return view($this->view.'.form')
        ->with('new', $new)
        ->with('method', 'PUT')
        ->with('newsList', 'selected')
        ->with('newsClass', 'start active open');
    }

    public function update(Request $request, $new) {
        $this->validate($request, M03New::rules($new->id));

        try {

            DB::beginTransaction();
            $new->m01_user_id = Auth::user()->id;
            $new->description = $request->description;
            $new->tags = $request->tags;
            $new->event_time = $request->event_time;
            $new->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $new->title = $request->title;
            if ($request->file('picture')) {
                $upload = FileHelper::upload(request(), 'picture', 'jpeg,bmp,png');
                $new->picture = $upload;
            }
            if (!$new->save()) {
                return back()->withInput()->withErrors($new->errors());
            }

            DB::commit();
            SessionHelper::setMessage("New berhasil diupdate.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function destroy($new, Request $request) {
        $new->delete();
        if ($request->ajax() || $request->wantsJson()) {
            return response('ok.', 200);
        }
        SessionHelper::setMessage("Record Has Been Deleted!");
        return redirect()->route($this->route.'.index');
    }

}