@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN section-title -->
		<h1 class="section-title clearfix">
			How To Transaction<br>
			@if($howTo)
			<small>updated at {{ $howTo->created_at }}</small>
			@endif
		</h1>
		<br>
		<!-- END section-title -->
		<!-- BEGIN row -->
		<div class="row row-space-10">
			@if($howTo)
			<div>{{ $howTo->content }}</div>
			@endif
		</div>
		<!-- END row -->
	</div>
	<!-- END container -->
</div>
<!-- END #promotions -->
@endsection