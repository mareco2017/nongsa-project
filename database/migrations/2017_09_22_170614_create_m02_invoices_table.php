<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM02InvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m02_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_user_id');
            $table->date('created_at')->nullable();
            $table->date('expired_at');
            $table->longText('invoice_no')->nullable();
            $table->longText('in_terms_of')->nullable();
            $table->longText('remark')->nullable();
            $table->integer('bill_category')->nullable();
            $table->decimal('bill_amount', 24, 2)->default(0);
            $table->decimal('discount', 24, 2)->default(0);
            $table->decimal('service_charge', 24, 2)->default(0);
            $table->decimal('tax', 24, 2)->default(0);
            $table->decimal('sub_total', 24, 2)->default(0);
            $table->decimal('grand_total', 24, 2)->default(0);
            $table->integer('status')->default(0);
            $table->longText('delivery_status')->nullable();

            $table->foreign('m01_user_id')->references('id')->on('m01_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m02_invoices');
    }
}
