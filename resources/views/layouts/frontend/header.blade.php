<?php
    $subcategories = App\Http\Controllers\Frontend\HomepageController::getFavoredSubcategories();
    $categories =  App\Http\Controllers\Frontend\HomepageController::getOtherCategories();
?>
<div id="header" class="header container-fluid">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN header-container -->
        <div class="header-container">
            <!-- BEGIN navbar-header -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="header-logo">
                    <a href="{{ route('user.homepages.index') }}">
                     <img src="{{ asset('assets/ngp_Logo.png')}}" class="ngp-logo">
                 </a>
             </div>
         </div>
         <!-- END navbar-header -->

         <!-- BEGIN header-nav -->
         <div class="header-nav">
            <ul class="nav pull-right">
                <li class="divider"></li>
                @if(Auth::user())
                <li class="profile-info dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
                        @if(Auth::user()->photo == '')
                        <img src="{{ asset('assets/img/user.png') }}" class="user-img" alt="" /> 
                        @else
                        <img src="{{ Auth::user()->photo }}" alt="" class="img-circle" width="44" />
                        @endif
                        {{Auth::user()->name}}
                    </a> 
                    <ul class="dropdown-menu">
                        <li> 
                            <a href="{{ route('user.change.profile') }}"> 
                                Edit Profile
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('user.order.index') }}">
                                Order History
                            </a>
                        </li>
                        <li> 
                            <a href="{{ route('user.logout') }}"> 
                                Log Out
                            </a>
                        </li>
                    </ul>
                </li> 
                @else
                <li>
                    <a href="{{ route('user.login.form') }}">
                        <img src="{{ asset('assets/img/user.png') }}" class="user-img" alt="" /> 
                        <span class="hidden-md hidden-sm hidden-xs">Login / Register</span>
                    </a>
                </li>
                @endif
            </li>
        </ul>
    </div>
    <!-- END header-nav -->
</div>
<!-- END header-container -->
</div>
<!-- END container -->
</div>
<div id="top-nav" class="nav-bar-background-color">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown dropdown-hover">
                    <a href="#" data-toggle="dropdown"><img src="" class="flag-img" alt="" /> Tanaman Hias <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu">
                        @foreach ($subcategories as $subcategory)
                        <li><a href="{{ route('user.product.subcategory', ['category_slug_url' => $subcategory->category->slug_url, 'subcategory_slug_url' => $subcategory->slug_url]) }}" class='f-14'>{{ $subcategory->name }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li class="dropdown dropdown-hover">
                    <a href="#" data-toggle="dropdown"><img src="" class="flag-img" alt="" /> Tanaman Lainnya <i class="fa fa-angle-down"></i></a>
                    <ul class="dropdown-menu">
                        @foreach ($categories as $category)
                        <li><a href="{{ route('user.product.category', $category->slug_url)}}" class='f-14'>{{ $category->name }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li class="{{ $aboutUsClass or '' }}">
                    <a href="{{ route('user.abouts.index') }}">About</a>
                </li>
                <li class="{{ $feedbackClass or '' }}">
                    <a href="{{ route('user.feedback.index') }}">Feedback</a>
                </li>
                <li class="{{ $contactUsClass or '' }}">
                    <a href="{{ route('user.contact-us.index') }}">Contact</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-hover">
                    <form class="navbar-form full-width" action="{{route('search.product.view')}}">
                        <div class="form-group search">
                            {!! Form::text('search', null, array('placeholder' => 'Enter keyword','class' => 'form-control half-width mg-auto h-40','id'=>'search_text')) !!}
                            <span class="fa fa-search"></span>
                            <div id="searchResult" class="dropdown-menu dropdown-menu-cart p-0"></div>
                        </div>
                    </form>
                </li>
                <li class="dropdown dropdown-hover">
                    <a href="{{ route('user.cart.index') }}" class="header-cart">
                        <i class="fa fa-shopping-bag"></i>
                        <span class="total" id="cart-badge">
                        </span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- END container -->
</div>
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function() {
        $('#search_text').keyup(function(e){
            var search = $("#search_text").val();
            $.ajax({
                url:  "{{ route('search.product') }}",
                data: { 'search' : search, '_token' : '{{csrf_token() }}'},
                type: 'POST',
                success: function(result) {
                    $("#searchResult").show();
                    $("#searchResult").html(result);
                    if(search == '') {
                        $("#searchResult").hide();
                    }
                }
            });
        });

        getCartCount();

    });

    function getCartCount() {
        $.ajax({
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: "{{ route('user.cart.count') }}",
            method: "GET",
            data: null,
            success: function (data) {
                if (data.qty == 0) {
                    $("#cart-badge").css("visibility","hidden");
                } else {
                    console.log(data.qty);
                    $("#cart-badge").css("visibility","visible");
                    $('#cart-badge').text(data.qty);
                }
            }
        });
    }
</script>
@endpush
