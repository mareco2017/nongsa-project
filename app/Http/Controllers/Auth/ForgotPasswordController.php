<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Enums\AuthOption;
use App\Helpers\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerification;
use App\Models\M01User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index() {
        return view('auth.passwords.email')->with('title','Reset');
    }

    public function frontendIndex() {
        return view('frontend.auth.reset_form')->with('title','Reset');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);
        $user = M01User::where('email','=', $request->email)->first();
        if($user) {
            if($user->auth_option == AuthOption::GOOGLE_AUTH) {
                if($user->role == UserRole::CUSTOMER) {
                    return redirect()->route('user.verify.password.google2f.form', [$user]);
                } else {
                    return redirect()->route('admin.verify.password.google2f.form', [$user]);
                }
            } elseif($user->auth_option == AuthOption::EMAIL) {
                if($user->role == UserRole::CUSTOMER) {
                    $user->emailotp = strtoupper(str_random(6));
                    $email = new EmailVerification(new M01User(['emailotp' => $user->emailotp, 'name' => $user->name]));
                    $user->save();
                    Mail::to($user->email)->send($email);
                    return redirect()->route('user.verify.reset.email', [$user]);
                } else {
                    $user->emailotp = strtoupper(str_random(6));
                    $email = new EmailVerification(new M01User(['emailotp' => $user->emailotp, 'name' => $user->name]));
                    $user->save();
                    Mail::to($user->email)->send($email);
                    return redirect()->route('admin.verify.reset.email', [$user]);
                }
            }
        }
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
        ? $this->sendResetLinkResponse($response)
        : $this->sendResetLinkFailedResponse($request, $response);
    }

}
