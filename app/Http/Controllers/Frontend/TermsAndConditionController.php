<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M02Outlet;
use App\Models\M03TermsAndCondition;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class TermsAndConditionController extends Controller
{
    protected $view = 'frontend.vendor.termsandcondition';
    protected $route = 'vendor.terms-and-condition';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $tnc = M03TermsAndCondition::where('m02_outlet_id',Auth::user()->m02_outlet->id)->first();
        return view($this->view.'.index')
        ->with('termList', 'selected')
        ->with('termClass', 'start active open')
        ->with('tnc',$tnc);
    }

    public function store(Request $request) {
        $this->validate($request, M03TermsAndCondition::rules());
        try {

            DB::beginTransaction();
            $tnc = M03TermsAndCondition::where('m02_outlet_id',Auth::user()->m02_outlet->id)->first();
            if(!$tnc) $tnc = new M03TermsAndCondition();
            $tnc->m02_outlet_id = Auth::user()->m02_outlet->id;
            $tnc->content = $request->content;
            $tnc->created_at = Carbon::now()->format('Y-m-d H:i:s');
            if (!$tnc->save()) {
                return back()->withInput()->withErrors($tnc->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Syarat dan Ketentuan berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

}