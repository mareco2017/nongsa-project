<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM02ProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m02_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m02_outlet_id');
            $table->unsignedInteger('m03_category_id');
            $table->unsignedInteger('m03_subcategory_id')->nullable();
            $table->longText('code');
            $table->longText('name');
            $table->longText('slug_url');
            $table->longText('description')->nullable();
            $table->decimal('retail_price', 24, 2)->default(0);
            $table->decimal('distributor_price', 24, 2)->default(0);
            $table->double('commission')->default(0);
            $table->longText('picture')->nullable();
            $table->double('total_qty_in')->default(0);
            $table->double('total_qty_out')->default(0);
            $table->double('stock')->default(0);

            $table->foreign('m02_outlet_id')->references('id')->on('m02_outlets')->onDelete('cascade');
            $table->foreign('m03_category_id')->references('id')->on('m03_categories')->onDelete('cascade');
            $table->foreign('m03_subcategory_id')->references('id')->on('m03_subcategories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m02_products');
    }
}
