<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM01ReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m01_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_user_id');
            $table->unsignedInteger('m02_product_id');
            $table->longText('content');
            $table->dateTime('created_at')->nullable();

            $table->foreign('m01_user_id')->references('id')->on('m01_users')->onDelete('cascade');
            $table->foreign('m02_product_id')->references('id')->on('m02_products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m01_reviews');
    }
}
