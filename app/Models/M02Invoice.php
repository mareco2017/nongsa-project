<?php

namespace App\Models;

use App\Models\M01User;
use App\Models\M02DeliveryLog;
use App\Models\M03OutletCategory;
use Illuminate\Database\Eloquent\Model;

class M02Invoice extends Model
{
	protected $table = "m02_invoices";

	protected $fillable = [
		'invoice_no','created_at','expired_at','in_terms_of','remark',
		'bill_amount','discount','service_charge','tax','grand_total',
		'status','bill_category','sub_total','resi','delivery_status'
	];

	protected $guarded = ['m01_user_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'invoice_no' => 'required|unique:m02_invoices,invoice_no'.($id ? ','.$id : ''),
			'remark' => 'required',
		];
		return $rules;
	}

	public function m01_user() 
	{
		return $this->belongsTo(M01User::class);
	}

}