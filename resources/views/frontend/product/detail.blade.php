@extends('layouts.frontend.master')
@section('content')
@include('includes.modal_input')
@include('includes.modal_delete')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container">
		{{ csrf_field() }}
		<div id="products" class="category-container">
			<!-- BEGIN container -->
			<div class="col-md-4">
				<img src={{ $product->picture }} class="product-image">
			</div>
			<div class="col-md-8">
				<h4 class="product-detail-title">
					{{ $product->name }}
				</h4>
				<label>
					Rp. {{ $product->retail_price }}
				</label>
				<div class="cart-qty text-center">
					<div class="cart-qty-input">
						<a href="#" class="qty-control left disabled" data-click="decrease-qty" data-target="#qty">
							<span class="fa fa-minus"></span>
						</a>
						<input type="number" name="qty" value="1" class="form-control" id="qty" max="99" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
						<a href="#" class="qty-control right disabled" data-click="increase-qty" data-target="#qty">
							<span class="fa fa-plus"></span>
						</a>
					</div>
				</div>
				<div class="login-buttons form-group">
					<button type="submit" class="btn btn-buy-blue" onclick="addToCart({{$product->id}})">Buy Now</button>
				</div>
				
			</div>
		</div><br>
		<div id="products" class="category-container padding-wrapper">
			<!-- BEGIN container -->
			<h4 class="section-title align-left">
				Deskripsi
			</h4>
			<br>
			<p class="justify-text">
				{{ $product->description }}
			</p>

		</div>
		<br>
		<div id="products" class="category-container padding-wrapper">
			<!-- BEGIN container -->
			<div>
				
			</div>
			<h4 class="section-title align-left">
				Ulasan Pelanggan
				@if (!$hasReviewBefore && Auth::user())
				<a data-url="{{ route('user.product.review', $product->id) }}"
					data-toggle="modal" data-target=" #modalInput" data-title="Form Ulasan" data-message="Masukkan Ulasan" 
					class="btn btn-success btn-sm float-right" >
					<span class="fa fa-plus"></span> Tambah Ulasan
				</a>
				@endif
			</h4>
			<br>
			@if ($reviews->count() == 0)
			<p class="justify-text">Ulasan Tidak Ditemukan</p>
			@else
			<!-- BEGIN: Comments --> 
			<div class="portlet-body">
				<div class="tab-content">
					@if ($reviews->count() > 0)
					<div class="tab-pane active" id="portlet_comments_1">
						<div class="mt-comments">
							@foreach ($reviews as $review)
							<div class="mt-comment">
								<div class="mt-comment-img">
									@if ($review->m01_user->profile_pic)
									<img src={{ $review->m01_user->profile_pic }} class="user-review-image"/>
									@else
									<img src={{ asset('assets/img/user.png') }} class="user-review-image"/>
									@endif
								</div>
								<div class="mt-comment-body">
									<div class="mt-comment-info">
										<span class="mt-comment-author">{{ $review->m01_user->name }} </span>
										<span class="mt-comment-date">{{ $review->created_at }} </span>
									</div>
									<div class="mt-comment-text"> {{ $review->content }} </div>
									@if(Auth::user())
									@if ($review->m01_user->id == Auth::user()->id)
									<div class="mt-comment-details">
										<a data-url="{{ route('user.product.review.delete', $review->id) }}"
											data-toggle="modal" data-target=" #modalDelete" data-title="Konfirmasi" data-message="Apakah anda ingin menghapus ulasan ini ?" 
											class="btn btn-danger btn-outline btn-sm float-right" >
											<span class="fa fa-trash-o"></span> Hapus
										</a>
										<a data-url="{{ route('user.product.review.update', $review->id) }}"
											data-toggle="modal" data-target=" #modalInput" data-value="{{ $review->content }}" data-title="Form Ubah Ulasan" data-message="Masukkan Ulasan" 
											class="btn btn-success btn-sm float-right" >
											<span class="fa fa-refresh"></span> Ubah
										</a>
										
									</div>
									@endif
									@endif
								</div>
							</div>	
							@endforeach
						</div>
						<!-- END: Comments -->			
					</div>
					@endif
				</div>
			</div>
			@endif	
		</div>
	</div>
	<!-- END container -->
</div>
<!-- END #promotions -->
@endsection

@push('pageRelatedJs')
<script type="text/javascript">
	function addToCart($productId) {
		$.ajax({
			headers: {
				'X-CSRF-Token': $('input[name="_token"]').val()
			},
			url: "{{ route('user.product.cart') }}",
			method: "POST",
			data: { product_id : $productId,
				qty: $('#qty').val() },
				success: function (data) {
					if (data.message == null) {
						$.gritter.add( 
						{  
							text: '<i class="fa fa-info"></i> Added to Cart Successfully',  
							sticky: false,  
							time: ""
						});
						$("#cart-badge").css("visibility","visible");
						$('#cart-badge').text(data.qty);
					} else {
						$.gritter.add( 
						{    
							text: '<i class="fa fa-exclamation-circle"></i> ' + data.message,  
							sticky: false,  
							time: "",
							class_name:'gritter-light'
						})
					}
					setTimeout(function() {
						$('.alert').fadeOut('fast');
					}, 2000);
				}
			});
		
	}
</script>
@endpush