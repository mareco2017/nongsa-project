<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM02BannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m02_banners', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_picture_id');
            $table->tinyInteger('is_first')->default(0);
            $table->longText('title')->nullable();

            $table->foreign('m01_picture_id')->references('id')->on('m01_pictures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m02_banners');
    }
}
