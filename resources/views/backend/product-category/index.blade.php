@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
@include('includes.modal_delete')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Kategori Produk</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Kategori Produk</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="{{ route('admin.product-categories.new') }}" class="btn btn-m btn-primary pull-right"><span class="fa fa-plus"></span> Tambah Kategori Produk</a>
                        </div>
                        <h4>Daftar Kategori Produk</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover fullTable" id="product-category-list" width="100%">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Deskripsi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var categoryDataTable = $('#product-category-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.product-categories.list') }}",
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'name', name: 'name', 'className': 'text-center' },
            { data: 'description', name: 'description', 'className': 'text-center' },
            { data: 'action', name: 'action', 'className': 'text-center', orderable: false, searchable: false  }
            ]
        });
    });
</script>
@endpush