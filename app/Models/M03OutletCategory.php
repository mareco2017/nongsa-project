<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M03OutletCategory extends Model
{
	protected $table = "m03_outlet_categories";

	protected $fillable = [
		'name','description',
	];
	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'name' => 'required',
		];
		return $rules;
	}
}