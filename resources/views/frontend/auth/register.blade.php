@extends('layouts.frontend.master')
@section('content')
<?php 

use App\Helpers\Enums\AuthOption;


?>
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN section-title -->
        <h1 class="section-title clearfix">
            Register<br>
            <small>Register new account now</small>
        </h1>
        <br>
        <!-- END section-title -->
        <!-- BEGIN row -->
        <div class="row row-space-10">
            <form class="form-horizontal" method="POST" action="{{ route('user.register') }}">
                {{ csrf_field() }}
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="form-group m-b-15">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" autocomplete="off" value="{{ old('name') }}" required autofocus />
                    </div>
                    <div class="form-group m-b-15">
                        <input type="text" class="form-control" name="username" id="username" placeholder="Username" autocomplete="off" value="{{ old('username') }}" required />
                    </div>
                    <div class="form-group m-b-15">
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Telp" autocomplete="off" required/> 
                    </div>
                    <div class="form-group m-b-15 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group m-b-15 {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group m-b-15">
                        {{ Form::select('auth_option', AuthOption::getArray(), null, array('class' => 'form-control', 'placeholder' => 'Select an Authentication','required','id'=>'auth_option'))}}
                    </div>
                    <div class="login-buttons form-group">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Next Step</button>
                    </div>
                    <div class="m-t-20 m-b-40 p-b-40">
                        Already have an account?  <a href="{{ route('user.login.form') }}" class="text-success">Log in instead!</a>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </form>
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
</div>
<!-- END #promotions -->
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function(){
        $('#auth_option').append('<option disabled value="' + 3 + '">' + "SMS" + '</option>');
    });
</script>
@endpush