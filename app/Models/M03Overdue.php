<?php

namespace App\Models;

use App\Models\M01User;
use App\Models\M02Invoice;
use Illuminate\Database\Eloquent\Model;

class M03Overdue extends Model
{
	protected $table = "m03_overdues";

	protected $fillable = [
		'invoice_date','expired_date','grand_total','overdue_day'
	];
	public $timestamps = false;
	protected $guarded = ['m01_user_id','m02_invoice_id'];

	public function m01_user() 
	{
		return $this->belongsTo(M01User::class);
	}

	public function m02_invoice() 
	{
		return $this->belongsTo(M02Invoice::class);
	}
}