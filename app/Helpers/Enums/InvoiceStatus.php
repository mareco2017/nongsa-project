<?php

namespace App\Helpers\Enums;

final class InvoiceStatus {

	const UNPAID = 0;
	const PAID = 1;

	public static function getList() {
		return [
			InvoiceStatus::UNPAID,
			InvoiceStatus::PAID,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Unpaid";
			case 1:
				return "Paid";
		}
	}

}

?>
