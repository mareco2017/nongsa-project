<?php

namespace App\Models;

use App\Helpers\FileHelper;
use App\Models\M01Feedback;
use App\Models\M01User;
use App\Models\M03OutletCategory;
use Illuminate\Database\Eloquent\Model;

class M02Outlet extends Model
{
	protected $table = "m02_outlets";

	protected $fillable = [
		'outlet_no','name','address','phone','blok','profile_pic','status'
	];

	protected $guarded = ['m01_user_id','m03_outlet_category_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'outlet_no' => 'unique:m02_outlets,outlet_no'.($id ? ','.$id : ''),
			'phone' => 'required',
		];
		return $rules;
	}

	public function m01_user() 
	{
		return $this->belongsTo(M01User::class);
	}

	public function m03_outlet_category()
	{
		return $this->belongsTo(M03OutletCategory::class);
	}

	public function m01_feedback() 
    {
        return $this->hasMany(M01Feedback::class);
    }

	public function getPictureAttribute(){
		return FileHelper::getUrl($this->profile_pic);
    }
}