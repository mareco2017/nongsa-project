<?php

namespace App\Helpers\Enums;

final class OrderStatus {

	const WAITING_FOR_PAYMENT = 0;
	const ON_SHIPPING = 1;
	const COMPLETED = 2;
	const ON_VERIFICATION = 3;

	public static function getList() {
		return [
			OrderStatus::WAITING_FOR_PAYMENT,
			OrderStatus::ON_SHIPPING,
			OrderStatus::COMPLETED,
			OrderStatus::ON_VERIFICATION
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Waiting For Payment";
			case 1:
				return "On Shipping";
			case 2:
				return "Completed";
			case 3:
				return "On Verification";
		}
	}

}

?>
