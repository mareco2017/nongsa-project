<?php

namespace App\Helpers\Enums;

final class AuthOption {

	const GOOGLE_AUTH = 1;
	const EMAIL = 2;
	// const SMS = 3;

	public static function getList() {
		return [
			AuthOption::GOOGLE_AUTH,
			AuthOption::EMAIL,
			// AuthOption::SMS
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 1:
				return "Google Auth";
			case 2:
				return "Email";
			// case 3:
			// 	return "SMS";
		}
	}

}

?>
