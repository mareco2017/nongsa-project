@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
@include('includes.modal_delete')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>News & Event</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> News / Event</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="{{ route('admin.news.new') }}" class="btn btn-m btn-primary pull-right"><span class="fa fa-plus"></span> Tambah News / Event</a>
                        </div>
                        <h4>Daftar News & Event</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover fullTable" id="new-list" width="100%">
                            <thead>
                                <tr>
                                    <th>Gambar</th>
                                    <th>Judul</th>
                                    <th>Tanggal</th>
                                    <th>Keterangan</th>
                                    <th>Tags</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var newDataTable = $('#new-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.news.list') }}",
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
        { data: 'picture', name: 'picture', 'className': 'text-center', orderable: false, searchable: false,
            'render': function (data, type, full, meta) {
                if (!data) return '';
                return '<img src='+data+' class="table-img index-data-image" style="width: 130px; "/>';
            }
        },
        // { data: 'picture', name: 'picture', 'className': 'text-center' },
        { data: 'title', name: 'title', 'className': 'text-center' },
        { data: 'event_time', name: 'event_time', 'className': 'text-center' },
        { data: 'description', name: 'description', 'className': 'text-center' },
        { data: 'tags', name: 'tags', 'className': 'text-center' },
        { data: 'action', name: 'action', 'className': 'text-center', orderable: false, searchable: false  }
        ]
    });

        // $("#new-list thead tr#filter-row th").each(function (index) {
        //     var column = $(this).text();

        //     switch(column){
        //         case "No. Invoice":
        //         case "Email":
        //         case "Telepon":
        //         var input = '<input type="text" class="form-control" width="100%" placeholder="Search By ' + column + '" />';

        //         $(this).html(input);
        //         break;
        //     }
        // });

        // newDataTable.columns().every(function() {
        //     var that = this;
        //     $('input', this.footer()).on('keyup change', function() {
        //         if (that.search() !== this.value) {
        //             that
        //             .search(this.value)
        //             .draw();
        //         }
        //     });
        // });

    });
</script>
@endpush