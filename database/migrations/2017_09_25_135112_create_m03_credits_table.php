<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM03CreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m03_credits', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_user_id');
            $table->unsignedInteger('m02_invoice_id');
            $table->decimal('debit', 24, 2)->default(0);
            $table->decimal('credit', 24, 2)->default(0);
            $table->integer('bill_category')->nullable();
            $table->longText('remark')->nullable();
            $table->longText('reference_no')->nullable();
            $table->foreign('m01_user_id')->references('id')->on('m01_users')->onDelete('cascade');
            $table->foreign('m02_invoice_id')->references('id')->on('m02_invoices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m03_credits');
    }
}
