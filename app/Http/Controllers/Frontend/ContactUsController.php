<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\SessionHelper;
use App\Models\M01Feedback;
use App\Models\M01User;
use App\Models\M03ContactUs;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Facades\Datatables;

class ContactUsController extends Controller
{
    protected $view = 'frontend.vendor.contact-us';
    protected $route = 'vendor.contact-us';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $messages = M03ContactUs::where('m02_outlet_id',Auth::user()->m02_outlet->id)->get();
        return view($this->view.'.index')
        ->with('contactUsList', 'selected')
        ->with('contactUsClass', 'start active open')
        ->with('messages',$messages);
    }

    public function sendMessage(Request $request) {
        $data = array('name' => Auth::user()->name, 'content' => $request->content,'email' => $request->email);
        Mail::send('backend.mail.feedback', $data, function ($message) use ($data)
        {
            $emailCustomer = $data['email'];
            $message->from('nongsa.dev@gmail.com')->subject('Message Reply');
            $message->to($emailCustomer);
        });
        $feedback = M03ContactUs::where('id','=', $request->id)->first();
        $feedback->status = 1;
        $feedback->save();
        
        SessionHelper::setMessage("Pesan berhasil dikirim.");
        return redirect()->route($this->route.'.index');

    }

}