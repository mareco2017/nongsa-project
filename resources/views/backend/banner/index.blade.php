@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
@include('includes.modal_delete')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Banner</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Banner Iklan</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8" style="border: 1px solid;">
                {!! Form::open(['url' => route('admin.banners.save'),'method' => 'POST', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
                <div class="col-md-2">
                    <p><h4>Upload File :</h4></p>
                </div>
                <div class="col-md-8">
                    <p><h5>{{ Form::file('url', array('style' => 'width: 100%')) }}</h5></p>
                </div>
                <div class="col-md-2">
                    <p><h5><button type="submit" class="btn red btn-m"><span class="fa fa-save"></span> Save / Terbitkan</button></h5></p>
                </div>
                {{ Form::close() }}
            </div>
            <div class="col-md-2"></div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8" style="border: 1px solid;">
                <div class="col-md-12">
                    <p><h5>Gambar Banner :</h5></p>
                </div>
                <div class="col-md-12" style="text-align: center;">
                    @if($banners)
                    @foreach($banners as $banner)
                    @if($banner->is_first == 1)
                    <div class="col-md-4 container">
                        <img src="{{ asset($banner->m01_picture->url) }}" style="width: 270px; height: 270px; margin-bottom: 30px; border: 5px solid green" class="image"/>
                        <div class="middle">
                            <a href="{{ route('admin.banners.destroy', $banner->id) }}" class="text"><i class="fa fa-trash"></i> Hapus</a>
                        </div>
                    </div>
                    @else
                    <div class="col-md-4 container">
                        <img src="{{ asset($banner->m01_picture->url) }}" style="width: 270px; height: 270px; margin-bottom: 30px;" class="image"/>
                        <div class="middle">
                            <a href="{{ route('admin.banners.destroy', $banner->id) }}" class="text"><i class="fa fa-trash"></i> Hapus</a>
                        </div>
                        <div style="margin-top: -16px; margin-bottom: 15px;"><a href="{{ route('admin.banners.first', $banner->id) }}" class="text-banner"><i class="fa fa-check"></i> Set Utama</a></div>
                    </div>
                    @endif
                    @endforeach
                    @else
                    <p><h5>No Banner</h5></p>
                    @endif
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-md-6">
                <h1 class="page-title"> Store Banner</h1>
                <div class="col-md-6">
                    @if($storeBanner)
                    <img src="{{ asset($storeBanner->m01_picture->url) }}" style="width: 270px; height: 270px; margin-bottom: 30px;" class="image"/>
                    @else
                    <img src="{{ asset('uploads/noimg.jpg') }}" style="width: 270px; height: 270px; margin-bottom: 30px;" class="image"/>
                    @endif
                </div>
                <div class="col-md-6">
                    {!! Form::open(['url' => route('admin.storebanners.save'),'method' => 'POST', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
                    <p><h4>Upload Store Banner</h4></p>
                    <p><h5>{{ Form::file('url', array('style' => 'width: 100%')) }}</h5></p>
                    <p><h5><button type="submit" class="btn red btn-m"><span class="fa fa-save"></span> Save / Terbitkan</button></h5></p>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="col-md-6">
                <h1 class="page-title"> Product Banner</h1>
                <div class="col-md-6">
                    @if($productBanner)
                    <img src="{{ asset($productBanner->m01_picture->url) }}" style="width: 270px; height: 270px; margin-bottom: 30px;" class="image"/>
                    @else
                    <img src="{{ asset('uploads/noimg.jpg') }}" style="width: 270px; height: 270px; margin-bottom: 30px;" class="image"/>
                    @endif
                </div>
                <div class="col-md-6">
                    {!! Form::open(['url' => route('admin.productbanners.save'),'method' => 'POST', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
                    <p><h4>Upload Product Banner</h4></p>
                    <p><h5>{{ Form::file('url', array('style' => 'width: 100%')) }}</h5></p>
                    <p><h5><button type="submit" class="btn red btn-m"><span class="fa fa-save"></span> Save / Terbitkan</button></h5></p>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
