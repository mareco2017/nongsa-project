<?php

namespace App\Http\Controllers;

use App\Helpers\Enums\UserRole;
use App\Helpers\SessionHelper;
use Auth;
use Illuminate\Http\Request;
use PragmaRX\Google2FA\Google2FA;

class VerificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest');
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function showVerifyForm($user) {
        $google2fa = new Google2FA();
        $google2fa_url = $google2fa->getQRCodeGoogleUrl('Nongsa', $user->email, $user->google_auth_code);
        if($user->role == UserRole::ADMIN || $user->role == UserRole::VENDOR) {
            return view('backend.auth.google2f-login')->with('user', $user)->with('google2fa_url', $google2fa_url);
        } elseif ($user->role == UserRole::CUSTOMER) {
            return view('frontend.auth.google2f-login')->with('user', $user)->with('google2fa_url', $google2fa_url);
        }
    }

    public function verifyGoogle2F(Request $request, $user) {
        if($request->google_auth_code) {
            $google2fa = new Google2FA();
            $secret = $request->input('google_auth_code');
            $valid = $google2fa->verifyKey($user->google_auth_code, $secret);
            if($valid == true) {
                if($user->role == UserRole::CUSTOMER) {
                    Auth()->login($user, true);
                    SessionHelper::setMessage("Verify Success, Welcome to Nongsa");
                    return redirect()->route('user.homepages.index');
                } elseif ($user->role == UserRole::ADMIN) {
                    Auth()->login($user, true);
                    SessionHelper::setMessage("Verify Success, Welcome to Nongsa Admin");
                    return redirect()->route('admin.dashboards.index');
                } elseif ($user->role == UserRole::VENDOR) {
                    Auth()->login($user, true);
                    SessionHelper::setMessage("Verify Success, Welcome to Nongsa Admin");
                    return redirect()->route('vendor.dashboards.index');
                }
            } else {
                SessionHelper::setMessage("Google Auth Code you enter is wrong", "warning");
                return back();
            }
        } else {
            SessionHelper::setMessage("You must input google auth code", "warning");
            return back();
        }
    }

    public function verify(Request $request, $user) {
        if($request->emailotp) {
            if($request->emailotp == $user->emailotp) {
                if($user->role == UserRole::CUSTOMER) {
                    $user->email_verification = 1;
                    $user->save();
                    Auth()->login($user, true);
                    SessionHelper::setMessage("Verify Success, Welcome to Nongsa");
                    return redirect()->route('user.homepages.index');
                } elseif ($user->role == UserRole::VENDOR) {
                    $user->email_verification = 1;
                    $user->save();
                    Auth()->login($user, true);
                    SessionHelper::setMessage("Verify Success, Welcome to Vendor Nongsa");
                    return redirect()->route('vendor.dashboards.index');
                }
            } else {
                SessionHelper::setMessage("The code you enter is invalid", "warning");
                return back();
            }
        } else {
            SessionHelper::setMessage("You must input the confirmation code", "warning");
            return back();
        }
    }

    public function showVerifyEmail($user) {
        if($user->role == UserRole::CUSTOMER) {
            return view('frontend.auth.email2f-login')->with('user',$user);
        } elseif($user->role == UserRole::VENDOR) {
            return view('backend.auth.login_email_verify')->with('user', $user);
        }
    }

}
