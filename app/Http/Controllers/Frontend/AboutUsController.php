<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M03AboutUs;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AboutUsController extends Controller
{
    protected $view = 'frontend.vendor.about-us';
    protected $route = 'vendor.about-us';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $aboutUs = M03AboutUs::where('m02_outlet_id',Auth::user()->m02_outlet->id)->first();
        return view($this->view.'.index')
        ->with('aboutUsList', 'selected')
        ->with('aboutUsClass', 'start active open')
        ->with('aboutUs',$aboutUs);
    }

    public function store(Request $request) {
        $this->validate($request, M03AboutUs::rules());
        try {

            DB::beginTransaction();
            $aboutUs = M03AboutUs::where('m02_outlet_id',Auth::user()->m02_outlet->id)->first();
            if(!$aboutUs) $aboutUs = new M03AboutUs();
            $aboutUs->m02_outlet_id = Auth::user()->m02_outlet->id;
            $aboutUs->content = $request->content;
            $aboutUs->created_at = Carbon::now()->format('Y-m-d H:i:s');
            if (!$aboutUs->save()) {
                return back()->withInput()->withErrors($aboutUs->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Tentang Kami berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

}