<?php

namespace App\Models;

use App\Models\M01Order;
use Illuminate\Database\Eloquent\Model;

class M01Payment extends Model
{
	protected $table = "m01_payments";

	protected $fillable = [
		'payment_date','payment_proof','amount'
	];

	protected $guarded = ['m01_order_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
		];
		return $rules;
	}

	public function m01_order() 
	{
		return $this->belongsTo(M01Order::class);
	}
}