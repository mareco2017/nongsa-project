@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
@include('includes.modal_delete')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Pengiriman</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Atur Pengiriman</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="{{ route('admin.shipping-methods.new') }}" class="btn btn-m btn-primary pull-right"><span class="fa fa-plus"></span> Tambah Pengiriman</a>
                        </div>
                        <h4>Daftar Pengiriman</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover fullTable" id="delivery-list" width="100%">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Action</th>
                                </tr>
                                <tr id="filter-row">
                                    <th class="tr-align-center">Nama</th>
                                    <th class="tr-align-center">Harga</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var deliveryDataTable = $('#delivery-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.shipping-methods.list') }}",
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'name', name: 'name', 'className': 'text-center' },
            { data: 'price', name: 'price', 'className': 'text-center' },
            { data: 'action', name: 'action', 'className': 'text-center', orderable: false, searchable: false  }
            ]
        });

        $("#delivery-list thead tr#filter-row th").each(function (index) {
            var column = $(this).text();

            switch(column){
                case "Nama":
                var input = '<input type="text" class="text-input form-control" width="100%" placeholder="Search By ' + column + '" />';

                $(this).html(input);
                break;
            }
        });

        $(".text-input").on("keyup change", function () { 
            deliveryDataTable
            .column($(this).parent().index()) 
            .search(this.value) 
            .draw(); 
        }); 

    });
</script>
@endpush