<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM03NewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m03_news', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_user_id');
            $table->longText('title')->nullable();
            $table->longText('description')->nullable();
            $table->datetime('event_time')->nullable();
            $table->longText('picture')->nullable();
            $table->longText('tags')->nullable();
            $table->timestamp('created_at')->nullable();

            $table->foreign('m01_user_id')->references('id')->on('m01_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m03_news');
    }
}
