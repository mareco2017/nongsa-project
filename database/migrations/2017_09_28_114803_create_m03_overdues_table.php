<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM03OverduesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m03_overdues', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_user_id');
            $table->unsignedInteger('m02_invoice_id');
            $table->date('invoice_date');
            $table->date('expired_date');
            $table->decimal('grand_total', 24, 2)->default(0);
            $table->longText('overdue_day')->nullable();

            $table->foreign('m01_user_id')->references('id')->on('m01_users')->onDelete('cascade');
            $table->foreign('m02_invoice_id')->references('id')->on('m02_invoices')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m03_overdues');
    }
}
