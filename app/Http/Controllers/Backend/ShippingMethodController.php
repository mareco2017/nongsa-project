<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M03ShippingMethod;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class ShippingMethodController extends Controller
{
    protected $view = 'backend.shipping-method';
    protected $route = 'admin.shipping-methods';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view($this->view.'.index')
        ->with('deliveryList', 'selected')
        ->with('settingClass', 'start active open')
        ->with('deliveryClass', 'start active open');
    }

    public function listAllShippingMethod() {
        $deliveryList = M03ShippingMethod::all();
        return Datatables::of($deliveryList)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('admin.shipping-methods.edit', ['delivery' => $model->id]).'"
            class="btn btn-info btn-outline btn-sm">
            <span class="fa fa-eye"></span> View
            </a>
            <a data-url="'.route('admin.shipping-methods.destroy', ['delivery' => $model->id]).'"
            data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#delivery-list" data-message="Would you like to delete this shipping method?" 
            class="btn btn-danger btn-outline btn-sm" >
            <span class="fa fa-trash-o"></span> Delete
            </a>';
        })
        ->make(true);
    }

    public function newForm() {
        return view($this->view.'.form')
        ->with('method','POST')
        ->with('settingClass', 'start active open')
        ->with('deliveryList', 'selected')
        ->with('deliveryClass', 'start active open');
    }

    public function store(Request $request) {
        $this->validate($request, M03ShippingMethod::rules());
        try {

            DB::beginTransaction();

            $shippingMethod = new M03ShippingMethod();
            $shippingMethod->name = $request->name;
            $shippingMethod->price = $request->price;
            $shippingMethod->created_at = Carbon::now()->format('Y-m-d H:i:s');

            if (!$shippingMethod->save()) {
                return back()->withInput()->withErrors($shippingMethod->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Pengiriman berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function edit($m03_shipping_method) {
        return view($this->view.'.form')
        ->with('delivery', $m03_shipping_method)
        ->with('method', 'PUT')
        ->with('settingClass', 'start active open')
        ->with('deliveryList', 'selected')
        ->with('deliveryClass', 'start active open');
    }

    public function update(Request $request, $shippingMethod) {
        $this->validate($request, M03ShippingMethod::rules($shippingMethod->id));

        try {

            DB::beginTransaction();
            $shippingMethod->name = $request->name;
            $shippingMethod->price = $request->price;
            $shippingMethod->created_at = Carbon::now()->format('Y-m-d H:i:s');
            if (!$shippingMethod->save()) {
                return back()->withInput()->withErrors($shippingMethod->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Pengiriman berhasil diupdate.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function destroy($m03_shipping_method, Request $request) {
        $m03_shipping_method->delete();
        if ($request->ajax() || $request->wantsJson()) {
            return response('ok.', 200);
        }
        return redirect()->route($this->route.'.index');
    }

}