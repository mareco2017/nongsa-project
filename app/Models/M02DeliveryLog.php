<?php

namespace App\Models;

use App\Models\M02VendorSale;
use Illuminate\Database\Eloquent\Model;

class M02DeliveryLog extends Model
{
	protected $table = "m02_delivery_logs";

	protected $fillable = [
		'delivery_date','location','status'
	];

	protected $guarded = ['m02_vendor_sale_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'location' => 'required',
			'delivery_date' => 'required'
		];
		return $rules;
	}

	public function m02_vendor_sale()
	{
		return $this->belongsTo(M02VendorSale::class);
	}
}