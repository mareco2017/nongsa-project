@extends('layouts.backend.master')
@section('content')

<?php 

use App\Helpers\Enums\BillCategory;
use App\Helpers\Enums\InvoiceStatus;
use Carbon\Carbon;

?>

@if (isset($invoice))
{!! Form::model($invoice, ['url' => route('admin.invoices.update', ['invoice' => $invoice]),'method' => 'PUT', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@else
{!! Form::open(['url' => route('admin.invoices.save'),'method' => 'POST', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@endif

<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Invoice Tagihan</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Invoice</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('invoice_no','No.Invoice :')}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::text('invoice_no', null, array('class' => 'form-control', 'placeholder' => 'No. Invoice','data-parsley-required' => 'true'))}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::label('status','Status :')}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::select('status', InvoiceStatus::getArray(), null, array('class' => 'form-control','placeholder' => 'Pilih Status Invoice...','data-parsley-required' => 'true'))}}
                                </div>
                                <br><br><br>
                                <div class="col-md-3">
                                    {{ Form::label('bill_category','Bill Kategori :')}}
                                </div>
                                <div class="col-md-3">
                                    {{ Form::select('bill_category', BillCategory::getArray(), null, array('class' => 'form-control', 'placeholder' => 'Pilih Kategori Invoice...','data-parsley-required' => 'true'))}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('created_at', 'Tanggal Generate')}}
                                        {{ Form::text('created_at', Carbon::now()->format('Y-m-d'), array('class' => 'form-control','readonly'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('expired_at', 'Tanggal Expired')}}
                                        {{ Form::text('expired_at', null, array('class' => 'form-control dateonlypicker','placeholder' => 'Tanggal Expired','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('in_terms_of', 'Perihal')}}
                                        {{ Form::text('in_terms_of', null, array('class' => 'form-control', 'placeholder' => 'Perihal','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('m01_user_id', 'Kepada')}}
                                        {{ Form::select('m01_user_id', $listOfUser, null, array('class' => 'form-control', 'placeholder' => 'Pilih Outlet...','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('remark', 'Keterangan')}}
                                        {{ Form::text('remark', null, array('class' => 'form-control', 'placeholder' => 'Keterangan','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('bill_amount', 'Nilai Tagihan')}}
                                        {{ Form::number('bill_amount', null, array('class' => 'form-control', 'placeholder' => 'Nilai Tagihan','data-parsley-required' => 'true','id' => 'selectedAmount'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('service_charge', 'Service Charge (%)')}}
                                        {{ Form::number('service_charge', null, array('class' => 'form-control', 'placeholder' => 'Service Charge','id' => 'selectedService'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('discount', 'Diskon (%)')}}
                                        {{ Form::number('discount', null, array('class' => 'form-control', 'placeholder' => 'Diskon','id' => 'selectedDiscount'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('sub_total', 'Sub Total')}}
                                        {{ Form::number('sub_total', null, array('class' => 'form-control', 'placeholder' => 'Sub Total','id' => 'selectedSubTotal','readonly'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('tax', 'Pajak (%)')}}
                                        {{ Form::number('tax', null, array('class' => 'form-control', 'placeholder' => 'Pajak','id' => 'selectedTax'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('grand_total', 'Final Total Tagihan')}}
                                        {{ Form::number('grand_total', null, array('class' => 'form-control', 'placeholder' => 'Final Total Tagihan', 'readonly'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        <button type="submit" class="btn red btn-lg"><span class="fa fa-save"></span> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function(){

        var grandTotal = $('input[name="grand_total"]');
        var selectedAmount = $('#selectedAmount');
        var selectedDiscount = $('#selectedDiscount');
        var selectedService = $('#selectedService');
        var selectedSubTotal = $('#selectedSubTotal');
        var selectedTax = $('#selectedTax');

        selectedAmount.on('keyup', function(){
            ajaxToogleGrandTotal(this.value*selectedService.val()/100+parseFloat(selectedAmount.val()), grandTotal);
        });

        selectedService.on('keyup', function(){
            ajaxToogleGrandTotal(this.value*selectedAmount.val()/100+parseFloat(selectedAmount.val()), grandTotal);
        });

        selectedDiscount.on('keyup', function(){
            ajaxToogleGrandTotal(selectedService.val()*selectedAmount.val()/100+parseFloat(selectedAmount.val()), grandTotal);
        });

        selectedTax.on('keyup', function(){
            ajaxToogleGrandTotal(selectedService.val()*selectedAmount.val()/100+parseFloat(selectedAmount.val()), grandTotal);
        });

        function ajaxToogleGrandTotal(amount, obj){
            $.ajax({
                url: '{{ route('admin.invoices.grandTotal') }}',
                type: 'POST',
                data: {
                    amount: amount,
                    _token: '{{csrf_token()}}'
                },
                success: function(result) {
                    var subTotal = parseFloat(result)-result*selectedDiscount.val()/100;
                    selectedSubTotal.val(subTotal);

                    var grandTtl = parseFloat(subTotal)+subTotal*selectedTax.val()/100;
                    obj.val(grandTtl);
                }
            });
        }

        function readURL(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageSelect").change(function(){
            console.log('Change!');
            readURL(this);
        });
    });
</script>
@endpush