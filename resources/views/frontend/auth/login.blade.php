@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN section-title -->
        <h1 class="section-title clearfix">
            Login<br>
            <small>Access to your account now</small>
        </h1>
        <br>
        <!-- END section-title -->
        <!-- BEGIN row -->
        <div class="row row-space-10">
            <form class="form-horizontal" method="POST" action="{{ route('user.login') }}">
                {{ csrf_field() }}
                    @include('includes.session_message')
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="form-group m-b-15 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group m-b-15 {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="login-buttons form-group">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Sign in</button>
                    </div>
                    <a class="btn btn-link" href="{{ route('user.password.request.index') }}">
                        Forgot Your Password?
                    </a>
                    <div class="m-t-20 m-b-40 p-b-40">
                        Not a member yet? Click <a href="{{ route('user.register.form') }}" class="text-success">here</a> to register.
                    </div>
                </div>
                <div class="col-md-4"></div>
            </form>
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
</div>
<!-- END #promotions -->
@endsection