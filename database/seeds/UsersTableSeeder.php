<?php

use App\Helpers\Enums\UserRole;
use App\Models\M01User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            DB::beginTransaction();
            
            $user = new M01User;
            $user->name = 'Mr. Nongsa Admin';
            $user->username = 'nongsa88';
            $user->email = 'admin@admin.com';
            $user->phone = '085335566778';
            $user->password = bcrypt('admin');
            $user->role = UserRole::ADMIN;
            $user->save();

            $this->command->info('Admin created');

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->command->info($e);
        }
    }
}
