<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Enums\OrderStatus;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01Order;
use App\Models\M01User;
use App\Models\M02DeliveryLog;
use App\Models\M02Invoice;
use App\Models\M02VendorSale;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class DeliveryLogController extends Controller
{
    protected $view = 'frontend.vendor.deliverylog';
    protected $route = 'vendor.deliverylogs';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $deliveryLogs = null;
        return view($this->view.'.index')
        ->with('deliverylogList', 'selected')
        ->with('deliverylogClass', 'start active open')
        ->with('deliveryLogs', $deliveryLogs);
    }

    public function listAllInvoiceDeliveryLog() {
        $vendorSales = M02VendorSale::where('m02_outlet_id',Auth::user()->m02_outlet->id)->with('m01_order')->select('m02_vendor_sales.id','m02_vendor_sales.m01_order_id','m02_vendor_sales.resi_no','m02_vendor_sales.delivery_status')->get();

        return Datatables::of($vendorSales)
        ->addColumn('action', function ($model) {
            if ($model->resi_no != null) {
                return '
                <a href="'.route('vendor.deliverylogs.edit', ['vendorSale' => $model->id]).'"
                class="btn btn-info btn-outline btn-sm">
                <span class="fa fa-eye"></span> Lihat Status Pengiriman
                </a>';
            }
            else {
                return '
                <a data-url="'.route('vendor.deliverylogs.inputResi', ['vendorSale' => $model->id]).'"
                data-toggle="modal" data-target=" #modalInput" data-title="Form Nomor Resi" data-table-name="#deliverylog-list" data-message="Masukkan Nomor Resi" 
                class="btn btn-warning btn-outline btn-sm" >
                <span class="fa fa-edit"></span> Masukkan Nomor Resi
                </a>';
            }
            
        })
        ->editColumn('m01_order.status', function($model){
            return OrderStatus::getString($model->m01_order->status);
        })
        ->make(true);
    }

    public function listSelectedDeliveryLog($vendorSaleId) {
        $selectedVendorSale = M02VendorSale::where('id',$vendorSaleId)->with('m01_order')->select('m02_vendor_sales.resi_no','m02_vendor_sales.m01_order_id','m02_vendor_sales.id','m02_vendor_sales.delivery_status')->first();
        $deliveryLogs = M02DeliveryLog::where('m02_vendor_sale_id',$vendorSaleId)->get();
        return view($this->view.'.index')
        ->with('deliverylogList', 'selected')
        ->with('deliverylogClass', 'start active open')
        ->with('deliveryLogs', $deliveryLogs)
        ->with('selectedVendorSale', $selectedVendorSale);
    }

    public function inputResi($vendorSaleId, Request $request) {
        try {
            DB::beginTransaction();
            $vendorSale = M02VendorSale::where('id', $vendorSaleId)->first();
            $vendorSale->resi_no = $request->id_input;
            $vendorSale->save();

            $order = M01Order::where('id','=', $vendorSale->m01_order_id)->first();
            $order->status = OrderStatus::ON_SHIPPING;
            $order->save();
            
            DB::commit();
            SessionHelper::setMessage("Nomor Resi berhasil disimpan!");
            return redirect()->route($this->route.'.index');
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
        
    }

    public function store(Request $request) {
        $this->validate($request, M02DeliveryLog::rules());
        try {
            DB::beginTransaction();
            $deliveryLog = new M02DeliveryLog();
            $deliveryLog->m02_vendor_sale_id = $request->m02_vendor_sale_id;
            $deliveryLog->delivery_date = $request->delivery_date;

            $deliveryLog->location = $request->location;
            $deliveryLog->delivery_status = $request->delivery_status;
            if (!$deliveryLog->save()) {
                return back()->withInput()->withErrors($deliveryLog->errors());
            }
            $vendorSale = M02VendorSale::where('id',$request->m02_vendor_sale_id)->first();
            $vendorSale->delivery_status = $request->delivery_status;
            if (!$vendorSale->save()) {
                return back()->withInput()->withErrors($vendorSale->errors());
            }
            DB::commit();
            SessionHelper::setMessage("Status Pengiriman berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function close($vendorSaleId){
        try {
            DB::beginTransaction();
            $deliveryLog = new M02DeliveryLog();
            $deliveryLog->m02_vendor_sale_id = $vendorSaleId;
            $deliveryLog->delivery_date = Carbon::now()->format('Y-m-d H:i:s');

            $deliveryLog->location = "Lokasi Customer";
            $deliveryLog->delivery_status = "Telah diterima Customer";
            if (!$deliveryLog->save()) {
                return back()->withInput()->withErrors($deliveryLog->errors());
            }
            $vendorSale = M02VendorSale::where('id',$vendorSaleId)->first();
            $vendorSale->delivery_status = "Telah diterima Customer";
            if (!$vendorSale->save()) {
                return back()->withInput()->withErrors($vendorSale->errors());
            }
            $selectedVendorSales = M02VendorSale::where('m01_order_id', $vendorSale->m01_order_id)->get();
            $completed = true;
            foreach ($selectedVendorSales as $key => $selectedVendorSale) {
                if ($selectedVendorSale->delivery_status != "Telah diterima Customer") {
                    $completed = false;
                }
            }
            if ($completed) {
                $order = M01Order::where('id', $vendorSale->m01_order_id)->first();
                $order->status = 2;
                $order->save();
            }
            DB::commit();
            SessionHelper::setMessage("Status Pengiriman berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

}