<?php

namespace App\Models;

use App\Models\M03ProductCategory;
use Illuminate\Database\Eloquent\Model;

class M03ProductSubcategory extends Model
{
	protected $table = "m03_subcategories";

	protected $fillable = [
		'name','m03_category_id', 'slug_url'
	];
	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'name' => 'required',
		];
		return $rules;
	}

	public function category() 
	{
		return $this->belongsTo(M03ProductCategory::class, 'm03_category_id');
	}
}