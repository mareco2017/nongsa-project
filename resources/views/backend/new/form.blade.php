@extends('layouts.backend.master')
@section('content')

<?php 

use Carbon\Carbon;

?>

@if (isset($new))
{!! Form::model($new, ['url' => route('admin.news.update', ['new' => $new]),'method' => 'PUT', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@else
{!! Form::open(['url' => route('admin.news.save'),'method' => 'POST', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@endif

<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>News & Event</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> News / Event</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption col-md-12">
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    {{ Form::label('title','Judul Event :')}}
                                </div>
                                <div class="col-md-6">
                                    {{ Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Judul Event','data-parsley-required' => 'true'))}}
                                </div>
                                <div class="col-md-3"></div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('event_time', 'Tanggal Event / News')}}
                                        {{ Form::text('event_time', null, array('class' => 'form-control datetimepicker','placeholder' => 'Tanggal Event / News','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('tags', 'Event / News Tags')}}
                                        {{ Form::text('tags', null, array('class' => 'form-control', 'placeholder' => 'Event / News Tags','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('description', 'Keterangan / Isi Konten')}}
                                        {{ Form::textarea('description', null, array('class' => 'form-control', 'placeholder' => 'Keterangan / Isi Konten','data-parsley-required' => 'true'))}}
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        {{ Form::label('picture', 'Gambar News / Event')}}<br>
                                        @if(isset($new))
                                        <img src="{{ asset($new->picture) }}" style="width: 210px; height: 210px" id="previewImage">
                                        @else
                                        <img src="{{ asset('assets/img/noimage.png') }}" style="width: 210px; height: 210px" id="previewImage">
                                        @endif
                                        <input type="file" name="picture" id="imageSelect">
                                    </div>
                                    <div class="form-group col-md-3"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-md-3"></div>
                                    <div class="form-group col-md-6">
                                        <button type="submit" class="btn red btn-lg"><span class="fa fa-save"></span> Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function(){

        function readURL(input) {
            console.log('Read URL');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e);
                    $('#previewImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imageSelect").change(function(){
            console.log('Change!');
            readURL(this);
        });

    });
</script>
@endpush