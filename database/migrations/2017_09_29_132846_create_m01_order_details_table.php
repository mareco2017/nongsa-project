<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM01OrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m01_order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_order_id');
            $table->unsignedInteger('m02_product_id');
            $table->double('qty')->default(0);
            $table->decimal('price', 24, 2)->default(0);
            $table->decimal('subtotal', 24, 2)->default(0);

            $table->foreign('m02_product_id')->references('id')->on('m02_products')->onDelete('cascade');
            $table->foreign('m01_order_id')->references('id')->on('m01_orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m01_order_details');
    }
}
