<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Enums\AuthOption;
use App\Helpers\Enums\UserRole;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerification;
use App\Models\M01User;
use App\Models\M02Outlet;
use App\Models\M03OutletCategory;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use PragmaRX\Google2FA\Google2FA;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    
    public function showRegisterForm() {
        $outletCategory = M03OutletCategory::pluck('name','id');
        return view('auth.register')
        ->with('outletCategory', $outletCategory)
        ->with('title', 'Register');
    }

    public function register(Request $request) {
        session_start();
        try {
            DB::beginTransaction();
            $google2fa = new Google2FA();
            $user = new M01User();
            $user->name = $request->input('name');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->google_auth_code = $google2fa->generateSecretKey();
            $user->password = bcrypt($request->input('password'));
            $user->auth_option = $request->input('auth_option');
            $user->iplog = $_SERVER['REMOTE_ADDR'];
            $user->role = UserRole::VENDOR;
            if (!$user->save()) {
                return back()->withInput()->withErrors($user->errors());
            }

            $outlet = new M02Outlet();
            $outlet->m01_user()->associate($user);
            $outlet->name = $user->name;
            $outlet->username =$user->username;
            $outlet->email =$user->email;
            $outlet->address = $request->input('address');
            $outlet->phone = $user->phone;
            $outlet->m03_outlet_category_id = $request->input('m03_outlet_category_id');
            $outlet->status = 0;
            if (!$outlet->save()) {
                return back()->withInput()->withErrors($outlet->errors());
            }

            DB::commit();
        } catch(Exception $e) {
            DB::rollback(); 
            return back();
        }

        if($request->auth_option == AuthOption::GOOGLE_AUTH) {
            return redirect()->route('admin.verify.register.form', [$user]);
        } elseif ($request->auth_option == AuthOption::EMAIL) {
            $user->emailotp = strtoupper(str_random(6));
            $email = new EmailVerification(new M01User(['emailotp' => $user->emailotp, 'name' => $user->name]));
            $user->save();
            Mail::to($user->email)->send($email);
            return redirect()->route('admin.verify.register.email');
        } 
    }

    public function showVerifyForm($user) {
        $google2fa = new Google2FA();
        $google2fa_url = $google2fa->getQRCodeGoogleUrl('Nongsa Green Park', $user->email, $user->google_auth_code);
        return view('backend.auth.google2f-register')->with('user', $user)->with('google2fa_url', $google2fa_url);
    }

    public function verifyGoogle2F(Request $request, $user) {
        if($request->google_auth_code) {
            $google2fa = new Google2FA();
            $secret = $request->input('google_auth_code');
            $valid = $google2fa->verifyKey($user->google_auth_code, $secret);
            if($valid == true) {
                $user->googleotp = 1;
                $user->save();
                Auth()->login($user, true);
                SessionHelper::setMessage("Verify Success, Welcome to Nongsa Green Park Vendor");
                return redirect()->route('vendor.dashboards.index');
            } else {
                SessionHelper::setMessage("Google Auth Code you enter is wrong", "warning");
                return back();
            }
        } else {
            SessionHelper::setMessage("You must input google auth code", "warning");
            return back();
        }
    }

    public function verify(Request $request) {
        if($request->emailotp) {
            $user = M01User::where('emailotp', $request->emailotp)->first();
            if($user) {
                $user->email_verification = 1;
                $user->save();
                Auth()->login($user, true);
                SessionHelper::setMessage("Verify Success, Welcome to Nongsa Green Park");
                return redirect()->route('vendor.dashboards.index');
            } else {
                SessionHelper::setMessage("The code you enter is invalid", "warning");
                return back();
            }
        } else {
            SessionHelper::setMessage("You must input the confirmation code", "warning");
            return back();
        }
    }

    public function showVerifyEmail() {
        return view('backend.auth.register_email_verify');
    }

    public function showResetEmail($user) {
        if($user->role == UserRole::CUSTOMER) {
            return view('frontend.auth.email-reset')->with('user', $user);
        } else {
            return view('backend.auth.reset_email_verify')->with('user', $user);
        }
    }

    public function showVerifyResetForm($user) {
        $google2fa = new Google2FA();
        $google2fa_url = $google2fa->getQRCodeGoogleUrl('Nongsa Green Park', $user->email, $user->google_auth_code);
        if($user->role == UserRole::CUSTOMER) {
            return view('frontend.auth.google2f-reset')->with('user', $user)->with('google2fa_url', $google2fa_url);
        } else {
            return view('backend.auth.google2f-reset')->with('user', $user)->with('google2fa_url', $google2fa_url);
        }
    }

    public function verifyResetGoogle2F(Request $request, $user) {
        if($request->google_auth_code) {
            $google2fa = new Google2FA();
            $secret = $request->input('google_auth_code');
            $valid = $google2fa->verifyKey($user->google_auth_code, $secret);
            if($valid == true) {
                $user->googleotp = 1;
                $user->save();
                $reset_token = strtolower(str_random(64));
                DB::table('password_resets')->insert([
                    'email' => $user->email,
                    'token' => bcrypt($reset_token),
                    'created_at' => Carbon::now(),
                ]);
                if($user->role == UserRole::CUSTOMER) {
                    return redirect()->route('user.password.reset', $reset_token);
                } else {
                    return redirect()->route('admin.password.reset', $reset_token);
                }
            } else {
                SessionHelper::setMessage("Google Auth Code you enter is wrong", "warning");
                return back();
            }
        } else {
            SessionHelper::setMessage("You must input google auth code", "warning");
            return back();
        }
    }

    public function verifyReset(Request $request, $user) {
        if($request->emailotp) {
            $user = M01User::where('emailotp', $request->emailotp)->first();
            if($user) {
                $user->email_verification = 1;
                $user->save();
                $reset_token = strtolower(str_random(64));
                DB::table('password_resets')->insert([
                    'email' => $user->email,
                    'token' => bcrypt($reset_token),
                    'created_at' => Carbon::now(),
                ]);
                if($user->role == UserRole::CUSTOMER) {
                    return redirect()->route('user.password.reset', $reset_token);
                } else {
                    return redirect()->route('admin.password.reset', $reset_token);
                }
            } else {
                SessionHelper::setMessage("The code you enter is invalid", "warning");
                return back();
            }
        } else {
            SessionHelper::setMessage("You must input the confirmation code", "warning");
            return back();
        }
    }
}
