<?php

namespace App\Models;

use App\Helpers\FileHelper;
use App\Models\M02Outlet;
use Illuminate\Database\Eloquent\Model;

class M01Feedback extends Model
{
	protected $table = "m01_feedbacks";

	protected $fillable = [
		'name','email','subject','description','status','created_at'
	];

	protected $guarded = ['m02_outlet_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'subject' => 'required',
			'description' => 'required'
		];
		return $rules;
	}

	public function m02_outlet() 
	{
		return $this->belongsTo(M02Outlet::class);
	}
}