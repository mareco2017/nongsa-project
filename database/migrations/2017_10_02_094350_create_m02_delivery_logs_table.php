<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM02DeliveryLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m02_delivery_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m02_vendor_sale_id');
            $table->longText('location')->nullable();
            $table->longText('delivery_status')->nullable();
            $table->dateTime('delivery_date')->nullable();

            $table->foreign('m02_vendor_sale_id')->references('id')->on('m02_vendor_sales')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m02_delivery_logs');
    }
}
