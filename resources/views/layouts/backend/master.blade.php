<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Nongsa Green Park | Admin</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="SMS-forwarder" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="{{ asset('assets/css/fonts.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{ asset('assets/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/dropzone/basic.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/js/bootstrap-datepicker.min.css') }}" />

    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('assets/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/themes/default.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{ asset('assets/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    
    <!-- Custom -->
    <link rel="shortcut icon" href="favicon.ico" />
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
</head>
<!-- END HEAD -->
<?php 

use App\Helpers\Enums\UserRole;


?>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-wrapper">
        <div class="page-header navbar navbar-fixed-top">
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="/admin">
                        <img src="{{ asset('assets/img/logo.jpg') }}" alt="logo" class="logo-default" style="width: 162px; margin-top: 12px;" />
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                        <span></span>
                    </div>
                </div>
                <!-- END LOGO -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                @if( Auth::user()->photo == '')
                                <img alt="" class="img-circle" src="{{ asset('assets/img/avatar.png') }}" />
                                @else
                                <img alt="" class="img-circle" src="{{ Auth::user()->photo }}" />
                                @endif
                                <span class="username username-hide-on-mobile"> {{ Auth::user()->name }} </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    @if(Auth::user()->role == UserRole::ADMIN)
                                    <a href="{{ route('admin.change.password') }}">
                                        <i class="icon-user"></i> My Profile
                                    </a>
                                    @else
                                     <a href="{{ route('vendor.change.password') }}">
                                        <i class="icon-user"></i> My Profile
                                    </a>
                                    @endif
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="{{ route('admin.logout') }}">
                                        <i class="icon-key"></i> Log Out
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
        <div class="page-container">
            <div class="page-sidebar-wrapper">
                @include('layouts.backend.sidebar')
            </div>
            @yield('content')
        </div>
        <div class="page-footer">
            <div class="page-footer-inner"> 2017 &copy; Nongsa Green Park
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/js.cookie.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/jquery.blockui.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/gritter/js/jquery.gritter.js') }}"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{ asset('assets/js/layout/app.min.js') }}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/plugins/dropzone/dropzone.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/form-dropzone.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/plugins/bootstrap-daterangepicker/moment.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/js/layout/layout.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/layout/demo.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

    <!-- END THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('.dateonlypicker').datetimepicker({ format: 'YYYY-MM-DD'});
            $('.datetimepicker').datetimepicker({ format: 'YYYY-MM-DD H:mm'});

            $('#session-message').fadeIn(2000, function () {
                $('#session-message').fadeOut(2000);
            });

            jQuery.fn.preventDoubleSubmission = function() {
                $(this).on('submit',function(e){
                    var $form = $(this);

                    if ($form.data('submitted') === true) {
                        // Previously submitted - don't submit again
                        e.preventDefault();
                    } else {
                        // Mark it so that the next submit can be ignored
                        $form.data('submitted', true);
                    }
                });
                return this;
            };

            $('form').preventDoubleSubmission();
        });
    </script>
    @stack('pageRelatedJs')
</body>
</html>