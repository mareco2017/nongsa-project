@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
    <!-- BEGIN container -->
    @include('includes.session_message')
    <div class="container">
        <!-- BEGIN section-title -->
        <div class="col-md-12" style="margin-bottom: 70px;">
            <div class="col-md-4">
                @if($m02_outlet)
                <img src="{{ asset($m02_outlet->profile_pic) }}" style="width: 270px; height: 250px;">
                @else
                <img src="{{ asset('assets/img/user.png') }}" style="width: 270px; height: 250px;">
                @endif
            </div>
            <div class="col-md-8">
                <div class="col-md-12">
                    <h1>
                        {{ $m02_outlet->name }} - {{ $m02_outlet->blok}}
                    </h1>
                </div>
                <br><br>
                <br><br>
                <div class="col-md-2">
                    <span>Email : </span><br>
                    <span>Phone : </span><br>
                    <span>Address : </span><br>
                </div>
                <div class="col-md-10">
                    <span> @if($m02_outlet->email != NULL) {{ $m02_outlet->email}} @else - @endif</span><br>
                    <span> @if($m02_outlet->phone != NULL) {{ $m02_outlet->phone}} @else - @endif</span><br>
                    <span> @if($m02_outlet->address != NULL) {{ $m02_outlet->address}} @else - @endif</span><br>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom: 70px;">
            <div class="col-md-6">
                <h1 class="section-title clearfix">
                    About Us
                </h1>
                <br><br>
                <p style="text-align: justify;"> @if($about) {{ $about->content }} @else - @endif</p>
            </div>
            <div class="col-md-6">
                <h1 class="section-title clearfix">
                    How To Transaction
                </h1>
                <br><br>
                <p style="text-align: justify;"> @if($htt) {{ $htt->content }} @else - @endif</p>
            </div>
        </div>
        <div class="col-md-12" style="margin-bottom: 70px;">
            <div class="col-md-6">
                <h1 class="section-title clearfix">
                    Feedback
                </h1>
                <br>
                <div class="col-md-12">
                    <form class="form-horizontal" method="POST" action="{{ route('user.feedback.save.detail', $m02_outlet->id) }}">
                        {{ csrf_field() }}
                        <div class="form-group m-b-15">
                            <input id="name" class="form-control" name="name" placeholder="Name" required autofocus>
                        </div>
                        <div class="form-group m-b-15 {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group m-b-15">
                            <input id="subject" class="form-control" name="subject" placeholder="Subject" required autofocus>
                        </div>
                        <div class="form-group m-b-15">
                            <textarea id="description" class="form-control" rows="5" name="description" placeholder="Messages" required autofocus></textarea>
                        </div>
                        <div class="login-buttons form-group">
                            <button type="submit" class="btn btn-success btn-block btn-lg">Send</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <h1 class="section-title clearfix">
                    Contact Us
                </h1>
                <br>
                <div class="col-md-12">
                    <form class="form-horizontal" method="POST" action="{{ route('user.contact-us.save.detail', $m02_outlet->id) }}">
                        {{ csrf_field() }}
                        <div class="form-group m-b-15">
                            <input id="name" class="form-control" name="name" placeholder="Name" required autofocus>
                        </div>
                        <div class="form-group m-b-15 {{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group m-b-15">
                            <textarea id="subject" class="form-control" rows="5" name="subject" placeholder="Messages" required autofocus></textarea>
                        </div>
                        <div class="login-buttons form-group">
                            <button type="submit" class="btn btn-success btn-block btn-lg">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <h1 class="section-title clearfix">
                Terms & Conditions
            </h1>
            <br><br>
            <p style="text-align: justify;"> @if($tnc) {{ $tnc->content }} @else - @endif</p>
        </div>
        <!-- END section-title -->
    </div>
    <!-- END container -->
</div>
<!-- END #promotions -->
@endsection