@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
	<div class="container">
		<h1 class="section-title clearfix">
			News & Events<br>
		</h1>
		<br>
		@if($news)
		<div class="col-md-12">
			@foreach($news as $new)
			<a href="{{ route('user.news.detail', $new->id) }}">
				<div class="col-md-4">
					<img src="{{ asset($new->picture)}}" class="news-and-events-image">
					@if(strlen($new->title) > 70)
					<i><h5 class="news-and-events-date">{{ $new->created_at }}</h5></i>
					<h4 class="news-and-events-line-height">{{ substr($new->title,0,70)."..." }}</h4>
					@else
					<i><h5 class="news-and-events-date">{{ $new->created_at }}</h5></i>
					<h4 class="news-and-events-line-height">{{ $new->title }}</h4>
					@endif
				</div>
			</a>
			@endforeach
			@else
			<h4>Sorry, There's no News & Events at the moment</h4>
		@endif
		<br>
		</div>
	</div>
	<div class="news-and-events-pagination">
		{{ $news->links() }}
	</div>
</div>
<!-- END #promotions -->
@endsection