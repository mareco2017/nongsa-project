<?php

namespace App\Models;

use App\Models\M02Banner;
use Illuminate\Database\Eloquent\Model;

class M01Picture extends Model
{
	protected $table = "m01_pictures";

	protected $fillable = [
		'url',
	];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'url' => 'required',
		];
		return $rules;
	}

	public function banner()
	{
		return $this->hasOne(M02Banner::class);
	}
}