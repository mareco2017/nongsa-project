@extends('layouts.backend.master')
<?php 

use App\Helpers\Enums\UserRole;


?>
@section('content')
@if(Auth::user()->role == UserRole::ADMIN)
{!! Form::open(['url' => route('admin.password.save'),'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@elseif(Auth::user()->role == UserRole::VENDOR)
{!! Form::open(['url' => route('vendor.password.save'),'method' => 'POST','data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
@endif
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ redirect('/') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Change Password</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Change Password
        </h1>
        @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="{{ URL::previous() }}" class="btn btn-xs btn-primary pull-right"><span class="fa fa-reply"></span> Back</a>
                        </div>
                        <h4>Password Form</h4>
                    </div>

                    <div class="panel-body">
                        <div class="modal-body">
                            <fieldset>
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-3">
                                        {{Form::checkbox('googleotp',null, Auth::user()->googleotp)}}
                                        Google Auth
                                    </div>
                                    <div class="col-md-6">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                        <div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
                                            {{ Form::label('old_password', 'Old Password')}}
                                            {{ Form::password('old_password', array('class' => 'form-control', 'placeholder' => 'Enter your old password', 'required' => 'required', 'maxlength'=> '250'))}}
                                            @if ($errors->has('old_password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('old_password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="row">
                                   <div class="col-md-3">
                                        {{Form::checkbox('emailotp',null, Auth::user()->emailotp)}}
                                        Email
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                            {{ Form::label('password', 'New Password')}}
                                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Enter your new password', 'required' => 'required', 'maxlength'=> '250'))}}
                                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            {{ Form::label('password_confirmation', 'Re-enter Password')}}
                                            {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Re-enter your password', 'required' => 'required', 'maxlength'=> '250'))}}
                                            @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-sm btn-success"><span class="fa fa-save"></span> Save</button>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
