@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="form-group">
            <h1>Billing : Biaya Lain-lain</h1>
            @include('includes.session_message')
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse">
                        <div class="panel-body">
                            <table class="table table-striped table-hover fullTable" id="guard-list" width="100%">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Tanggal</th>
                                        <th>No. Invoice</th>
                                        <th>Total Amount Due / Debit</th>
                                        <th>Total Amount Paid / Credit</th>
                                        <th>Action</th>
                                    </tr>
                                    <tr id="filter-row">
                                        <th></th>
                                        <th></th>
                                        <th class="tr-align-center">No. Invoice</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Biaya Lain-lain Summary : </th>
                                        <th></th>
                                        <th></th>
                                        <th>{{ number_format($totalDebit ,2,'.',',')}}</th>
                                        <th>{{ number_format($totalCredit,2,'.',',') }}</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>Total Utang Saldo : {{ number_format($totalCredit-$totalDebit, 2, '.',',' ) }}</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <div class="panel panel-inverse">
                                <div class="panel-heading">
                                    <h4>Form Invoice :  <b>{{ $m03_credit->m02_invoice->invoice_no }}</b> </h4>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    {!! Form::open(['url' => route('vendor.others.save'),'method' => 'POST', 'data-parsley-validate' => 'true','enctype'=>'multipart/form-data']) !!}
                                    {{ Form::hidden('m02_invoice_id', $m03_credit->m02_invoice->id)}}
                                    <div class="portlet-">
                                        <div class="row">
                                            <div class="row">
                                                <div class="form-group col-md-3"></div>
                                                <div class="form-group col-md-6">
                                                    {{ Form::label('created_at', 'Tanggal')}}
                                                    {{ Form::text('created_at', \Carbon\Carbon::now()->format('Y-m-d'), array('class' => 'form-control dateonlypicker', 'placeholder' => 'Username','data-parsley-required' => 'true','readonly'))}}
                                                </div>
                                                <div class="form-group col-md-3"></div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-3"></div>
                                                <div class="form-group col-md-6">
                                                    {{ Form::label('reference_no', 'No. Referensi')}}
                                                    {{ Form::text('reference_no', null, array('class' => 'form-control', 'placeholder' => 'Masukkan nomor referensi','data-parsley-required' => 'true'))}}
                                                </div>
                                                <div class="form-group col-md-3"></div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-3"></div>
                                                <div class="form-group col-md-6">
                                                    {{ Form::label('remark', 'Keterangan')}}
                                                    {{ Form::textarea('remark', null, array('class' => 'form-control', 'placeholder' => 'Masukkan keterangan','data-parsley-required' => 'true'))}}
                                                </div>
                                                <div class="form-group col-md-3"></div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-md-3"></div>
                                                <div class="form-group col-md-6">
                                                    {{ Form::label('bill_amount', 'Nilai Pembayaran')}}
                                                    {{ Form::number('bill_amount', null, array('class' => 'form-control', 'placeholder' => 'Masukkan nilai pembayaran','data-parsley-required' => 'true'))}}
                                                </div>
                                                <div class="form-group col-md-3"></div>
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="form-group col-md-3"></div>
                                                <div class="form-group col-md-6">
                                                    <button type="submit" class="btn red btn-lg"><span class="fa fa-save"></span> Simpan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var guardDataTable = $('#guard-list').DataTable(
        {
            dom: "lrtip",
            orderCellsTop: true,
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('vendor.others.list') }}",
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            {
                "data": "id",
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: 'm02_invoice.created_at', name: 'm02_invoice.created_at', 'className': 'text-center' },
            { data: 'm02_invoice.invoice_no', name: 'm02_invoice.invoice_no', 'className': 'text-center' },
            { data: 'debit', name: 'debit', 'className': 'text-center' },
            { data: 'credit', name: 'credit', 'className': 'text-center' },
            { data: 'action', name: 'action', 'className': 'text-center', orderable: false, searchable: false  }
            ],
             "order": [[ 1, "asc" ]]
        });

        $("#guard-list thead tr#filter-row th").each(function (index) {
            var column = $(this).text();

            switch(column){
                case "No. Invoice":
                var input = '<input type="text" class="text-input form-control" width="100%" placeholder="Search By ' + column + '" />';

                $(this).html(input);
                break;
            }
        });

        $(".text-input").on("keyup change", function () { 
            guardDataTable
            .column($(this).parent().index()) 
            .search(this.value) 
            .draw(); 
        });

        
    });
</script>
@endpush