@extends('layouts.app')
<?php 

use App\Helpers\Enums\AuthOption;

?>
@section('content')
<div class="login-content">
    <h4 style="text-align: center">Sign Up</h4>
    <br>
    <form class="form-horizontal" method="POST" action="{{ route('admin.register') }}">
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name" class="col-md-4 control-label">Name</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="username" class="col-md-4 control-label">Username</label>

            <div class="col-md-6">
                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required>
                @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="phone" class="col-md-4 control-label">Phone</label>

            <div class="col-md-6">
                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>
                @if ($errors->has('phone'))
                <span class="help-block">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Password</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="auth_option" class="col-md-4 control-label">Outlet Category</label>
            <div class="col-md-6">
                {{ Form::select('m03_outlet_category_id', $outletCategory, null, array('class' => 'form-control', 'placeholder' => 'Please Choose one...','required'))}}
            </div>
        </div>

        <div class="form-group">
            <label for="auth_option" class="col-md-4 control-label">Authentication</label>
            <div class="col-md-6">
                {{ Form::select('auth_option', AuthOption::getArray(), null, array('class' => 'form-control', 'placeholder' => 'Please Choose one...','required','id'=>'auth_option'))}}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Register
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function(){
        $('#auth_option').append('<option disabled value="' + 3 + '">' + "SMS" + '</option>');
    });
</script>
@endpush