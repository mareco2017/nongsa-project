<?php

namespace App\Models;

use App\Models\M01User;
use App\Models\M02Invoice;
use Illuminate\Database\Eloquent\Model;

class M03Credit extends Model
{
	protected $table = "m03_credits";

	protected $fillable = [
		'debit','credit', 'bill_category'
	];
	public $timestamps = false;
	protected $guarded = ['m01_user_id','m02_invoice_id'];

	public static function rules($id = '') 
	{
		$rules = [
			'm02_invoice_id' => 'required',
		];
		return $rules;
	}
	
	public function m01_user() 
	{
		return $this->belongsTo(M01User::class);
	}

	public function m02_invoice() 
	{
		return $this->belongsTo(M02Invoice::class);
	}
}