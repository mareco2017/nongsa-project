@extends('layouts.backend.master')
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		{!! Form::open(['url' => route('vendor.how-to-transaction.save'),'method' => 'POST']) !!}
		<div class="form-group">
			<h1>How To Transaction</h1>
			@include('includes.session_message')
			<div class="content-header-gray">
				<label>Content Editor</label>
			</div>
			@if ($howToTransaction)
				<textarea class="content-text-area" name="content" placeholder="Enter any text here.." >{{ $howToTransaction->content }}</textarea>
			@else
				<textarea class="content-text-area" name="content" placeholder="Enter any text here.." ></textarea>
			@endif
		</div>
		<div class="form-group col-md-6">
			<button type="submit" class="btn green btn-lg">Simpan</button>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection