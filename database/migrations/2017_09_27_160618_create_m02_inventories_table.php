<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM02InventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m02_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m02_product_id');
            $table->longText('reference_no');
            $table->longText('remark')->nullable();
            $table->double('qty_in')->default(0);
            $table->double('qty_out')->default(0);
            $table->date('created_at')->nullable();

            $table->foreign('m02_product_id')->references('id')->on('m02_products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m02_inventories');
    }
}
