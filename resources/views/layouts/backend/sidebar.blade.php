<?php 

use App\Helpers\Enums\UserRole;
use Illuminate\Support\Facades\Auth;



?>
<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

        <li class="sidebar-toggler-wrapper hide">
            <div class="sidebar-toggler">
                <span></span>
            </div>
        </li>
        @if(Auth::user()->role == UserRole::ADMIN)
        <li class="nav-item {{ $dashboardClass or ''}}">
            <a href="{{ route('admin.dashboards.index') }}" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Beranda</span>
                <span class="{{ $dashboardList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $categoryClass or '' }}">
            <a href="{{ route('admin.categories.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-database"></i>
                <span class="title">Kategori Outlet</span>
                <span class="{{ $categoryList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $outletClass or '' }}">
            <a href="{{ route('admin.outlets.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-address-card"></i>
                <span class="title">Profil Outlet</span>
                <span class="{{ $outletList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $productCategoryClass or '' }}">
            <a href="{{ route('admin.product-categories.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-tag"></i>
                <span class="title">Kategori Produk</span>
                <span class="{{ $productCategoryList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $productSubcategoryClass or '' }}">
            <a href="{{ route('admin.product-subcategories.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-tags"></i>
                <span class="title">Subkategori Produk</span>
                <span class="{{ $productSubcategoryList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $bannerClass or '' }}">
            <a href="{{ route('admin.banners.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-picture-o"></i>
                <span class="title">Banner Iklan</span>
                <span class="{{ $bannerList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $invoiceClass or '' }}">
            <a href="{{ route('admin.invoices.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-folder-open-o"></i>
                <span class="title">Invoice Tagihan</span>
                <span class="{{ $invoiceList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $creditClass or '' }}">
            <a href="{{ route('admin.credits.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-server"></i>
                <span class="title">Buku Piutang</span>
                <span class="{{ $creditList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $tunggakanClass or '' }}">
            <a href="{{ route('admin.overdues.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-pencil"></i>
                <span class="title">Tunggakan</span>
                <span class="{{ $tunggakanList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $settingClass or '' }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-gear"></i>
                <span class="title">Lain-lain</span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item {{ $deliveryClass or '' }}">
                    <a href="{{ route('admin.shipping-methods.index') }}" class="nav-link nav-toggle">
                        <i class="fa fa-send"></i>
                        <span class="title">Pengiriman</span>
                        <span class="{{ $deliveryList or ''}}"></span>
                    </a>
                </li>
                <li class="nav-item {{ $newsClass or '' }}">
                    <a href="{{ route('admin.news.index') }}" class="nav-link nav-toggle">
                        <i class="fa fa-info"></i>
                        <span class="title">Berita & Acara</span>
                        <span class="{{ $newsList or ''}}"></span>
                    </a>
                </li>
                <li class="nav-item {{ $adminTermClass or '' }}">
                    <a href="{{ route('admin.terms-and-condition.index') }}" class="nav-link nav-toggle">
                        <i class="fa fa-puzzle-piece"></i>
                        <span class="title">Syarat & Ketentuan</span>
                        <span class="{{ $adminTermList or ''}}"></span>
                    </a>
                </li>
                <li class="nav-item {{ $adminFeedbackClass or '' }}">
                    <a href="{{ route('admin.feedbacks.index') }}" class="nav-link nav-toggle">
                        <i class="fa fa-bullhorn"></i>
                        <span class="title">Komplain</span>
                        <span class="{{ $adminFeedbackList or ''}}"></span>
                    </a>
                </li>
                <li class="nav-item {{ $adminAboutUsClass or '' }}">
                    <a href="{{ route('admin.about-us.index') }}" class="nav-link nav-toggle">
                        <i class="fa fa-building-o"></i>
                        <span class="title">Tentang Kami</span>
                        <span class="{{ $adminAboutUsList or ''}}"></span>
                    </a>
                </li>
                <li class="nav-item {{ $adminHowToTransactionClass or '' }}">
                    <a href="{{ route('admin.how-to-transaction.index') }}" class="nav-link nav-toggle">
                        <i class="fa fa-credit-card"></i>
                        <span class="title">Cara Bertransaksi</span>
                        <span class="{{ $adminHowToTransactionList or ''}}"></span>
                    </a>
                </li>
                <li class="nav-item {{ $adminContactUsClass or '' }}">
                    <a href="{{ route('admin.contact-us.index') }}" class="nav-link nav-toggle">
                        <i class="fa fa-envelope"></i>
                        <span class="title">Daftar Pesan</span>
                        <span class="{{ $adminContactUsList or ''}}"></span>
                    </a>
                </li>
            </ul>
        </li>
        @endif

        @if(Auth::user()->role == UserRole::VENDOR)
        <!-- Initiate Sidebar for Management Vendor -->
        <li class="nav-item {{ $vendorClass or ''}}">
            <a href="{{ route('vendor.dashboards.index') }}" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title">Beranda</span>
                <span class="{{ $vendorList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $outletClass or '' }}">
            <a href="{{ route('vendor.outlets.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-address-book-o"></i>
                <span class="title">Profil Outlet</span>
                <span class="{{ $outletList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $productClass or '' }}">
            <a href="{{ route('vendor.products.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-database"></i>
                <span class="title">Detail Produk</span>
                <span class="{{ $productList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $termClass or '' }}">
            <a href="{{ route('vendor.terms-and-condition.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-puzzle-piece"></i>
                <span class="title">Syarat & Ketentuan</span>
                <span class="{{ $termList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $feedbackClass or '' }}">
            <a href="{{ route('vendor.feedbacks.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-picture-o"></i>
                <span class="title">Komplain</span>
                <span class="{{ $feedbackList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $deliverylogClass or '' }}">
            <a href="{{ route('vendor.deliverylogs.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-folder-open-o"></i>
                <span class="title">Status Pengiriman</span>
                <span class="{{ $deliverylogList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $aboutUsClass or '' }}">
            <a href="{{ route('vendor.about-us.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-picture-o"></i>
                <span class="title">Tentang Kami</span>
                <span class="{{ $aboutUsList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $howToTransactionClass or '' }}">
            <a href="{{ route('vendor.how-to-transaction.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-credit-card"></i>
                <span class="title">Cara Bertransaksi</span>
                <span class="{{ $howToTransactionList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $contactUsClass or '' }}">
            <a href="{{ route('vendor.contact-us.index') }}" class="nav-link nav-toggle">
                <i class="fa fa-envelope"></i>
                <span class="title">Daftar Pesan</span>
                <span class="{{ $contactUsList or ''}}"></span>
            </a>
        </li>
        <li class="nav-item {{ $billingClass or '' }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-dollar"></i>
                <span class="title">Tagihan</span>
                <span class="arrow open"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item {{ $landClass or '' }}">
                    <a href="{{ route('vendor.land.index') }}" class="nav-link ">
                        <i class="fa fa-building-o"></i>
                        <span class="title">Sewa Lahan</span>
                        <span class="{{ $landList or ''}}"></span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li class="nav-item {{ $electricityClass or '' }}">
                    <a href="{{ route('vendor.electricity.index') }}" class="nav-link ">
                        <i class="fa fa-flash"></i>
                        <span class="title">Biaya Listrik</span>
                        <span class="{{ $electricityList or ''}}"></span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li class="nav-item {{ $waterExpenseClass or '' }}">
                    <a href="{{ route('vendor.water-expense.index') }}" class="nav-link ">
                        <i class="fa fa-tint"></i>
                        <span class="title">Biaya Air</span>
                        <span class="{{ $waterExpenseList or ''}}"></span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li class="nav-item {{ $guardClass or '' }}">
                    <a href="{{ route('vendor.guards.index') }}" class="nav-link ">
                        <i class="fa fa-hand-stop-o"></i>
                        <span class="title">Biaya Satpam</span>
                        <span class="{{ $guardList or ''}}"></span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li class="nav-item {{ $otherClass or '' }}">
                    <a href="{{ route('vendor.others.index') }}" class="nav-link ">
                        <i class="fa fa-reorder"></i>
                        <span class="title">Biaya Lain-lain</span>
                        <span class="{{ $lainList or ''}}"></span>
                        <span class="selected"></span>
                    </a>
                </li>
            </ul>
        </li>
        <!-- End Sidebar for Management Vendor -->
        @endif
    </ul>
</div>
