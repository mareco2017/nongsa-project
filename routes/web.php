<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Without namespace but middleware
Route::group(['middleware' => 'auth'], function(){
	Route::get('/login/{m01_user}', 'VerificationController@showVerifyForm')->name('user.verify.login.form');
	Route::post('/verify/login/{m01_user}', 'VerificationController@verifyGoogle2F')->name('user.verify.login.google2f');
	Route::get('/verify/email/{m01_user}', 'VerificationController@showVerifyEmail')->name('user.verify.login.email');
	Route::post('/verify/email/{m01_user}', 'VerificationController@verify')->name('user.verify.login.email.code');
	
});
//With namespace Frontend
Route::group(['namespace' => 'Frontend'], function(){
	Route::get('/', 'HomepageController@index')->name('user.homepages.index');
	Route::get('/home', 'HomepageController@showIndex')->name('user.homepages.showIndex');
	Route::get('/outlet-details/{m02_outlet}', 'HomepageController@outletDetail')->name('outlet.homepages.detail');
	Route::post('/contact-us/{m02_outlet}', 'HomepageController@saveMessageToOutlet')->name('user.contact-us.save.detail');
	Route::post('/feedback/{m02_outlet}', 'HomepageController@saveFeedbackToOutlet')->name('user.feedback.save.detail');
	Route::get('/news-and-events','HomepageController@newsAndEvents')->name('user.news.index');
	Route::get('/news-and-events/detail/{m03_news}','HomepageController@newsAndEventsDetail')->name('user.news.detail');
	Route::get('/terms-and-conditions', 'HomepageController@terms')->name('user.terms.index');
	Route::get('/about-us', 'HomepageController@abouts')->name('user.abouts.index');
	Route::get('/how-to-transaction', 'HomepageController@howTo')->name('user.how-to-transaction.index');
	Route::get('/delivery', 'HomepageController@delivery')->name('user.delivery.index');
	Route::post('/delivery', 'HomepageController@checkDelivery')->name('user.delivery.check');
	Route::get('/feedback', 'HomepageController@feedback')->name('user.feedback.index');
	Route::post('/feedback/save', 'HomepageController@saveFeedback')->name('user.feedback.save');
	Route::get('/contact-us', 'HomepageController@contactUs')->name('user.contact-us.index');
	Route::post('/contact-us', 'HomepageController@saveMessage')->name('user.contact-us.save');

	//Product
	Route::get('/product/kategori/{category_slug_url}', 'ProductController@filterProductByCategories')->name('user.product.category');
	Route::get('/product/kategori/{category_slug_url}/{subcategory_slug_url}', 'ProductController@filterProductBySubcategories')->name('user.product.subcategory');
	Route::get('/product/{m02_product_slug_url}', 'HomepageController@showProductDetail')->name('user.product.detail');
	Route::post('/product/search/', 'HomepageController@searchProduct')->name('search.product');
	Route::get('/search/product/', 'HomepageController@searchIndex')->name('search.product.view');
	Route::post('/product/{m02_product_id}/review', 'ProductController@addReview')->name('user.product.review');
	Route::post('/product/review/{m01_review_id}/update', 'ProductController@updateReview')->name('user.product.review.update');
	Route::delete('/product/review/{m01_review_id}/delete', 'ProductController@deleteReview')->name('user.product.review.delete');
	Route::post('/product/add/cart', 'ProductController@addToCart')->name('user.product.cart');
	Route::get('/product/buy/{m02_product_slug_url}', 'HomepageController@buyProduct')->name('user.product.buy');

	//Cart
	Route::get('/cart', 'HomepageController@cart')->name('user.cart.index');
	Route::post('/cart/update', 'ProductController@updateCart')->name('user.cart.update');
	Route::delete('/cart/{m01_cart}/delete', 'ProductController@deleteCart')->name('user.cart.delete');
	Route::get('/cart/count', 'ProductController@getCartCount')->name('user.cart.count');
	Route::get('/cart/checkout', 'HomepageController@checkout')->name('user.cart.checkout');
	Route::post('/cart/checkout', 'HomepageController@submitCheckout')->name('user.cart.checkout.save');

	//Order
	Route::get('/order', 'HomepageController@order')->name('user.order.index');
	Route::get('/order/{m01_order_id}', 'HomepageController@orderDetail')->name('user.order.detail');
	Route::get('/order/{m01_order_id}/payment', 'HomepageController@payment')->name('user.order.payment.index');
	Route::post('/order/{m01_order_id}/payment', 'HomepageController@submitPayment')->name('user.order.payment.save');
	Route::post('/shipping-cost', 'HomepageController@getShippingCost')->name('user.shipping.price');

	//Login
	Route::get('/user/login','LoginController@showLoginForm')->name('user.login.form');
	Route::post('/user/login','LoginController@login')->name('user.login');
	Route::get('/feedback', 'HomepageController@feedback')->name('user.feedback.index');
	Route::post('/feedback/save', 'HomepageController@saveFeedback')->name('user.feedback.save');
	//Reset PW
	//Register
	Route::get('/user/register/email', 'RegisterController@showVerifyEmail')->name('user.verify.register.with.email');
	Route::get('/user/register','RegisterController@showRegistrationForm')->name('user.register.form');
	Route::post('/user/register','RegisterController@register')->name('user.register');
	Route::get('/user/register/{m01_user}', 'RegisterController@showVerifyForm')->name('user.verify.register.form');
	Route::post('/verify/register/{m01_user}', 'RegisterController@verifyGoogle2F')->name('user.verify.register.google2f');
	Route::post('/register/verify/email', 'RegisterController@verifyEmail')->name('user.verify.register.email.code');

	Route::group(['middleware' => 'auth'], function(){
		Route::get('/logout', 'LoginController@logout')->name('user.logout');
		Route::get('/profile', 'ChangePassword@edit')->name('user.change.profile');
		Route::post('/profile', 'ChangePassword@update')->name('user.password.save');
	});
});

//With namespace Auth
Route::group(['namespace' => 'Auth'], function() {
	Route::get('/user/reset/password','ForgotPasswordController@frontendIndex')->name('user.password.request.index');
	Route::get('/admin/register', 'RegisterController@showRegisterForm')->name('admin.register.form');
	Route::post('/admin/register', 'RegisterController@register')->name('admin.register');
	Route::get('/admin/register/{m01_user}', 'RegisterController@showVerifyForm')->name('admin.verify.register.form');
	Route::post('/admin/verify/register/{m01_user}', 'RegisterController@verifyGoogle2F')->name('admin.verify.register.google2f');
	Route::get('/admin/email', 'RegisterController@showVerifyEmail')->name('admin.verify.register.email');
	Route::post('/admin/email/register', 'RegisterController@verify')->name('admin.verify.register.email.code');
	Route::get('/admin/reset/email/{m01_user}', 'RegisterController@showResetEmail')->name('admin.verify.reset.email');
	Route::post('/admin/reset/email/verify/{m01_user}', 'RegisterController@verifyReset')->name('admin.verify.reset.email.code');


	Route::get('/admin/login', 'LoginController@showLoginForm')->name('admin.login.form');
	Route::post('/admin/login', 'LoginController@login')->name('admin.login');
	Route::get('/admin/password/reset', 'ForgotPasswordController@index')->name('admin.password.request.index');
	Route::get('/admin/logout', 'LoginController@logout')->name('admin.logout');

	Route::get('/admin/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.password.reset');
	Route::post('/admin/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('/admin/reset/google/{m01_user}', 'RegisterController@showVerifyResetForm')->name('admin.verify.password.google2f.form');
	Route::post('/admin/verify/reset/google/{m01_user}', 'RegisterController@verifyResetGoogle2F')->name('admin.verify.password.google2f');

	Route::post('/admin/password/reset', 'ResetPasswordController@reset');

	Route::get('/user/password/reset/{token}', 'ResetPasswordController@frontendResetForm')->name('user.password.reset');
	Route::get('/user/reset/google/{m01_user}', 'RegisterController@showVerifyResetForm')->name('user.verify.password.google2f.form');
	Route::post('/user/verify/reset/google/{m01_user}', 'RegisterController@verifyResetGoogle2F')->name('user.verify.password.google2f');
	Route::get('/user/reset/email/{m01_user}', 'RegisterController@showResetEmail')->name('user.verify.reset.email');
	Route::post('/user/reset/email/verify/{m01_user}', 'RegisterController@verifyReset')->name('user.verify.reset.email.code');


});
Route::group(['middleware' => ['auth']], function(){
	Route::get('/admin/login/{m01_user}', 'VerificationController@showVerifyForm')->name('admin.verify.login.form');
	Route::post('/admin/verify/login/{m01_user}', 'VerificationController@verifyGoogle2F')->name('admin.verify.login.google2f');
	Route::get('/admin/login/email/{m01_user}', 'VerificationController@showVerifyEmail')->name('admin.verify.login.email');
	Route::post('/admin/login/verify/email/{m01_user}', 'VerificationController@verify')->name('admin.verify.login.email.code');


});

//With Auth and namespace Backend
Route::group(['middleware' => ['auth','admin']], function(){

	Route::group(['prefix' => 'admin','namespace' => 'Backend'], function(){
		Route::get('/','HomeController@index')->name('admin.dashboards.index');

		Route::group(['prefix' => 'password'], function() {
			Route::get('/change', 'ChangePassword@edit')->name('admin.change.password');
			Route::post('/save', 'ChangePassword@update')->name('admin.password.save');
		});
		
		Route::group(['prefix' => 'categories'], function(){
			Route::get('/', 'OutletCategoryController@index')->name('admin.categories.index');
			Route::get('/new', 'OutletCategoryController@newForm')->name('admin.categories.new');
			Route::post('/save', 'OutletCategoryController@store')->name('admin.categories.save');
			Route::get('/edit/{m03_outlet_category}', 'OutletCategoryController@edit')->name('admin.categories.edit');
			Route::put('/save/{m03_outlet_category}', 'OutletCategoryController@update')->name('admin.categories.update');
			Route::delete('/delete/{m03_outlet_category}', 'OutletCategoryController@destroy')->name('admin.categories.destroy');
			Route::post('/list', 'OutletCategoryController@listAllOutletCategory')->name('admin.categories.list');
		});

		Route::group(['prefix' => 'product-categories'], function(){
			Route::get('/', 'ProductCategoryController@index')->name('admin.product-categories.index');
			Route::get('/new', 'ProductCategoryController@newForm')->name('admin.product-categories.new');
			Route::post('/save', 'ProductCategoryController@store')->name('admin.product-categories.save');
			Route::get('/edit/{id}', 'ProductCategoryController@edit')->name('admin.product-categories.edit');
			Route::put('/save/{id}', 'ProductCategoryController@update')->name('admin.product-categories.update');
			Route::delete('/delete/{id}', 'ProductCategoryController@destroy')->name('admin.product-categories.destroy');
			Route::post('/list', 'ProductCategoryController@listAllProductCategory')->name('admin.product-categories.list');
		});

		Route::group(['prefix' => 'product-subcategories'], function(){
			Route::get('/', 'ProductSubcategoryController@index')->name('admin.product-subcategories.index');
			Route::get('/new', 'ProductSubcategoryController@newForm')->name('admin.product-subcategories.new');
			Route::post('/save', 'ProductSubcategoryController@store')->name('admin.product-subcategories.save');
			Route::get('/edit/{id}', 'ProductSubcategoryController@edit')->name('admin.product-subcategories.edit');
			Route::put('/save/{id}', 'ProductSubcategoryController@update')->name('admin.product-subcategories.update');
			Route::delete('/delete/{id}', 'ProductSubcategoryController@destroy')->name('admin.product-subcategories.destroy');
			Route::post('/list', 'ProductSubcategoryController@listAllProductSubcategory')->name('admin.product-subcategories.list');
		});


		Route::group(['prefix' => 'outlets'], function(){
			Route::get('/', 'OutletController@index')->name('admin.outlets.index');
			Route::get('/new', 'OutletController@newForm')->name('admin.outlets.new');
			Route::post('/save', 'OutletController@store')->name('admin.outlets.save');
			Route::get('/edit/{m02_outlet}', 'OutletController@edit')->name('admin.outlets.edit');
			Route::put('/save/{m02_outlet}', 'OutletController@update')->name('admin.outlets.update');
			Route::delete('/delete/{m02_outlet}', 'OutletController@destroy')->name('admin.outlets.destroy');
			Route::post('/list', 'OutletController@listAllOutlet')->name('admin.outlets.list');
		});

		Route::group(['prefix' => 'banners'], function(){
			Route::get('/', 'BannerController@index')->name('admin.banners.index');
			Route::post('/save', 'BannerController@store')->name('admin.banners.save');
			Route::get('/set/{m02_banner}', 'BannerController@setFirstBanner')->name('admin.banners.first');
			Route::get('/delete/{m02_banner}', 'BannerController@destroy')->name('admin.banners.destroy');
			Route::post('/list', 'BannerController@listAllBanner')->name('admin.banners.list');
			Route::post('/store-banner', 'BannerController@storeBanner')->name('admin.storebanners.save');
			Route::post('/product-banner', 'BannerController@productBanner')->name('admin.productbanners.save');
		});

		Route::group(['prefix' => 'invoices'], function(){
			Route::get('/', 'InvoiceController@index')->name('admin.invoices.index');
			Route::get('/new', 'InvoiceController@newForm')->name('admin.invoices.new');
			Route::post('/save', 'InvoiceController@store')->name('admin.invoices.save');
			Route::get('/edit/{m02_invoice}', 'InvoiceController@edit')->name('admin.invoices.edit');
			Route::put('/save/{m02_invoice}', 'InvoiceController@update')->name('admin.invoices.update');
			Route::delete('/delete/{m02_invoice}', 'InvoiceController@destroy')->name('admin.invoices.destroy');
			Route::post('/list', 'InvoiceController@listAllInvoice')->name('admin.invoices.list');
			Route::post('/grand_total', 'InvoiceController@ajaxToogleGrandTotal')->name('admin.invoices.grandTotal');
		});

		Route::group(['prefix' => 'credits'], function(){
			Route::get('/', 'CreditController@index')->name('admin.credits.index');
			Route::get('/new', 'CreditController@newForm')->name('admin.credits.new');
			Route::post('/list', 'CreditController@listAllCredit')->name('admin.credits.list');
		});

		Route::group(['prefix' => 'overdues'], function(){
			Route::get('/', 'OverdueController@index')->name('admin.overdues.index');
			Route::get('/new', 'OverdueController@newForm')->name('admin.overdues.new');
			Route::post('/list', 'OverdueController@listAllOverdue')->name('admin.overdues.list');
		});

		Route::group(['prefix' => 'news'], function(){
			Route::get('/', 'NewController@index')->name('admin.news.index');
			Route::get('/new', 'NewController@newForm')->name('admin.news.new');
			Route::post('/save', 'NewController@store')->name('admin.news.save');
			Route::get('/edit/{m03_news}', 'NewController@edit')->name('admin.news.edit');
			Route::put('/save/{m03_news}', 'NewController@update')->name('admin.news.update');
			Route::delete('/delete/{m03_news}', 'NewController@destroy')->name('admin.news.destroy');
			Route::post('/list', 'NewController@listAllNew')->name('admin.news.list');
		});
		
		Route::group(['prefix' => 'terms-and-conditions'], function(){
			Route::get('/', 'TermsAndConditionController@index')->name('admin.terms-and-condition.index');
			Route::post('/save', 'TermsAndConditionController@store')->name('admin.terms-and-condition.save');
		});

		Route::group(['prefix' => 'feedbacks'], function(){
			Route::get('/', 'FeedbackController@index')->name('admin.feedbacks.index');
			Route::post('/send', 'FeedbackController@sendFeedback')->name('admin.send.feedback.save');
		});

		Route::group(['prefix' => 'about-us'], function(){
			Route::get('/', 'AboutUsController@index')->name('admin.about-us.index');
			Route::post('/save', 'AboutUsController@store')->name('admin.about-us.save');
		});

		Route::group(['prefix' => 'how-to-transaction'], function(){
			Route::get('/', 'HowToTransactionController@index')->name('admin.how-to-transaction.index');
			Route::post('/save', 'HowToTransactionController@store')->name('admin.how-to-transaction.save');
		});

		Route::group(['prefix' => 'contact-us'], function(){
			Route::get('/', 'ContactUsController@index')->name('admin.contact-us.index');
			Route::post('/send', 'ContactUsController@sendMessage')->name('admin.send.contact-us.save');
		});
		Route::group(['prefix' => 'shipping-methods'], function(){
			Route::get('/', 'ShippingMethodController@index')->name('admin.shipping-methods.index');
			Route::get('/new', 'ShippingMethodController@newForm')->name('admin.shipping-methods.new');
			Route::post('/save', 'ShippingMethodController@store')->name('admin.shipping-methods.save');
			Route::get('/edit/{m03_shipping_method}', 'ShippingMethodController@edit')->name('admin.shipping-methods.edit');
			Route::put('/save/{m03_shipping_method}', 'ShippingMethodController@update')->name('admin.shipping-methods.update');
			Route::delete('/delete/{m03_shipping_method}', 'ShippingMethodController@destroy')->name('admin.shipping-methods.destroy');
			Route::post('/list', 'ShippingMethodController@listAllShippingMethod')->name('admin.shipping-methods.list');
		});
		
	});
});

Route::group(['middleware' => ['auth','vendor']], function(){
	Route::group(['prefix' => 'vendor','namespace' => 'Frontend'], function(){
		Route::get('/','HomeController@index')->name('vendor.dashboards.index');

		Route::group(['prefix' => 'password'], function() {
			Route::get('/change', 'ChangePassword@edit')->name('vendor.change.password');
			Route::post('/save', 'ChangePassword@update')->name('vendor.password.save');
		});

		Route::group(['prefix' => 'terms-and-conditions'], function(){
			Route::get('/', 'TermsAndConditionController@index')->name('vendor.terms-and-condition.index');
			Route::post('/save', 'TermsAndConditionController@store')->name('vendor.terms-and-condition.save');
		});

		Route::group(['prefix' => 'feedbacks'], function(){
			Route::get('/', 'FeedbackController@index')->name('vendor.feedbacks.index');
			Route::post('/send', 'FeedbackController@sendFeedback')->name('vendor.send.feedback.save');
		});

		Route::group(['prefix' => 'products'], function(){
			Route::get('/', 'ProductController@index')->name('vendor.products.index');
			Route::get('/new', 'ProductController@newForm')->name('vendor.products.new');
			Route::post('/list', 'ProductController@listAllProduct')->name('vendor.products.list');
			Route::post('/save', 'ProductController@store')->name('vendor.products.save');
			Route::get('/edit/{m02_product}', 'ProductController@edit')->name('vendor.products.edit');
			Route::put('/save/{m02_product}', 'ProductController@update')->name('vendor.products.update');		
			Route::delete('/delete/{m02_product}', 'ProductController@destroy')->name('vendor.products.destroy');
			Route::post('/inventory/save', 'ProductController@storeInventory')->name('vendor.inventories.save');
		});

		Route::group(['prefix' => 'subcategories'], function(){
			Route::get('/subcategories', 'ProductController@getSubcategoriesByCategory')->name('vendor.subcategories.filter');
		});

		Route::group(['prefix' => 'outlets'], function(){
			Route::get('/', 'OutletController@index')->name('vendor.outlets.index');
			Route::put('/save/{m02_outlet}', 'OutletController@update')->name('vendor.outlets.update');
		});

		Route::group(['prefix' => 'billings'], function(){
			Route::group(['prefix' => 'land'], function(){
				Route::get('/', 'BillingController@indexLand')->name('vendor.land.index');
				Route::post('/list', 'BillingController@listAllLand')->name('vendor.land.list');
				Route::get('/pick/{m03_credit}', 'BillingController@specifyLand')->name('vendor.pick.land');
				Route::post('/save', 'BillingController@storeLand')->name('vendor.land.save');
			});

			Route::group(['prefix' => 'electricity'], function(){
				Route::get('/', 'BillingController@indexElectricity')->name('vendor.electricity.index');
				Route::post('/list', 'BillingController@listAllElectricity')->name('vendor.electricity.list');
				Route::get('/pick/{m03_credit}', 'BillingController@specifyElectricity')->name('vendor.pick.electricity');
				Route::post('/save', 'BillingController@storeElectricity')->name('vendor.electricity.save');
			});

			Route::group(['prefix' => 'water-expense'], function(){
				Route::get('/', 'BillingController@indexWaterExpense')->name('vendor.water-expense.index');
				Route::post('/list', 'BillingController@listAllWaterExpense')->name('vendor.water-expense.list');
				Route::get('/pick/{m03_credit}', 'BillingController@specifyWaterExpense')->name('vendor.pick.water-expense');
				Route::post('/save', 'BillingController@storeWaterExpense')->name('vendor.water-expense.save');
			});

			Route::group(['prefix' => 'guards'], function(){
				Route::get('/', 'BillingController@indexGuard')->name('vendor.guards.index');
				Route::post('/list', 'BillingController@listAllGuard')->name('vendor.guards.list');
				Route::get('/pick/{m03_credit}', 'BillingController@specifyGuard')->name('vendor.pick.guards');
				Route::post('/save', 'BillingController@storeGuard')->name('vendor.guards.save');
			});

			Route::group(['prefix' => 'others'], function(){
				Route::get('/', 'BillingController@indexOther')->name('vendor.others.index');
				Route::post('/list', 'BillingController@listAllOther')->name('vendor.others.list');
				Route::get('/pick/{m03_credit}', 'BillingController@specifyOther')->name('vendor.pick.others');
				Route::post('/save', 'BillingController@storeOther')->name('vendor.others.save');
			});
		});

		Route::group(['prefix' => 'delivery-logs'], function(){
			Route::get('/', 'DeliveryLogController@index')->name('vendor.deliverylogs.index');
			Route::post('/list', 'DeliveryLogController@listAllInvoiceDeliveryLog')->name('vendor.deliverylogs.list');
			Route::post('/save', 'DeliveryLogController@store')->name('vendor.deliverylogs.save');
			Route::get('/close/{m02_vendor_sale_id}', 'DeliveryLogController@close')->name('vendor.deliverylogs.close');
			Route::get('/edit/{m02_vendor_sale_id}', 'DeliveryLogController@listSelectedDeliveryLog')->name('vendor.deliverylogs.edit');
			Route::post('/edit/resi/{m02_vendor_sale_id}', 'DeliveryLogController@inputResi')->name('vendor.deliverylogs.inputResi');
		});

		Route::group(['prefix' => 'about-us'], function(){
			Route::get('/', 'AboutUsController@index')->name('vendor.about-us.index');
			Route::post('/save', 'AboutUsController@store')->name('vendor.about-us.save');
		});

		Route::group(['prefix' => 'how-to-transaction'], function(){
			Route::get('/', 'HowToTransactionController@index')->name('vendor.how-to-transaction.index');
			Route::post('/save', 'HowToTransactionController@store')->name('vendor.how-to-transaction.save');
		});

		Route::group(['prefix' => 'contact-us'], function(){
			Route::get('/', 'ContactUsController@index')->name('vendor.contact-us.index');
			Route::post('/send', 'ContactUsController@sendMessage')->name('vendor.send.contact-us.save');
		});
	});
});
Auth::routes();
