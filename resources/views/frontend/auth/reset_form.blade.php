@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
    <!-- BEGIN container -->
    <div class="container">
        <!-- BEGIN section-title -->
        <h1 class="section-title clearfix">
            Reset Password<br>
            <small>You're about to reset password</small>
        </h1>
        <br>
        <!-- END section-title -->
        <!-- BEGIN row -->
        <div class="row row-space-10">
            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                @include('includes.session_message')
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="form-group m-b-15 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="login-buttons form-group">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Next</button>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </form>
        </div>
        <!-- END row -->
    </div>
    <!-- END container -->
</div>
<!-- END #promotions -->
@endsection