<?php

namespace App\Helpers\Enums;

final class BillCategory {

	const SEWA_LAHAN = 0;
	const LISTRIK = 1;
	const AIR = 2;
	const SATPAM = 3;
	const LAIN = 4;

	public static function getList() {
		return [
			BillCategory::SEWA_LAHAN,
			BillCategory::LISTRIK,
			BillCategory::AIR,
			BillCategory::SATPAM,
			BillCategory::LAIN,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
			return "Sewa Lahan";
			case 1:
			return "Biaya Listrik";
			case 2:
			return "Biaya Air";
			case 3:
			return "Biaya Satpam";
			case 4:
			return "Biaya Lain-lain";
		}
	}

}

?>
