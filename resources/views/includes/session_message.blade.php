<div id="session-message">
    @include('flash::message')
</div>
@if (count($errors))
<span class="help-block alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <strong><li>{{ $error }}</li></strong>
        @endforeach
    </ul>
</span>
@endif