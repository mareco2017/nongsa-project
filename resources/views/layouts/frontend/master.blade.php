<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" style="height:100%">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title>Nongsa Frontend</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="{{ asset('assets/css/fonts.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/frontend/style.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/frontend/style-responsive.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/frontend/default.css') }}" id="theme" rel="stylesheet" />
    <link href="{{ asset('assets/css/frontend/animate.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/js/bootstrap-datepicker.min.css') }}" />
    
    <!-- ================== END BASE CSS STYLE ================== -->
    
    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{ asset('assets/plugins/pace/pace.min.js') }}"></script>
    <!-- ================== END BASE JS ================== -->
</head>
<body>
    <!-- BEGIN #page-container -->
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        @include('layouts.frontend.header')

        <div id="content" class="content">
            @yield('content')
        </div>
        @include('layouts.frontend.footer')
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
        
    </div>
    <script src="{{ asset('assets/frontend/jquery/jquery-1.9.1.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/jquery/jquery-migrate-1.1.0.min.js') }}"></script>
    <script src="{{ asset('assets/frontend/bootstrap/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/frontend/jquery-cookie/jquery.cookie.js') }}"></script>
    <script src="{{ asset('assets/frontend/apps.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/gritter/js/jquery.gritter.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-daterangepicker/moment.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>

    <script>
        $(document).ready(function() {
            App.init();

            $('.dateonlypicker').datetimepicker({ format: 'YYYY-MM-DD'});
            $('.datetimepicker').datetimepicker({ format: 'YYYY-MM-DD H:mm'});

            $('#session-message').fadeIn(4000, function () {
                $('#session-message').fadeOut(4000);
            });
        });
    </script>
    @stack('pageRelatedJs')

</body>
</html>
