@extends('layouts.frontend.master')
@section('content')
<?php

use App\Helpers\Enums\DeliveryType;
use App\Helpers\Enums\OrderStatus;

?>
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container">
        <!-- BEGIN section-title -->
        @include('includes.session_message')
        <h1 class="section-title clearfix">CHECKOUT<br>
        </h1>
        <br>
        <!-- END section-title -->
        <!-- BEGIN row -->
        <form class="form-horizontal" method="POST" action="{{ route('user.cart.checkout.save') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="resp-label">Product Detail</label>
                    </div>
                    <div class="category-container">
                        <div class="table-responsive">
                            <table class="table table-cart">
                                <thead>
                                    <tr>
                                        <th>Product Name</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($carts as $cart)
                                    <tr>
                                        <td class="cart-product">
                                            <div class="medium-size-text">{{ $cart->m02_product->name }}</div>
                                        </td>
                                        <td class="cart-qty text-center medium-size-text">
                                            {{ $cart->qty }}
                                        </td>
                                        <td class="cart-total text-center medium-size-text" >
                                            {{ round($cart->subtotal,0) }}
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td class="cart-summary" colspan="4">
                                            <div class="summary-container">
                                                <div class="summary-row total">
                                                    <div class="field">
                                                        Ongkos Kirim
                                                    </div>
                                                    <div class="value" id="cost" name="amount">
                                                        0
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="summary-row total">
                                                    <div class="field size-18">
                                                        Total
                                                    </div>
                                                    <div class="value size-18" id="grand-total">
                                                        {{ $totalPrice }}
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5">
                    <div class="form-group m-b-15">
                        <label class="resp-label">Delivery Address</label>
                    </div>
                    <div class="form-group m-b-15">
                        <textarea type="text" id="address" rows="5" class="form-control resp-text" name="address" placeholder="Enter address" required></textarea>
                    </div>
                    <div class="form-group m-b-15">
                        <br>
                        <label class="resp-label">Delivery Method</label> 
                    </div>
                    <div class="form-group m-b-15">
                        {{ Form::select('m03_shipping_method_id', $shippingMethods, null,array('class' => 'form-control col-md-6 resp-text', 'placeholder' => 'Select Delivery Method...','data-parsley-required' => 'true', 'onchange' => 'updatePrice()', 'id' => 'shipping_method' ))}}
                    </div>
                    <br>
                    <div class="form-group m-b-15">
                        <button type="submit" class="btn btn-info btn-outline float-right mar-right">Checkout</button>
                    </div>
                </div>
            </div>
            <!-- END row -->
        </form>
    </div>
    <!-- END container -->
</div>

@endsection

@push('pageRelatedJs')
<script type="text/javascript">
    function updateTotalPrice($cart, $qty) {
        $currentTotal = parseInt($('#total-price').text());
        $currentSubtotal = parseInt($('#total' + $cart.id).text());
        $totalFieldId = "total" + $cart.id;
        $newSubtotal = $cart.m02_product.retail_price * $qty;
        $('#'+$totalFieldId).html($newSubtotal);
        $newTotal = $currentTotal - $currentSubtotal + $newSubtotal;
        $('#total-price').text($newTotal);
        updateCart($cart, $qty);
    }

    function getPrice($shippingMethodId) {
        $.ajax(
        {
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url:  "{{ route('user.shipping.price') }}",
            data: { 'id' : $shippingMethodId},
            type: 'POST',
            success: function(result) {
                console.log(result.price);
                if (result) {
                    $('#cost').text(result.price);
                    $newTotal = result.totalPrice + result.price;
                    $('#grand-total').text($newTotal);
                }
            }
        });
    }

    function updatePrice() {
        if ($('#shipping_method').val() != "") {
            getPrice($('#shipping_method').val());
        }
        else {
            $currentTotal = parseInt($('#grand-total').text());
            $currentCost = parseInt($('#cost').text());
            $newTotal = $currentTotal - $currentCost;
            $('#grand-total').text($newTotal);
            $('#cost').text('0');
        }
    }

</script>
@endpush