@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
@include('includes.modal_input')
<?php

use App\Helpers\Enums\OrderStatus;

?>
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="form-group">
			<h1>Status Pengiriman</h1>
			@include('includes.session_message')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4>Daftar Status Pengiriman</h4>
						</div>
						<div class="panel-body">
							<table class="table table-striped table-hover fullTable" id="deliverylog-list" width="100%">
								<thead>
									<tr>
										<th>No.</th>
										<th>Tanggal</th>
										<th>No. Invoice</th>
										<th>Status Invoice</th>
										<th>Resi</th>
										<th>Status Pengiriman</th>
										<th>Action</th>
									</tr>
									<tr id="filter-row">
										<th></th>
										<th></th>
										<th class="tr-align-center">No. Invoice</th>
										<th class="tr-align-center">Status Invoice</th>
										<th class="tr-align-center">Resi</th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>
							@if ($deliveryLogs)
							<div class="panel panel-inverse">
								<div class="panel-heading">
									<h5>Invoice No : {{ $selectedVendorSale->m01_order->invoice_no }}</h5>
									<h5>Resi No : {{ $selectedVendorSale->resi_no }}</h5>
								</div>
							</div>
							<table class="table table-striped table-hover fullTable" id="selected-deliverylog-list" width="100%">
								<thead>
									<tr>
										<th>Tanggal</th>
										<th>Posisi Paket</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($deliveryLogs as $key=>$deliveryLog)
									<tr>
										<td>
											{{ $deliveryLog->delivery_date }}
										</td>
										<td>
											{{ $deliveryLog->location }}
										</td>
										<td>
											{{ $deliveryLog->delivery_status }}
										</td>
									</tr>
									@endforeach
									@if ($selectedVendorSale->delivery_status != "Telah diterima Customer")
									{!! Form::open(['url' => route('vendor.deliverylogs.save'),'method' => 'POST', 'class' => 'form-group']) !!}
									{!! Form::hidden('m02_vendor_sale_id', $selectedVendorSale->id) !!}
									<tr>
										<td>
											{{ Form::text('delivery_date',null, array('class' => 'datetimepicker form-control', 'placeholder' => 'Tanggal', 'id' => 'datetimepicker'))}}
										</td>
										<td>
											{{ Form::text('location',null, array('class' => 'form-control', 'placeholder' => 'Posisi Paket'))}}
										</td>
										<td>
											{{ Form::text('delivery_status',null, array('class' => 'form-control', 'placeholder' => 'Status'))}}
										</td>
									</tr>
									@endif
								</tbody>
							</table>
							@if ($selectedVendorSale->delivery_status != "Telah diterima Customer")
							<button type="submit" class="btn green btn-lg">Update Info Pengiriman</button>
							{{ Form::close() }}
							<a href="{{ route('vendor.deliverylogs.close',$selectedVendorSale->id)}}" class="btn green btn-lg">Close Pengiriman</a>

							@endif    
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
	$(document).ready(function () {
		var deliverylogDataTable = $('#deliverylog-list').DataTable(
		{
			dom: "lrtip",
			orderCellsTop: true,
			responsive: true,
			serverSide: true,
			ajax: {
				url:  "{{ route('vendor.deliverylogs.list') }}",
				data: { '_token' : '{{ csrf_token() }}'},
				type: 'POST',
			},
			columns: [
			{
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{ data: 'm01_order.created_at', name: 'm01_order.created_at', 'className': 'text-center' },
			{ data: 'm01_order.invoice_no', name: 'm01_order.invoice_no', 'className': 'text-center' },
			{ data: 'm01_order.status', name: 'm01_order.status', 'className': 'text-center' },
			{ data: 'resi_no', name: 'resi', 'className': 'text-center' },
			{ data: 'delivery_status', name: 'delivery_status', 'className': 'text-center' },
			{ data: 'action', name: 'action', 'className': 'text-center', orderable: false, searchable: false  }
			]
		});

		$("#deliverylog-list thead tr#filter-row th").each(function (index) {
			var column = $(this).text();

			switch(column){
				case "Tanggal":
				case "No. Invoice":
				case "Status Invoice":
				case "Resi":
				case "Status Pengiriman":
				var input = '<input type="text" class="text-input form-control" width="100%" placeholder="Search By ' + column + '" />';

				$(this).html(input);
				break;
			}
		});

		$(".text-input").on("keyup change", function () { 
			deliverylogDataTable
			.column($(this).parent().index()) 
			.search(this.value) 
			.draw(); 
		});

		var dateInput = $("#datetimepicker");
		dateInput.parent().css("position", "relative");
		dateInput.datetimepicker();

	});

</script>
@endpush