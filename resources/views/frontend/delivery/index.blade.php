@extends('layouts.frontend.master')
@section('content')
<?php

use App\Helpers\Enums\DeliveryType;

?>
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN section-title -->
		@include('includes.session_message')
		<h1 class="section-title clearfix">CHECK YOUR DELIVERY STATUS<br>
		</h1>
		<br>
		<!-- END section-title -->
		<!-- BEGIN row -->
		<div class="row row-space-10">
            <form class="form-horizontal" method="POST" action="{{ route('user.delivery.check') }}">
                {{ csrf_field() }}
                <div class="col-md-4"></div>
                <div class="col-md-4">
                	<div class="form-group m-b-15">
                        <input id="resi_no" class="form-control" name="resi_no" placeholder="Resi Number" required autofocus>
                    </div>
                    <div class="login-buttons form-group">
                        <button type="submit" class="btn btn-success btn-block btn-lg">Check</button>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </form>

            <table class="table table-bordered table-striped table-hover fullTable">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                   <tr>
                       <td class="col-md-6">Resi Number</td>
                       <td class="col-md-6">
                            @if ($delivery)
                                {{ $delivery->resi_no }}
                           @endif
                       </td>
                   </tr>
                   <tr>
                       <td>Name</td>
                       <td>
                           @if ($user)
                                {{ $user->name}}
                           @endif
                       </td>
                   </tr>
                   <tr>
                       <td>Delivery Type</td>
                       <td>
                           @if ($shippingMethod)
                                {{ $shippingMethod }}
                           @endif
                       </td>
                   </tr>
                   <tr>
                       <td>Delivery Status</td>
                       <td>
                           @if ($delivery)
                                {{ $delivery->delivery_status}}
                           @endif
                       </td>
                   </tr>
                   <tr>
                       <td>Address</td>
                       <td>
                           @if ($delivery)
                                {{ $delivery->m01_order->address}}
                           @endif
                       </td>
                   </tr>
               </div>
               <div class="col-md-3"></div>
           </table>

       </div>
       <!-- END row -->
   </div>
   <!-- END container -->
</div>
<!-- END #promotions -->
@endsection