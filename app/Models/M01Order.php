<?php

namespace App\Models;

use App\Models\M01User;
use App\Models\M03ShippingMethod;
use Illuminate\Database\Eloquent\Model;

class M01Order extends Model
{
	protected $table = "m01_orders";

	protected $fillable = [
		'invoice_no','grand_total','expired_at', 'address',
		'm03_shipping_method_id','address','grand_total',
		'status'
	];

	protected $guarded = ['m01_user_id','created_at'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'invoice_no' => 'required|unique:m01_orders,invoice_no'.($id ? ','.$id : ''),
		];
		return $rules;
	}

	public function m01_user() 
	{
		return $this->belongsTo(M01User::class);
	}

	public function m03_shipping_method() 
	{
		return $this->belongsTo(M03ShippingMethod::class);
	}

}