@extends('layouts.backend.master')
@section('content')
<div class="page-content-wrapper">
	<div class="page-content">
		{!! Form::open(['url' => route('admin.about-us.save'),'method' => 'POST']) !!}
		<div class="form-group">
			<h1>Tentang Kami</h1>
			@include('includes.session_message')
			<div class="content-header-gray">
				<label>Content Editor</label>
			</div>
			@if ($aboutUs)
				<textarea class="content-text-area" name="content" placeholder="Enter any text here.." >{{ $aboutUs->content }}</textarea><br>
				Google Maps
				<input type="text" name="latlong" value="{{ $aboutUs->latlong }}" class="form-control" placeholder="Eg: BCS+Mall+(Batam+City+Square)" style="width: 70% !important">
			@else
				<textarea class="content-text-area" name="content" placeholder="Enter any text here.." ></textarea><br>
				Google Maps
				<input type="text" name="latlong" class="form-control" placeholder="Eg: Nongsa+Point+Marina+%26+Resort" style="width: 70% !important">
			@endif
		</div>
		<div class="form-group col-md-6">
			<button type="submit" class="btn green btn-lg">Simpan</button>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection