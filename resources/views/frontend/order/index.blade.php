@extends('layouts.frontend.master')
@section('content')
<?php

use App\Helpers\Enums\OrderStatus;

?>
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN section-title -->
		@include('includes.session_message')
		<h1 class="section-title clearfix">Order History<br>
		</h1>
		<br>
		<!-- END section-title -->
		<!-- BEGIN row -->
		<div class="row row-space-10">
            <div class="table-responsive">
                <table class="table table-striped table-hover fullTable">
                  <div class="col-md-3"></div>
                  <div class="col-md-6">
                    <tr>
                        <td class="col-md-3">No. Invoice</td>
                        <td class="col-md-3">Tanggal</td>
                        <td class="col-md-3">Total</td>
                        <td class="col-md-2">Status</td>
                        <td class="col-md-1">Action</td>

                    </tr>
                    @if ($orders)
                    @foreach ($orders as $order)
                    <tr>
                     <td class="col-md-3">{{ $order->invoice_no }}</td>
                     <td class="col-md-3">{{ $order->created_at }}</td>
                     <td class="col-md-3">Rp {{ round($order->grand_total) }}</td>
                     <td class="col-md-3">
                       {{ OrderStatus::getString($order->status) }}
                   </td>
                   <td>
                       <a href="{{ route('user.order.detail', $order->id) }}" class="btn btn-info btn-outline btn-sm">
                         <span class="fa fa-eye"></span> Lihat Detail
                     </a>
                 </td>
             </tr>
             @endforeach
             @endif
         </div>
         <div class="col-md-3"></div>
     </table>
 </div>
</div>
<!-- END row -->
</div>
<!-- END container -->
</div>
<!-- END #promotions -->
@endsection