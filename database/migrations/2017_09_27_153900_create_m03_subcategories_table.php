<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM03SubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m03_subcategories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m03_category_id');
            $table->longText('name');
            $table->longText('slug_url');
            $table->dateTime('created_at')->nullable();

            $table->foreign('m03_category_id')->references('id')->on('m03_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m03_subcategories');
    }
}
