@extends('layouts.backend.master')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="/">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Dashboard</span>
                </li>
            </ul>
        </div>
        @include('includes.session_message')
        <h1 class="page-title"> Hello Vendor {{ Auth::user()->name}}, have a wonderful day ahead</h1>
    </div>
</div>
@endsection