<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M03AboutUs;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AboutUsController extends Controller
{
    protected $view = 'backend.about-us';
    protected $route = 'admin.about-us';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $aboutUs = M03AboutUs::whereNull('m02_outlet_id')->first();
        return view($this->view.'.index')
        ->with('adminAboutUsList', 'selected')
        ->with('settingClass', 'start active open')
        ->with('adminAboutUsClass', 'start active open')
        ->with('aboutUs',$aboutUs);
    }

    public function store(Request $request) {
        $this->validate($request, M03AboutUs::rules());
        try {

            DB::beginTransaction();
            $aboutUs = M03AboutUs::whereNull('m02_outlet_id')->first();
            if(!$aboutUs) $aboutUs = new M03AboutUs();
            $aboutUs->content = $request->content;
            $aboutUs->latlong = $request->latlong;
            $aboutUs->created_at = Carbon::now()->format('Y-m-d H:i:s');
            if (!$aboutUs->save()) {
                return back()->withInput()->withErrors($aboutUs->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Tentang Kami berhasil disimpan.");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

}