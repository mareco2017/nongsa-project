<?php

namespace App\Helpers;

use Session;
use Laracasts\Flash\flash;

final class SessionHelper {

	public static function setMessage($msg, $style = 'success', $important = false) {
		return $important ? 
            flash(self::prettifyMessage($msg), $style)->important() :
            flash(self::prettifyMessage($msg), $style);
	}

    public static function errorMessage($msg, $style = 'error', $important = false) {
        return $important ? 
            flash(self::prettifyMessage($msg), $style)->important() :
            flash(self::prettifyMessage($msg), $style);
    }

    public static function prettifyMessage($msg) {

        if (gettype($msg) == 'string') {
            return self::prettifyMessage([$msg]);
        }
    
        $result = '<div style="font-size: 20px;">';
        if (gettype($msg) == 'array') {
            foreach ($msg as $value) {
                $result .= ''. $value ;
            }
            $result .= '</div>';
            return $result;
        }
    }

}

?>
