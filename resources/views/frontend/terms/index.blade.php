@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container">
		<!-- BEGIN section-title -->
		<h1 class="section-title clearfix">
			Terms & Conditions<br>
			@if($term)
			<small>updated at {{ $term->created_at }}</small>
			@endif
		</h1>
		<br>
		<!-- END section-title -->
		<!-- BEGIN row -->
		<div class="row row-space-10">
			@if($term)
			<div>{{ $term->content }}</div>
			@endif
		</div>
		<!-- END row -->
	</div>
	<!-- END container -->
</div>
<!-- END #promotions -->
@endsection