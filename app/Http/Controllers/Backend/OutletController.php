<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\OutletStatus;
use App\Helpers\Enums\UserRole;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M02Outlet;
use App\Models\M03OutletCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class OutletController extends Controller
{
    protected $view = 'backend.outlet';
    protected $route = 'admin.outlets';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view($this->view.'.index')
        ->with('outletList', 'selected')
        ->with('outletStatus', OutletStatus::getArray() )
        ->with('outletClass', 'start active open');
    }

    public function listAllOutlet() {
        $outletList = M02Outlet::select('m02_outlets.*');
        return Datatables::of($outletList)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('admin.outlets.edit', ['outlet' => $model->id]).'"
                class="btn btn-info btn-outline btn-sm">
                <span class="fa fa-eye"></span> View
            </a>
            <a data-url="'.route('admin.outlets.destroy', ['outlet' => $model->id]).'"
                data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#outlet-list" data-message="Would you like to delete this outlet?" 
                class="btn btn-danger btn-outline btn-sm" >
                <span class="fa fa-trash-o"></span> Delete
            </a>';
        })
        ->editColumn('status', function($model){
            return OutletStatus::getString($model->status);
        })
        ->make(true);
    }

    public function newForm() {
        $outletCategory = M03OutletCategory::pluck('name','id');
        return view($this->view.'.form')
        ->with('method','POST')
        ->with('outletList', 'selected')
        ->with('outletCategory', $outletCategory)
        ->with('outletClass', 'start active open');
    }

    public function store(Request $request) {
        $this->validate($request, M02Outlet::rules());
        try {

            DB::beginTransaction();

            $user = new M01User();
            $user->username = $request->input('username');
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->phone = $request->input('phone');
            $user->iplog = $_SERVER['REMOTE_ADDR'];
            $user->role = UserRole::VENDOR;
            $user->save();


            $outlet = new M02Outlet();
            $outlet->m01_user()->associate($user);
            $outlet->name = $user->name;
            $outlet->username =$user->username;
            $outlet->email =$user->email;
            $outlet->address = $request->input('address');
            $outlet->phone = $user->phone;
            $outlet->m03_outlet_category_id = $request->input('m03_outlet_category_id');
            $outlet->outlet_no = $request->input('outlet_no');
            $outlet->blok = $request->input('blok');
            $outlet->global_commission = $request->input('global_commission');
            $outlet->status = $request->input('status');
            if ($request->file('profile_pic')) {
                $upload = FileHelper::upload(request(), 'profile_pic', 'jpeg,bmp,png');
                $outlet->profile_pic = $upload;
            }
            if (!$outlet->save()) {
                return back()->withInput()->withErrors($outlet->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Form succesfully created");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function edit($outlet) {
        $outletCategory = M03OutletCategory::pluck('name','id');
        return view($this->view.'.form')
        ->with('outlet', $outlet)
        ->with('method', 'PUT')
        ->with('outletList', 'selected')
        ->with('outletCategory', $outletCategory)
        ->with('outletClass', 'start active open');
    }

    public function update(Request $request, $outlet) {
        $this->validate($request, M02Outlet::rules($outlet->id));

        try {

            DB::beginTransaction();
            $user = M01User::where('id','=', $outlet->m01_user_id)->first();
            $user->username = $request->input('username');
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            if($request->password != NULL) $user->password = bcrypt($request->input('password'));
            $user->phone = $request->input('phone');
            $user->iplog = $_SERVER['REMOTE_ADDR'];
            $user->role = UserRole::VENDOR;
            $user->save();


            $outlet->m01_user()->associate($user);
            $outlet->name = $user->name;
            $outlet->username =$user->username;
            $outlet->email =$user->email;
            $outlet->address = $request->input('address');
            $outlet->phone = $user->phone;
            $outlet->m03_outlet_category_id = $request->input('m03_outlet_category_id');
            $outlet->outlet_no = $request->input('outlet_no');
            $outlet->blok = $request->input('blok');
            $outlet->status = $request->input('status');
            $outlet->global_commission = $request->input('global_commission');
            if ($request->file('profile_pic')) {
                $upload = FileHelper::upload(request(), 'profile_pic', 'jpeg,bmp,png');
                $outlet->profile_pic = $upload;
            }
            if (!$outlet->save()) {
                return back()->withInput()->withErrors($outlet->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Form succesfully updated");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function destroy($outlet, Request $request) {
        $outlet->delete();
        if ($request->ajax() || $request->wantsJson()) {
            return response('ok.', 200);
        }
        SessionHelper::setMessage("Record Has Been Deleted!");
        return redirect()->route($this->route.'.index');
    }

}