@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #section -->
<div class="section-container bg-white">
	<!-- BEGIN container -->
	<div class="container-hp">
		{{ csrf_field() }}
		@include('includes.session_message')
		<!-- BEGIN carousel slide -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				@for($i=0; $i<$countBanner; $i++)
				<li data-target="#myCarousel" data-slide-to="{{ $i }}" class="active"></li>
				@endfor
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				@if(count($banners) != 0)
				@foreach($banners as $banner)
				<div class="item {{ $isFirst ? 'active' : ''}}">
					<img src="{{ asset($banner->m01_picture->url) }}" alt="First Item">
				</div>
				<?php $isFirst = false ?>
				@endforeach
				@else
				<img src={{ asset('/uploads/95b85df2417f3047a8ba6a00273049d3.jpg') }}
				@endif
			</div>

			<!-- Left and right controls -->
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		<!-- END carousel slide -->
		<!-- BEGIN store-outlets -->
		<div id="store-outlets" class="category-container borderless-container">
			<!-- BEGIN container -->
			<div class="section-title">
				<!-- BEGIN section-title -->
				<h4 class="section-title item store clearfix">
					<span class="fa fa-home section-icon-store"></span>
					Store/Outlets
				</h4>
			</div>
			<div class="col-md-12 grid-items">
				<div class="col-md-4 grid-items">
					@if($storeBanner)
					<img src={{ asset($storeBanner->m01_picture->url) }} class="section-image">
					@else
					<img src={{ asset('/uploads/95b85df2417f3047a8ba6a00273049d3.jpg') }} class="section-image">
					@endif
				</div>
				<div class="col-md-8 grid-items">
					<div class="col-md-12 grid-items">
						@if ($outlets)
						@foreach ($outlets as $outlet)
						<div class="col-md-4 col-sm-4" style="padding-left:0px!important;padding-right:0px!important">
							<div class="item item-thumbnail">
								<div class="item-image">
									<img src="{{ $outlet->profile_pic }}" >
								</div>
								<div class="item-info">
									<h4 class="item-title">
										{{ $outlet->name }}
									</h4>
									<br>
									<a href="{{ route('outlet.homepages.detail', $outlet->id) }}" class="btn btn-buy-blue">
										Details
									</a>
								</div>
							</div>
						</div>
						@endforeach
					</div>

					@endif
				</div>
			</div>
			@if ($outlets)
			{{ $outlets->appends(['products' => $products->currentPage()])->links() }}
			@endif
		</div>
		<!-- END store-outlets -->
		<!-- BEGIN products -->
		<div id="products" class="category-container borderless-container">
			<!-- BEGIN container -->
			<div class="container section-title">
				<!-- BEGIN section-title -->
				<h4 class="section-title item product clearfix" style="width: 117%">
					<span class="fa fa-shopping-basket section-icon-product"></span>
					Product Recommendation
				</h4>

			</div>
			<div class="col-md-12 grid-items">
				<div class="col-md-4 grid-items">
					@if($productBanner)
					<img src={{ asset($productBanner->m01_picture->url) }} class="section-image">
					@else
					<img src={{ asset('/uploads/95b85df2417f3047a8ba6a00273049d3.jpg') }} class="section-image">
					@endif
				</div>
				<div class="col-md-8 grid-items">
					<div class="col-md-12 grid-items">
						@if ($products)
						@foreach ($products as $product)
						<div class="col-md-4 col-sm-4 hoverable-item" style="padding-left:0px!important;padding-right:0px!important">
							<div class="item item-thumbnail section-item">
								<div id="hover-area">
									<div class="item-image">
										<img src="{{ $product->picture }}" >
									</div>
									<div class="item-info">
										<h4 class="item-title">
											{{ $product->name }}
										</h4>
										<p class="preview-description">
											{{ $product->description }}
										</p>
										
									</div>
								</div>
								<div class="hover-wrapper">
									<div class="wrapper-section">
										
									</div>
									<div class="centered-hover-button">
										<a href="{{ route('user.product.detail', $product->slug_url) }}" class="btn hoverable-white-button">
											View Details
										</a>
									</div>
									<div class="bottom-hover-button item-info">
										<button onclick="addToCart({{ $product->id }})" class="btn btn-yellow" >
											Add to Cart
										</button>
										<a href="{{ route('user.product.buy', $product->slug_url) }}" class="btn btn-buy-blue">
											Buy
										</a>
									</div>
									
								</div>

							</div>
							
						</div>
						@endforeach
					</div>

					@endif
				</div>
			</div>
			@if ($products)
			{{ $products->appends(['outlets' => $outlets->currentPage()])->links() }}
			@endif
		</div>
		<!-- END products -->
	</div>
	<!-- END container -->
</div>
<!-- END #section -->
@endsection

@push('pageRelatedJs')
<script type="text/javascript">
	function addToCart($productId) {
		$.ajax({
			headers: {
				'X-CSRF-Token': $('input[name="_token"]').val()
			},
			url: "{{ route('user.product.cart') }}",
			method: "POST",
			data: { product_id : $productId,
				qty: 1 },
				success: function (data) {
					if (data.message == null) {
						$.gritter.add( 
						{  
							text: '<i class="fa fa-info"></i> Added to Cart Successfully',  
							sticky: false,  
							time: ""
						});
						$("#cart-badge").css("visibility","visible");
						$('#cart-badge').text(data.qty);
					} else {
						$.gritter.add( 
						{    
							text: '<i class="fa fa-exclamation-circle"></i> ' + data.message,  
							sticky: false,  
							time: "",
							class_name:'gritter-light'
						})
					}
					setTimeout(function() {
						$('.alert').fadeOut('fast');
					}, 500);
				}
			});
	}
</script>
@endpush