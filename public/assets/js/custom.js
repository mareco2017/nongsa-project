$(document).ready(function(){

    $('#modalDelete').on('shown.bs.modal', function(e) {
        e.stopPropagation();
        var id = $(e.relatedTarget).data('id');
        var parentTr = $(e.relatedTarget).closest('tr');
        var title = $(e.relatedTarget).data('title');
        var message = $(e.relatedTarget).data('message');
        var action = $(e.relatedTarget).data('url');
        var table_name = $(e.relatedTarget).data('table-name');
        var method = $(e.relatedTarget).data('method') == undefined ? 'DELETE' : $(e.relatedTarget).data('method');
        $('#modal_id_delete').val(id);
        $('.modal-header h4').text(title);
        $('.modal-body p').text(message);
        $('#modalDelete form').attr('action',action).on("submit", function(e){
            e.preventDefault();
            e.stopPropagation();
            var that = this;
            $.ajax({
                url: action,
                type: method,
                data: $(that).serialize() ,
                success: function(result) {
                    $("#modalDelete").modal('hide')
                    if (typeof table_name === "string") {
                        $(table_name).DataTable().ajax.reload();
                    }
                    else {
                        window.location.reload();
                    }
                    $.gritter.add( 
                    {  
                        // title: '<i class="fa fa-info"></i> Process Success',  
                        text: '<i class="fa fa-info"></i> Request successfully process',  
                        sticky: false,  
                        time: ""
                    } 
                    ); 
                }
            });
        });
    }).on('hidden.bs.modal', function (e) {
        e.stopPropagation();
        $('#modal_id_delete').val('');
        $('.modal-header h4').text('');
        $('.modal-body p').text('');
        $('#modalDelete form').attr('action','/').off('submit');
    });

    $('#modalInput').on('shown.bs.modal', function(e) {
        e.stopPropagation();
        var id = $(e.relatedTarget).data('id');
        var parentTr = $(e.relatedTarget).closest('tr');
        var title = $(e.relatedTarget).data('title');
        var message = $(e.relatedTarget).data('message');
        var value = $(e.relatedTarget).data('value');
        var action = $(e.relatedTarget).data('url');
        var table_name = $(e.relatedTarget).data('table-name');
        var method = $(e.relatedTarget).data('method') == undefined ? 'POST' : $(e.relatedTarget).data('method');
        $('#modal_id_input').val(id);
        $('.modal-header h4').text(title);
        $('.modal-body p').text(message);
        $('#modal_id_input').val(value);
        $('#modalInput form').attr('action',action).on("submit", function(e){
            e.preventDefault();
            e.stopPropagation();
            var that = this;
            $.ajax({
                url: action,
                type: method,
                data: $(that).serialize() ,
                success: function(result) {
                    $("#modalInput").modal('hide')
                    if (typeof table_name === "string") {
                        $(table_name).DataTable().ajax.reload();
                    }
                    window.location.reload();
                    $.gritter.add( 
                    {  
                        // title: '<i class="fa fa-info"></i> Process Success',  
                        text: '<i class="fa fa-info"></i> Request successfully process',  
                        sticky: false,  
                        time: ""
                    } 
                    ); 
                }
            });
        });
    }).on('hidden.bs.modal', function (e) {
        e.stopPropagation();
        $('#modal_id_input').val('');
        $('.modal-header h4').text('');
        $('.modal-body p').text('');
        $('#modalInput form').attr('action','/').off('submit');
    });
});
