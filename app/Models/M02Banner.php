<?php

namespace App\Models;

use App\Models\M01Picture;
use Illuminate\Database\Eloquent\Model;

class M02Banner extends Model
{
	protected $table = "m02_banners";

	protected $fillable = [
		'title',
	];
	protected $guarded = ['m01_picture_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'm01_picture_id' => 'required',
		];
		return $rules;
	}

	public function m01_picture()
	{
		return $this->belongsTo(M01Picture::class,'id');
	}

}