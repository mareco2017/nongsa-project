<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Enums\OutletStatus;
use App\Helpers\Enums\UserRole;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M02Outlet;
use App\Models\M03OutletCategory;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class OutletController extends Controller
{
    protected $view = 'frontend.vendor.outlet';
    protected $route = 'vendor.outlets';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $user = M01User::where('id','=', Auth::user()->id)->first();
        $outletCategory = M03OutletCategory::pluck('name','id');
        $outlet = $user->m02_outlet;
        return view($this->view.'.index')
        ->with('outletList', 'selected')
        ->with('outlet', $outlet)
        ->with('outletCategory', $outletCategory)
        ->with('outletStatus', OutletStatus::getArray() )
        ->with('outletClass', 'start active open');
    }


    public function update(Request $request, $outlet) {
        $this->validate($request, M02Outlet::rules($outlet->id));

        try {

            DB::beginTransaction();
            $user = M01User::where('id','=', $outlet->m01_user_id)->first();
            $user->username = $request->input('username');
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            if($request->password != NULL) $user->password = bcrypt($request->input('password'));
            $user->phone = $request->input('phone');
            $user->iplog = $_SERVER['REMOTE_ADDR'];
            $user->role = UserRole::VENDOR;
            $user->save();

            $outlet->m01_user()->associate($user);
            $outlet->name = $user->name;
            $outlet->username =$user->username;
            $outlet->email =$user->email;
            $outlet->address = $request->input('address');
            $outlet->phone = $user->phone;
            $outlet->blok = $request->input('blok');
            if ($request->file('profile_pic')) {
                $upload = FileHelper::upload(request(), 'profile_pic', 'jpeg,bmp,png');
                $outlet->profile_pic = $upload;
            }
            if (!$outlet->save()) {
                return back()->withInput()->withErrors($outlet->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Profile berhasil diupdate");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

}