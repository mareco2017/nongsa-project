<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Enums\AuthOption;
use App\Helpers\Enums\UserRole;
use App\Helpers\SessionHelper;
use App\Http\Controllers\Controller;
use App\Mail\EmailVerification;
use App\Models\M01User;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use PragmaRX\Google2FA\Google2FA;
use Redirect;
use RedirectsUsers;
use Session;
use Validator;

class LoginController extends Controller
{
	use ThrottlesLogins;

    protected function guard()
    {
        return Auth::guard();
    }

    public function showLoginForm() {
        return view('frontend.auth.login');
    }

    public function showRegistrationForm() {
        return view('frontend.auth.register');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        $user = M01User::where('email','=',$request->email)->first();
        if (count($user) != 0) {
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            if ($this->attemptLogin($request)) {
                if ($this->guard()->user()->auth_option == AuthOption::GOOGLE_AUTH) {
                    return redirect()->route('user.verify.login.form', [$this->guard()->user()]);
                } elseif ($this->guard()->user()->auth_option == AuthOption::EMAIL) {
                    $this->guard()->user()->emailotp = strtoupper(str_random(6));
                    $email = new EmailVerification(new M01User(['emailotp' => $this->guard()->user()->emailotp, 'name' => $this->guard()->user()->name]));
                    $this->guard()->user()->save();
                    Mail::to($this->guard()->user()->email)->send($email);
                    return redirect()->route('user.verify.login.email', [$this->guard()->user()]);
                } 

                return $this->sendLoginResponse($request);
            }

                // If the login attempt was unsuccessful we will increment the number of attempts
                // to login and redirect the user back to the login form. Of course, when this
                // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);
        }
        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }
    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
        ?: redirect()->intended($this->redirectPath());
    }

    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        //
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
        ->withInput($request->only($this->username(), 'remember'))
        ->withErrors($errors);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('user.login.form');
    }



}