<?php

namespace App\Helpers;

trait RedirectWhenError {

    public function sendErrorWhenFailed($errors) {
        return property_exists($this, 'redirectWhenError') ? 
            redirect($this->redirectWhenError)->withErrors($errors) : 
            redirect()->back()->withErrors($errors);
    }

}

?>
