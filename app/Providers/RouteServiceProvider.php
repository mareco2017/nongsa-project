<?php

namespace App\Providers;

use App\Models\M01Cart;
use App\Models\M01User;
use App\Models\M02Banner;
use App\Models\M02DeliveryLog;
use App\Models\M02Invoice;
use App\Models\M02Outlet;
use App\Models\M02Product;
use App\Models\M03Credit;
use App\Models\M03New;
use App\Models\M03OutletCategory;
use App\Models\M03ShippingMethod;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        Route::model('m01_user', M01User::class);
        Route::model('m02_outlet', M02Outlet::class);
        Route::model('m02_banner', M02Banner::class);
        Route::model('m02_invoice', M02Invoice::class);
        Route::model('m03_news', M03New::class);
        Route::model('m03_outlet_category', M03OutletCategory::class);
        Route::model('m02_product', M02Product::class);
        Route::model('m02_delivery_log', M02DeliveryLog::class);
        Route::model('m03_credit', M03Credit::class);
        Route::model('m03_shipping_method', M03ShippingMethod::class);
        Route::model('m01_cart', M01Cart::class);


        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
