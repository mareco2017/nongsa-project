<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM01PaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m01_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_order_id');
            $table->date('payment_date');
            $table->longText('payment_proof')->nullable();
            $table->decimal('amount', 24, 2)->default(0);
            
            $table->foreign('m01_order_id')->references('id')->on('m01_orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m01_payments');
    }
}
