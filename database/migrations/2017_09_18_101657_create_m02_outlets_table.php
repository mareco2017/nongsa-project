<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM02OutletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m02_outlets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_user_id');
            $table->unsignedInteger('m03_outlet_category_id');
            $table->longText('outlet_no')->nullable();
            $table->longText('name');
            $table->string('username')->unique();
            $table->longText('address')->nullable()->nullable();
            $table->longText('email');
            $table->longText('phone');
            $table->longText('blok')->nullable();
            $table->longText('profile_pic')->nullable();
            $table->integer('status')->default(0);
            $table->double('global_commission')->default(0);

            $table->foreign('m01_user_id')->references('id')->on('m01_users')->onDelete('cascade');
            $table->foreign('m03_outlet_category_id')->references('id')->on('m03_outlet_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m02_outlets');
    }
}
