<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class M03ProductCategory extends Model
{
	protected $table = "m03_categories";

	protected $fillable = [
		'name','description', 'slug_url'
	];
	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'name' => 'required',
		];
		return $rules;
	}
}