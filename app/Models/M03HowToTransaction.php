<?php

namespace App\Models;

use App\Models\M02Outlet;
use Illuminate\Database\Eloquent\Model;

class M03HowToTransaction extends Model
{
	protected $table = "m03_how_to_transactions";

	protected $fillable = [
		'content','created_at'
	];

	protected $guarded = ['id','m02_outlet_id'];

	public $timestamps = false;

	public static function rules($id = '') 
	{
		$rules = [
			'content' => 'required',
		];
		return $rules;
	}

	public function m02_outlet()
	{
		return $this->belongsTo(M02Outlet::class);
	}
}