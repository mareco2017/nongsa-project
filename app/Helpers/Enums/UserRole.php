<?php

namespace App\Helpers\Enums;

final class UserRole {

	const CUSTOMER = 0;
	const VENDOR = 1;
	const ADMIN = 2;

	public static function getList() {
		return [
			UserRole::CUSTOMER,
			UserRole::VENDOR,
			UserRole::ADMIN,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Customer";
			case 1:
				return "Vendor";
			case 2:
				return "Admin";
		}
	}

}

?>
