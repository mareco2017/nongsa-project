<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\UserRole;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M02Outlet;
use App\Models\M03Overdue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class OverdueController extends Controller
{
    protected $view = 'backend.overdue';
    protected $route = 'admin.overdues';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $overdue = M02Outlet::select('m01_user_id', DB::raw('CONCAT (outlet_no, " - " , name) AS overdueList'))
        ->pluck('overdueList','m01_user_id');
        return view($this->view.'.index')
        ->with('tunggakanList', 'selected')
        ->with('listOfCredit', $overdue)
        ->with('tunggakanClass', 'start active open');
    }

    public function listAllOverdue() {
        $overdueList = M03Overdue::with(['m01_user','m02_invoice'])->get();
        return Datatables::of($overdueList)
        ->editColumn('overdue_day', function($model){
            $today = Carbon::today();
            $expiredDate = Carbon::parse($model->m02_invoice->expired_at);
            Carbon::setLocale('id');
            $rawData = $expiredDate->diffForHumans($today);
            if(strpos($rawData, 'setelah') !== false) {
                return str_replace("setelah", "sebelum jatuh tempo", $rawData);
            } elseif (strpos($rawData, 'detik') !== false) {
                return "jatuh tempo hari ini";
            } elseif (strpos($rawData, 'sebelum') !== false) {
                return str_replace("sebelum", "setelah jatuh tempo", $rawData);
            }
        })->make(true);
    }

}