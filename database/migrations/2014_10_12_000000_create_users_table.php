<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m01_users', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('profile_pic')->nullable();
            $table->longText('name');
            $table->string('username')->unique();
            $table->longText('email');
            $table->longText('emailotp')->nullable();
            $table->tinyInteger('email_verification')->default(0);
            $table->longText('password');
            $table->longText('phone');
            $table->longText('phoneotp')->nullable();
            $table->tinyInteger('phone_verification')->default(0);
            $table->longText('google_auth_code')->nullable();
            $table->longText('googleotp')->nullable();
            $table->longText('iplog')->nullable();
            $table->unsignedInteger('role')->default(0);
            $table->unsignedInteger('auth_option')->default(0);
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m01_users');
    }
}
