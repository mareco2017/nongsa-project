@extends('layouts.frontend.master')
@section('content')
<!-- BEGIN #promotions -->
<div class="section-container bg-white">
	<div class="container">
		<h1 class="section-title clearfix">
			@if($new)
			{{ $new->title }}<br>
			<small>updated at {{ $new->created_at }}</small>
			@endif
		</h1>
		<br>
		<!-- END section-title -->
		<!-- BEGIN row -->
		<div class="row row-space-10">
			@if($new)
			<div>{{ $new->description }}</div>
			@endif
		</div>
	</div>

</div>
<!-- END #promotions -->
@endsection