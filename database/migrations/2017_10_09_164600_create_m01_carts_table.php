<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateM01CartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m01_carts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m01_user_id')->index()->nullable();
            $table->unsignedInteger('m02_product_id');
            $table->longText('session_id')->nullable();
            $table->double('qty')->default(0);
            $table->decimal('subtotal', 24, 2)->default(0);
            
            $table->foreign('m01_user_id')->references('id')->on('m01_users')->onDelete('cascade');
            $table->foreign('m02_product_id')->references('id')->on('m02_products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m01_carts');
    }
}
