@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
@include('includes.modal_delete')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{ route('admin.dashboards.index') }}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Tunggakan</span>
                </li>
            </ul>
        </div>
        <h1 class="page-title"> Tunggakan Invoice</h1>
        @include('includes.session_message')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <h4>Tunggakan</h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-8 search-wrapper">
                            <br />
                            <div class="col-md-2">
                                <h4>Account No.</h4>
                            </div>
                            <div class="col-md-5">
                                {{ Form::select("m01_user_id", $listOfCredit , null, array("id" => "m01_user_id", "class" => "select-input form-control","style"=>"height:35px;")) }}
                            </div>
                            <div class="col-md-4">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-striped table-hover fullTable" id="overdue-list" width="100%">
                            <thead>
                                <tr>
                                    <th id="userable">Nama</th>
                                    <th>Ref. No</th>
                                    <th>Tanggal Inv</th>
                                    <th>Tanggal JTO</th>
                                    <th>Total Tagihan</th>
                                    <th>Keterangan JTO / Hari</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
    $(document).ready(function () {
        var overdueDataTable = $('#overdue-list').DataTable(
        {
            dom: "lrtip",
            responsive: true,
            serverSide: true,
            ajax: {
                url:  "{{ route('admin.overdues.list') }}",
                data: { '_token' : '{{ csrf_token() }}'},
                type: 'POST',
            },
            columns: [
            { data: 'm01_user.name', name: 'm01_user_id', 'className': 'text-center' ,searchable: true, orderable: true },
            { data: 'm02_invoice.invoice_no', name: 'm02_invoice.invoice_no', 'className': 'text-center' },
            { data: 'm02_invoice.created_at', name: 'm02_invoice.created_at', 'className': 'text-center' },
            { data: 'm02_invoice.expired_at', name: 'm02_invoice.expired_at', 'className': 'text-center' },
            { data: 'm02_invoice.grand_total', name: 'm02_invoice.grand_total', 'className': 'text-center' },
            { data: 'overdue_day', name: 'overdue_day', 'className': 'text-center' },
            ]
        });

        $(".select-input, #m01_user_id").on( "change", function () { 
            overdueDataTable
            .column($(this).index()) 
            .search(this.value)
            .draw();
        }); 

        $(".select-input").prepend('<option value="" selected="selected">All</option>');
    });
</script>
@endpush