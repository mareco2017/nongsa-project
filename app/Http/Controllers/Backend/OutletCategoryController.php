<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\OutletStatus;
use App\Helpers\Enums\UserRole;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M03OutletCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class OutletCategoryController extends Controller
{
    protected $view = 'backend.category';
    protected $route = 'admin.categories';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view($this->view.'.index')
        ->with('categoryList', 'selected')
        ->with('categoryClass', 'start active open');
    }

    public function listAllOutletCategory() {
        $categoryList = M03OutletCategory::all();
        return Datatables::of($categoryList)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('admin.categories.edit', ['category' => $model->id]).'"
                class="btn btn-info btn-outline btn-sm">
                <span class="fa fa-eye"></span> View
            </a>
            <a data-url="'.route('admin.categories.destroy', ['category' => $model->id]).'"
                data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#category-list" data-message="Would you like to delete this category?" 
                class="btn btn-danger btn-outline btn-sm" >
                <span class="fa fa-trash-o"></span> Delete
            </a>';
        })
        ->make(true);
    }

    public function newForm() {
        return view($this->view.'.form')
        ->with('method','POST')
        ->with('categoryList', 'selected')
        ->with('categoryClass', 'start active open');
    }

    public function store(Request $request) {
        $this->validate($request, M03OutletCategory::rules());
        try {

            DB::beginTransaction();

            $category = new M03OutletCategory();
            $category->name = $request->name;
            $category->description = $request->description;
            if (!$category->save()) {
                return back()->withInput()->withErrors($category->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Kategori berhasil disimpan");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function edit($category) {
        return view($this->view.'.form')
        ->with('category', $category)
        ->with('method', 'PUT')
        ->with('categoryList', 'selected')
        ->with('categoryClass', 'start active open');
    }

    public function update(Request $request, $category) {
        $this->validate($request, M03OutletCategory::rules($category->id));

        try {

            DB::beginTransaction();
            $category->name = $request->name;
            $category->description = $request->description;
            if (!$category->save()) {
                return back()->withInput()->withErrors($category->errors());
            }

            DB::commit();
            SessionHelper::setMessage("Kategori berhasil diupdate");
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function destroy($category, Request $request) {
        $category->delete();
        if ($request->ajax() || $request->wantsJson()) {
            return response('ok.', 200);
        }
        SessionHelper::setMessage("Record Has Been Deleted!");
        return redirect()->route($this->route.'.index');
    }

}