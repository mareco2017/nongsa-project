@extends('layouts.backend.master')
@extends('shared.datatables')
@section('content')
@include('includes.modal_delete')
<div class="page-content-wrapper">
	<div class="page-content">
		<div class="form-group">
			<h1>Billing : Biaya Lain-lain</h1>
			@include('includes.session_message')
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-inverse">
						<div class="panel-body">
							<table class="table table-striped table-hover fullTable" id="guard-list" width="100%">
								<thead>
									<tr>
										<th>No.</th>
										<th>Tanggal</th>
										<th>No. Invoice</th>
										<th>Total Amount Due / Debit</th>
										<th>Total Amount Paid / Credit</th>
										<th>Action</th>
									</tr>
									<tr id="filter-row">
										<th></th>
										<th></th>
										<th class="tr-align-center">No. Invoice</th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
								<tfoot>
									<tr>
										<th>Biaya Lain-lain : </th>
										<th></th>
										<th></th>
										<th>{{ number_format($totalDebit ,2,'.',',')}}</th>
										<th>{{ number_format($totalCredit,2,'.',',') }}</th>
										<th></th>
									</tr>
									<tr>
										@if($totalCredit-$totalDebit < 0)
										<th style="color: red;">Selisih Debit / Credit : {{ number_format($totalCredit-$totalDebit, 2, '.',',' ) }} </th>
										@elseif($totalCredit-$totalDebit > 0)
										<th style="color: green">Selisih Debit / Credit : {{ number_format($totalCredit-$totalDebit, 2, '.',',' ) }} </th>
										@else
										<th>Selisih Debit / Credit : {{ number_format($totalCredit-$totalDebit, 2, '.',',' ) }} </th>
										@endif
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('pageRelatedJs')
<script type="text/javascript">
	$(document).ready(function () {
		var guardDataTable = $('#guard-list').DataTable(
		{
			dom: "lrtip",
			orderCellsTop: true,
			responsive: true,
			serverSide: true,
			ajax: {
				url:  "{{ route('vendor.others.list') }}",
				data: { '_token' : '{{ csrf_token() }}'},
				type: 'POST',
			},
			columns: [
			{
				"data": "id",
				render: function (data, type, row, meta) {
					return meta.row + meta.settings._iDisplayStart + 1;
				}
			},
			{ data: 'm02_invoice.created_at', name: 'm02_invoice.created_at', 'className': 'text-center' },
			{ data: 'm02_invoice.invoice_no', name: 'm02_invoice.invoice_no', 'className': 'text-center' },
			{ data: 'debit', name: 'debit', 'className': 'text-center' },
			{ data: 'credit', name: 'credit', 'className': 'text-center' },
			{ data: 'action', name: 'action', 'className': 'text-center', orderable: false, searchable: false  }
			],
			"order": [[ 1, "asc" ]]
		});

		$("#guard-list thead tr#filter-row th").each(function (index) {
			var column = $(this).text();

			switch(column){
				case "No. Invoice":
				var input = '<input type="text" class="text-input form-control" width="100%" placeholder="Search By ' + column + '" />';

				$(this).html(input);
				break;
			}
		});

		$(".text-input").on("keyup change", function () { 
			guardDataTable
			.column($(this).parent().index()) 
			.search(this.value) 
			.draw(); 
		});

		
	});
</script>
@endpush