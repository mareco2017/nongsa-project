<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Enums\OutletStatus;
use App\Helpers\Enums\UserRole;
use App\Helpers\FileHelper;
use App\Helpers\SessionHelper;
use App\Models\M01User;
use App\Models\M03ProductCategory;
use App\Models\M03ProductSubcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;

class ProductSubcategoryController extends Controller
{
    protected $view = 'backend.product-subcategory';
    protected $route = 'admin.product-subcategories';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view($this->view.'.index')
        ->with('productSubcategoryList', 'selected')
        ->with('productSubcategoryClass', 'start active open');
    }

    public function listAllProductSubcategory() {
        $productSubcategoryList = M03ProductSubcategory::all();
        return Datatables::of($productSubcategoryList)
        ->addColumn('action', function ($model) {
            return '
            <a href="'.route('admin.product-subcategories.edit', ['productSubcategory' => $model->id]).'"
                class="btn btn-info btn-outline btn-sm">
                <span class="fa fa-eye"></span> Lihat
            </a>
            <a data-url="'.route('admin.product-subcategories.destroy', ['productSubcategory' => $model->id]).'"
                data-toggle="modal" data-target=" #modalDelete" data-title="Confirmation" data-table-name="#product-subcategory-list" data-message="Apakah anda yakin ingin menghapus subkategori ini ?" 
                class="btn btn-danger btn-outline btn-sm" >
                <span class="fa fa-trash-o"></span> Hapus
            </a>';
        })
        ->editColumn('m03_category_id', function($model){
            return $model->category->name;
        })
        ->make(true);
    }

    public function newForm() {
        $productCategories = M03ProductCategory::pluck('name','id');
        return view($this->view.'.form')
        ->with('method','POST')
        ->with('productCategories', $productCategories)
        ->with('productSubcategoryList', 'selected')
        ->with('productSubcategoryClass', 'start active open');
    }

    public function store(Request $request) {
        $this->validate($request, M03ProductSubcategory::rules());
        try {

            DB::beginTransaction();

            $productSubcategory = new M03ProductSubcategory();
            $productSubcategory->name = $request->name;
            $productSubcategory->slug_url = str_slug($request->name, '-');
            $productSubcategory->m03_category_id = $request->m03_category_id;
            if (!$productSubcategory->save()) {
                return back()->withInput()->withErrors($productSubcategory->errors());
            }

            DB::commit();
            SessionHelper::setMessage('Subkategori berhasil disimpan');
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function edit($id) {
        $productSubcategory = M03ProductSubcategory::where('id', $id)->first();
        $productCategories = M03ProductCategory::pluck('name','id');
        return view($this->view.'.form')
        ->with('productSubcategory', $productSubcategory)
        ->with('method', 'PUT')
        ->with('productCategories', $productCategories)
        ->with('productSubcategoryList', 'selected')
        ->with('productSubcategoryClass', 'start active open');
    }

    public function update(Request $request, $id) {
        $productSubcategory = M03ProductSubcategory::where('id', $id)->first();
        $this->validate($request, M03ProductSubcategory::rules($productSubcategory->id));

        try {

            DB::beginTransaction();
            $productSubcategory->name = $request->name;
            $productSubcategory->slug_url = str_slug($request->name, '-');
            $productSubcategory->m03_category_id = $request->m03_category_id;
            if (!$productSubcategory->save()) {
                return back()->withInput()->withErrors($productSubcategory->errors());
            }

            DB::commit();
            SessionHelper::setMessage('Subkategori Produk berhasil diupdate');
            return redirect()->route($this->route.'.index');

        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error($e);
            return back()->withInput()->withErrors($e);
        }
    }

    public function destroy($id, Request $request) {
        $productSubcategory = M03ProductSubcategory::where('id', $id)->first();
        $productSubcategory->delete();
        if ($request->ajax() || $request->wantsJson()) {
            return response('ok.', 200);
        }
        SessionHelper::setMessage('Subkategori Produk berhasil dihapus.');
        return redirect()->route($this->route.'.index');
    }

}