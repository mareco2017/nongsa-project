<?php

namespace App\Helpers\Enums;

final class OutletStatus {

	const PENDING = 0;
	const TERSEWA = 1;
	const EXPIRED = 2;

	public static function getList() {
		return [
			OutletStatus::PENDING,
			OutletStatus::TERSEWA,
			OutletStatus::EXPIRED,
		];
	}

	public static function getArray() {
		$result = [];
		foreach (self::getList() as $arr) {
			$result[$arr] = self::getString($arr);
		}
		return $result;
	}

	public static function getString($val) {
		switch ($val) {
			case 0:
				return "Pending";
			case 1:
				return "Tersewa";
			case 2:
				return "Expired";
		}
	}

}

?>
